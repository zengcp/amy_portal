package cn.gzjp.modules.portal.entity;

import java.util.List;

import cn.gzjp.modules.portal.entity.vo.DownLoadVo;
import org.hibernate.validator.constraints.Length;
import com.fasterxml.jackson.annotation.JsonBackReference;

import cn.gzjp.common.persistence.DataEntity;
import cn.gzjp.common.utils.excel.annotation.ExcelField;

/**
 * 新闻类型Entity
 * @author zengcp
 * @version 2017-08-17
 */
public class Mdict extends DataEntity<Mdict> {
	
	private static final long serialVersionUID = 1L;
	private String type;

	private String name;		// 名称
	private Mdict parent;		// 上级id
	private Integer sort;		// 排序
	private String parentIds;		// 所有父级编号
	private List<Mdict> children;	// 子菜单



	private List<DownLoadVo> downLoadVoList;
	
	public Mdict() {
		super();
	}

	public Mdict(String id){
		super(id);
	}

	@Length(min=0, max=64, message="名称长度必须介于 0 和 64 之间")
	@ExcelField(title="名称", align=2, sort=1)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	@JsonBackReference
	@ExcelField(title="上级id", align=2, sort=3)
	public Mdict getParent() {
		return parent;
	}

	public void setParent(Mdict parent) {
		this.parent = parent;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	public List<Mdict> getChildren() {
		return children;
	}

	public void setChildren(List<Mdict> children) {
		this.children = children;
	}
	public String getParentId() {
		return parent != null && parent.getId() != null ? parent.getId() : "0";
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public List<DownLoadVo> getDownLoadVoList() {
		return downLoadVoList;
	}

	public void setDownLoadVoList(List<DownLoadVo> downLoadVoList) {
		this.downLoadVoList = downLoadVoList;
	}
}