package cn.gzjp.modules.portal.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.portal.entity.Mdict;
import cn.gzjp.modules.portal.dao.MdictDao;

/**
 * 类型Service
 * @author zengcp
 * @version 2017-08-17
 */
@Service
@Transactional(readOnly = true)
public class MdictService extends CrudService<MdictDao, Mdict> {

	public Mdict get(String id) {
		return super.get(id);
	}
	
	public List<Mdict> findList(Mdict businessType) {
		return super.findList(businessType);
	}
	
	public Page<Mdict> findPage(Page<Mdict> page, Mdict businessType) {
		return super.findPage(page, businessType);
	}
	
	@Transactional(readOnly = false)
	public void save(Mdict businessType) {
		super.save(businessType);
	}
	
	@Transactional(readOnly = false)
	public void delete(Mdict businessType) {
		super.deleteByLogic(businessType);
	}
	
	public List<Mdict> findTreeList() {
		Mdict search = new Mdict();
		search.setParent(new Mdict("0"));
		List<Mdict> roots = findList(search);
		List<Mdict> results = Lists.newArrayList();
		for(Mdict mdict:roots){
			search.setParent(new Mdict(mdict.getId()));
			mdict.setChildren(findList(search));
			results.add(mdict);
		}
		return results;
	}

	public List<Mdict> findTreeList(String type) {
		Mdict search = new Mdict();
		search.setParent(new Mdict("0"));
		search.setType(type);
		List<Mdict> roots = findList(search);
		List<Mdict> results = Lists.newArrayList();
		for(Mdict mdict:roots){
			search.setParent(new Mdict(mdict.getId()));
			search.setType(type);
			mdict.setChildren(findList(search));
			results.add(mdict);
		}
		return results;
	}
	
}