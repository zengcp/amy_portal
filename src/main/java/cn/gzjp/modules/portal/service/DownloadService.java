package cn.gzjp.modules.portal.service;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.portal.dao.DownloadDao;
import cn.gzjp.modules.portal.entity.Download;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 下载中心Service
 * @author zengcp
 * @version 2017-08-22
 */
@Service
@Transactional(readOnly = true)
public class DownloadService extends CrudService<DownloadDao, Download> {

	public Download get(String id) {
		return super.get(id);
	}
	
	public List<Download> findList(Download banner) {
		return super.findList(banner);
	}
	
	public Page<Download> findPage(Page<Download> page, Download banner) {
		return super.findPage(page, banner);
	}
	
	@Transactional(readOnly = false)
	public void save(Download banner) {
		super.save(banner);
	}
	
	@Transactional(readOnly = false)
	public void delete(Download banner) {
		super.deleteByLogic(banner);
	}
	
}