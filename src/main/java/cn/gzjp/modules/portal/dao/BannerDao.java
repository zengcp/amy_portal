package cn.gzjp.modules.portal.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.portal.entity.Banner;

/**
 * 轮播图DAO接口
 * @author zengcp
 * @version 2017-08-22
 */
@MyBatisDao
public interface BannerDao extends CrudDao<Banner> {
	
}