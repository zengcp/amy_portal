package cn.gzjp.modules.portal.entity;

import cn.gzjp.common.persistence.DataEntity;

/**
 * 集团概况Entity
 * @author zengcp
 * @version 2017-08-14
 */
public class Profile extends DataEntity<Profile> {
	
	private static final long serialVersionUID = 1L;
	private String title;		// 中文标题
	private String content;		// 详细信息

	private String type; //1公司简介 2品牌关

	public Profile() {
		super();
	}

	public Profile(String id){
		super(id);
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}