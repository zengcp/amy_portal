package cn.gzjp.modules.portal.web;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.constant.Constants;
import cn.gzjp.common.constant.Constants.Status;
import cn.gzjp.common.mapper.JsonMapper;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.utils.Encodes;
import cn.gzjp.common.utils.FileUtils;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.common.utils.VelocityInitializer;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.portal.entity.Item;
import cn.gzjp.modules.portal.entity.Mdict;
import cn.gzjp.modules.portal.entity.News;
import cn.gzjp.modules.portal.service.NewsService;
import cn.gzjp.modules.sys.entity.Dict;
import cn.gzjp.modules.sys.utils.DictUtils;
import com.xiaoleilu.hutool.http.HtmlUtil;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.h2.util.New;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;

/**
 * 最新活动Controller
 * @author zengcp
 * @version 2017-08-17
 */
@Controller
@RequestMapping(value = "${adminPath}/portal/activity")
public class ActivityController extends BaseController {

	@Autowired
	private NewsService newsService;
	

	@RequestMapping(value = {"list", ""})
	public String list(News news, HttpServletRequest request, HttpServletResponse response, Model model) {
		news.setType(Constants.NewsType.ACTIVITY);
		Page<News> apage = new Page<News>(request, response);
		Page<News> page = newsService.findPage(apage, news);
		model.addAttribute("page", page);
		List<Item> itemList = itemService.findTreeList(Constants.ItemId.ACTIVITY);
		model.addAttribute("itemList", itemList);
		return "modules/portal/activityList";
	}


	@RequestMapping(value = "form")
	public String form(News news, Model model) {
		if(news.getNewsId()!=0){
			news = newsService.get(news.getNewsId());
		}
		model.addAttribute("news", news);
		List<Item> itemList = itemService.findTreeList(Constants.ItemId.ACTIVITY);
		model.addAttribute("itemList", itemList);
		return "modules/portal/activityForm";
	}
	

	/**
	 * 保存活动
	 */
	@RequestMapping(value = "save")
	public String save(News news, Model model, RedirectAttributes redirectAttributes) throws Exception{
		news.setContent(Encodes.unescapeHtml(news.getContent()));
		news.setType(Constants.NewsType.ACTIVITY);
		if(StringUtils.isNoneBlank(news.getContent())&&!news.getContent().startsWith("<p")){
			String content = HtmlUtil.cleanHtmlTag(news.getContent());
			news.setContent(content);
		}

		if (!beanValidator(model, news)){
			return form(news, model);
		}
		if(news.getCreateDate()==null){
			news.setCreateDate(new Date());

		}else{
			news.setUpdateDate(new Date());
		}
		newsService.save(news);
		addMessage(redirectAttributes, "保存成功");
		return redirect("/portal/activity/");
	}

	
	/**
	 * 删除活动
	 */
	@RequestMapping(value = "delete")
	public String delete(News news, RedirectAttributes redirectAttributes) {
		newsService.delete(news);
		addMessage(redirectAttributes, "删除成功");
		return redirect("/portal/activity/");
	}
	
	@ModelAttribute
	public News get(@RequestParam(required=false) String id) {
		News entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = newsService.get(id);
		}
		if (entity == null){
			entity = new News();
		}
		return entity;
	}


	@RequestMapping(value = "createIndexHtml")
	@ResponseBody
	public String createIndexHtml(){

		VelocityInitializer.initVelocity();
		VelocityContext context = new VelocityContext();
		context.put("bannerList", this.getBannerList());
		context.put("productTypeList", this.getProductTypeList());
		context.put("ctxStatic", Global.getConfig("ctxStatic"));
		context.put("frontUrl", Global.getConfig("frontUrl"));
		context.put("ctx", ".");
		context.put("menu",6);

		News news = new News();
		news.setType(Constants.NewsType.ACTIVITY);
        //总的
		List<News> activityList = newsService.findList(news);
		context.put("list", activityList);
		context.put("bigType","最新活动");
		context.put("type","0");
		context.put("url","activity");
		String fileName="activity.html";

		String template=Constants.TEMPLATES+"activity.html";
		StringWriter sw = new StringWriter();
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		String htmlFile = Global.getConfig("htmlPath")+fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)){
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}

        //mobile
		template=Constants.TEMPLATES+"mobile/activity.html";
		sw = new StringWriter();
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		htmlFile = Global.getConfig("htmlPath")+"mobile/"+fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)){
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}


        //所有类型的来一波
		List<Dict> dictList = DictUtils.getDictList("activity_type");
		for(Dict dict:dictList) {
			news.setSmallType(dict.getValue());
			activityList = newsService.findList(news);
			context = new VelocityContext();
			context.put("bannerList", this.getBannerList());
			context.put("productTypeList", this.getProductTypeList());
			context.put("ctxStatic", Global.getConfig("ctxStatic"));
			context.put("frontUrl", Global.getConfig("frontUrl"));
			context.put("ctx", ".");
			context.put("menu",6);
			context.put("list", activityList);
			context.put("bigType","最新活动");
			context.put("type",dict.getLabel());
			context.put("url","activity");
			sw = new StringWriter();
			template=Constants.TEMPLATES+"activity.html";
			tpl = Velocity.getTemplate(template, Constants.UTF8);
			tpl.merge(context, sw);
			fileName = "activity-" + news.getSmallType() + ".html";
			htmlFile = Global.getConfig("htmlPath") + fileName;
			FileUtils.deleteFile(htmlFile);
			if (FileUtils.createFile(htmlFile)) {
				FileUtils.writeToFile(htmlFile, sw.toString(), true);
			}


			//mobile
			template=Constants.TEMPLATES+"mobile/activity.html";
			sw = new StringWriter();
			tpl = Velocity.getTemplate(template, Constants.UTF8);
			tpl.merge(context, sw);
			htmlFile = Global.getConfig("htmlPath")+"mobile/"+fileName;
			FileUtils.deleteFile(htmlFile);
			if (FileUtils.createFile(htmlFile)){
				FileUtils.writeToFile(htmlFile, sw.toString(), true);
			}

		}
		//全部记录来一下
		news = new News();
		news.setType(Constants.NewsType.ACTIVITY);
		List<News> allList = newsService.findList(news);
		for(News activity :allList){
			html(activity.getNewsId());
		}
		return JsonMapper.getInstance().toJson("生成成功");
	}


	@RequestMapping(value = "createHtml")
	@ResponseBody
	public String createHtml(long id){
		html(id);
		return JsonMapper.getInstance().toJson("生成成功");
	}

	private void html(long id){
		News news = newsService.get(id);
		String fileName ="activity/"+news.getSmallType()+"-"+id+".html";
		String template=Constants.TEMPLATES+"activity_detail.html";
		VelocityInitializer.initVelocity();
		VelocityContext context = new VelocityContext();
		context.put("news", news);
		context.put("bannerList", this.getBannerList());
		context.put("productTypeList", this.getProductTypeList());
		context.put("ctxStatic", Global.getConfig("ctxStatic"));
		context.put("frontUrl", Global.getConfig("frontUrl"));
		context.put("ctx", "..");
		context.put("menu",6);
		context.put("bigType","最新活动");
		context.put("type",news.getNewsTypeLabel());
		context.put("url","activity");
		StringWriter sw = new StringWriter();
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		String htmlFile = Global.getConfig("htmlPath")+fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)){
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}

		//mobile
		template=Constants.TEMPLATES+"mobile/activity_detail.html";
		sw = new StringWriter();
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		htmlFile = Global.getConfig("htmlPath")+"mobile/"+fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)){
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}
	}

}