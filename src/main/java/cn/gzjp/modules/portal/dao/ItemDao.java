package cn.gzjp.modules.portal.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.portal.entity.Item;

import java.util.List;

/**
 * 栏目DAO接口
 * @author zengcp
 * @version 2017-08-17
 */
@MyBatisDao
public interface ItemDao extends CrudDao<Item> {

    public Item getByItemId(Integer itemId);

    public List<Item> findProfileList();
	
}