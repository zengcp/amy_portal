<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title> 实体店管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<!--
	<div class="ibox-title">
		<h5>轮播图列表 </h5>
	</div>
    -->
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="store" action="${ctx}/portal/store/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
			<span>名称：</span>
			<form:input path="name" htmlEscape="false" maxlength="256"  class=" form-control input-sm"/>
			<div class="pull-right">
				<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			</div>
		</div>

	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
				<table:addRow url="${ctx}/portal/store/form" title="" width="800px" height="580px"></table:addRow><!-- 增加按钮 -->
			<button type="button" onclick="createIndexHtml()"  class="btn btn-primary btn-xs">生成html</button>
			</div>
		
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead style="background:#fff">
			<tr>
				<th>实体店名称</th>
				<th>地址</th>
				<th>电话</th>
				<th width="30%">操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="store">
			<tr>

				<td>
					${store.name}
				</td>
				<td>
					${store.address}
				</td>
				<td>
					${store.tel}
				</td>

				<td>
					<a href="#" onclick="openDialog('编辑', '${ctx}/portal/store/form?id=${store.id}','800px', '580px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 编辑</a>
					<a href="${ctx}/portal/store/delete?id=${store.id}" onclick="return confirmx('确认要删除吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>

				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
	<script type="text/javascript">
        function createIndexHtml(){
            layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
                $.get('${ctx}/portal/store/createIndexHtml',function(result){layer.msg('操作成功', {time: 2000}); search(); });
            });
        }



	</script>
</body>
</html>