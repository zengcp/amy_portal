﻿<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/modules/portal/front/include/taglib2.jsp"%>
<c:set var="ctx" value="${htmlUrl}"/>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="x-ua-compatible" content="IE=Edge,chrome=1" />
	<meta name="Author" content="C8创发软件设计制作" />
	<title>安幕茵---服务搜索</title>
	<meta name="keywords" content="安慕茵" />
	<meta name="Description" content="安慕茵" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<link href="${ctxStatic}/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

	<link rel="stylesheet" href="${ctxStatic}/css/leftnav.css" media="screen" type="text/css" />
	<link href="${ctxStatic}/css/style-c8.css" rel="stylesheet" type="text/css" />
	<link href="${ctxStatic}/css/media-c8.css" rel="stylesheet" type="text/css" />

	<script src="${ctxStatic}/js/jquery-1.10.2.min.js" type="text/javascript"></script>
	<script src="${ctxStatic}/bootstrap-3.3.7/js/bootstrap.min.js" type="text/javascript"></script>    <!--  引入  bootstrap-3.3.7  JS -->
	<script type="text/javascript" src="${ctxStatic}/js/ljz.js?time=2017"></script>
	<script type="text/javascript" src="${ctxStatic}/js/MSClass.js"></script>
	<script type="text/javascript" src="${ctxStatic}/js/baidu_share.js"></script>


	<script>
        function search3() {
            var key_word = $('#searchName3').val();
            if (key_word == "" || key_word == "输入你所查找的内容") {
                alert("请输入你所查找的内容！");
                return false;
            }
            $('#searchForm3').submit();
        };

	</script>
</head>
<body>

<div class="pcheader container-fluid  bg_fff mt35">
	<div class="container">
		<div class="row  topbar">
			<div class="col-lg-2 col-md-12 col-sm-12 col-xs-12 pad0">
				<div class="logo"><img src="${ctxStatic}/imgs/logo.png" alt=""></div>
			</div>
			<div class="col-lg-10 col-md-12 col-sm-12 col-xs-12">
				<div class="seach">
					<form id="searchForm" method="post" action="${frontUrl}/search/product_pc">
						<input type="text" id="searchName" name="name" value="${name}" placeholder="2018明星产品" /><a href="#" onclick="search1()"><img src="${ctxStatic}/imgs/seach-btn.png" alt=""></a>
					</form>
					<script type="text/javascript">
                        function search1() {
                            var key_word = $('#searchName').val();
                            if (key_word == "" || key_word == "输入你所查找的内容") {
                                alert("请输入你所查找的内容！");
                                return false;
                            }
                            $('#searchForm').submit();
                        };


					</script>



				</div>

				<nav>
					<ul class="nav nav-pills">
						<li role="presentation"><a href="${ctx}/index.html">首页</a></li>
						<li role="presentation" class="active" ><a href="#">全系产品</a>
							<ul>
								<c:forEach items="${productTypeList}" var="productTyp">
									<li><a href="${ctx}/product-${productTyp.id}.html">${productTyp.name}</a></li>
								</c:forEach>
							</ul>
						</li>
						<li role="presentation"><a href="${ctx}/brand.html">品牌相关</a>
							<ul>
								<li><a href="${ctx}/brand-1.html">品牌故事</a></li>
								<li><a href="${ctx}/brand-2.html">品牌理念</a></li>
								<li><a href="${ctx}/brand-3.html">品牌大事件</a></li>
								<li><a href="${ctx}/brand-4.html">健康理念</a></li>

							</ul>

						</li>
						<li role="presentation"><a href="${ctx}/news.html">媒体中心</a>
							<ul>
								<li><a href="${ctx}/news.html">新闻动态</a></li>
								<li><a href="${ctx}/download.html">下载中心</a></li>
								<li><a href="${ctx}/news-4.html">公司通告</a></li>
								<li><a href="${ctx}/news-5.html">公告栏</a></li>
								<li><a href="${ctx}/store.html">实体店展示</a></li>
							</ul>
						</li>
						<li role="presentation"><a href="${ctx}/service.html">服务中心</a>
							<ul>
								<li><a href="${ctx}/service1-1.html">问答指导</a></li>
								<li><a href="${ctx}/service2-1.html">政策专区</a></li>
								<li><a href="${ctx}/service3-1.html">政策指导</a></li>
								<li><a href="${ctx}/service4-1.html">学员天地</a></li>
								<li><a href="${ctx}/service5-1.html">用户专区</a></li>
								<li><a href="${ctx}/service6-1.html">商户专区</a></li>
							</ul>
						</li>
						<li role="presentation"><a href="${ctx}/activity.html">最新活动</a>
							<ul>
								<li><a href="${ctx}/activity-1.html">品牌活动</a></li>
								<li><a href="${ctx}/activity-2.html">商户活动</a></li>
								<li><a href="${ctx}/activity-3.html">用户活动</a></li>

							</ul>
						</li>
						<li role="presentation"><a href="${ctx}/profile-9.html">招商加盟</a></li>
						<li role="presentation"><a href="${ctx}/profile-1.html">公司简介</a>
							<ul>
								<li><a href="${ctx}/profile-1.html">了解我们</a></li>
								<li><a href="${ctx}/profile-2.html">企业荣誉</a></li>
								<li><a href="${ctx}/profile-3.html">企业文化</a></li>
								<li><a href="${ctx}/profile-4.html">联系我们</a></li>

							</ul>
						</li>
					</ul>
				</nav>


			</div>
		</div>
	</div>
</div>
<header class="mheader header_warp theme-bright">
	<script>

        $(function(){
            var orgStyle=$("body").attr('style');


            //header nav
            $(".navBox").bind("touchstart",function(){
                if($(".header_warp").is(".header_warp_cut")){

                    $(window).scrollTop(0);
                    $(".navList").slideToggle(function(){
                        $(".header_warp").removeClass('header_warp_cut');
                        $('body').attr('style',orgStyle);
                    });

                }else{
                    $(".header_warp").addClass('header_warp_cut');
                    $(".navList").slideToggle();
                    $("body").css({"height":"100%","overflow":"hidden"});
                }
            })

            //topbar
            $(".topbar_ico").bind("touchstart",function(){
                if($(".topbar_box").is(".topbar_box_cut") ){
                    $(".topbar_box").removeClass('topbar_box_cut');
                }else{
                    $(".topbar_box").addClass('topbar_box_cut');
                }
            })

            //hear menu
            $(".navList_ul .navdrop_toggle").bind("click",function(){

                if( $(this).siblings(".navdrop_icon").is(".navdrop_icon_cut")){
                    $(this).siblings(".navdrop_menu").slideToggle();
                    $(this).siblings(".navdrop_icon").removeClass("navdrop_icon_cut");
                }else{
                    $(this).parent().siblings().find(".navdrop_menu").hide();
                    $(this).siblings(".navdrop_menu").slideToggle();
                    $(this).parent().siblings().find(".navdrop_icon").removeClass("navdrop_icon_cut");
                    $(this).siblings(".navdrop_icon").addClass("navdrop_icon_cut");
                }

            })

            //foot menu
            $(".foot_menu .navdrop_toggle").bind("click",function(){

                if( $(this).siblings(".navdrop_icon").is(".navdrop_icon_cut")){
                    $(this).siblings(".navdrop_menu").slideToggle();
                    $(this).siblings(".navdrop_icon").removeClass("navdrop_icon_cut");
                }else{
                    $(this).parent().siblings().find(".navdrop_menu").hide();
                    $(this).siblings(".navdrop_menu").slideToggle();
                    $(this).parent().siblings().find(".navdrop_icon").removeClass("navdrop_icon_cut");
                    $(this).siblings(".navdrop_icon").addClass("navdrop_icon_cut");
                }

            })

            $(".to_top").bind("click",function(){
                $("body,html").animate({scrollTop:0});
            })



            $(window).scroll(function(){
                if($(window).scrollTop() >= 50){
                    $(".to_top").show();

                }else{
                    $(".to_top").hide();
                }
            })
            setTimeout(function(){
                if($(window).scrollTop() >= 50){
                    $(".to_top").show();
                }
            },1000)

        })


        function showEcode(){
            var getHeight=$(".pop_ecode").height();
            $(".pop_ecode").css({top:"50%",marginTop:-(getHeight/2)}).fadeIn();
            $(".ecode_overlay").fadeIn();
        }
        function hideEcode(){
            $(".pop_ecode").fadeOut();
            $(".ecode_overlay").fadeOut();
        }

        function showActivity(){
            var getHeight=$(".pop_activity").height();
            $(".pop_activity").css({top:"50%",marginTop:-(getHeight/2)}).fadeIn();
            $(".ecode_overlay").fadeIn();
        }
        function hideActivity(){
            $(".pop_activity").fadeOut();
            $(".ecode_overlay").fadeOut();
        }


	</script>
	<a class="logo" href="/m/" onclick="trackEvent('logo');"><img src="${ctxStatic}/imgs/logo.png"></a>
	<div class="seach" style="margin-right: 70px;
    margin-top: 15px;">
		<form id="searchForm2" method="post" action="${frontUrl}/search/product_pc">
			<input type="text" id="searchName2" name="name" value="${name}" placeholder="2018明星产品"><a href="#" onclick="search2()"><img src="${ctxStatic}/imgs/seach-btn.png" alt=""></a>
		</form>
		<script type="text/javascript">
            function search2() {
                var key_word = $('#searchName2').val();
                if (key_word == "" || key_word == "输入你所查找的内容") {
                    alert("请输入你所查找的内容！");
                    return false;
                }
                $('#searchForm2').submit();
            };

		</script>

	</div>
	<div class="navBox">
		<div class="nav_more">
			<span class="icon-bar top"></span> <span class="icon-bar middle"></span> <span class="icon-bar bottom"></span>
		</div>
	</div>

	<div id="navbar" class="navList" style="display: none;">
		<ul class="navList_ul">
			<li>
				<div class="navdrop_icon">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</div>
				<a href="${ctx}/index.html" >首页 <span class="menu_ico menu_ico1"></span></a>
			</li>
			<li class="navdrop">
				<div class="navdrop_icon">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</div>
				<a class="navdrop_toggle">全系产品<span class="menu_ico menu_ico2"></span></a>
				<ul class="navdrop_menu" style="display: none;">
					<c:forEach items="${productTypeList}" var="productTyp">
					<li><a href="${ctx}/product-${productTyp.id}.html">${productTyp.name}</a></li>
					</c:forEach>
				</ul>
			</li>
			<li class="navdrop">
				<div class="navdrop_icon">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</div>
				<a class="navdrop_toggle">品牌相关<span class="menu_ico menu_ico2"></span></a>
				<ul class="navdrop_menu" style="display: none;">
					<li>
						<a href="${ctx}/brand-1.html" >
							品牌故事
						</a>
					</li>
					<li>
						<a href="${ctx}/brand-2.html" >
							品牌理念
						</a>
					</li>
					<li>
						<a href="${ctx}/brand-3.html" >
							品牌大事件
						</a>
					</li>
					<li>
						<a href="${ctx}/brand-4.html" >
							健康理念
						</a>
					</li>
				</ul>
			</li>
			<li class="navdrop">
				<div class="navdrop_icon">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</div>
				<a class="navdrop_toggle">媒体中心<span class="menu_ico menu_ico2"></span></a>
				<ul class="navdrop_menu" style="display: none;">
					<li>
						<a href="${ctx}/news.html" >
							新闻动态
						</a>
					</li>
					<li>
						<a href="${ctx}/download.html" >
							下载中心
						</a>

					<li>
						<a href="${ctx}/news-4.html" >
							公司通告
						</a>
					</li>
					<li>
						<a href="${ctx}/news-5.html" >
							公告栏
						</a>
					</li>
					<li>
						<a href="${ctx}/store" >
							实体店展示录入
						</a>
					</li>
				</ul>
			</li>
			<li class="navdrop">
				<div class="navdrop_icon">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</div>
				<a class="navdrop_toggle">服务中心<span class="menu_ico menu_ico2"></span></a>
				<ul class="navdrop_menu" style="display: none;">
					<li>
						<a href="${ctx}/service1-1.html" >
							问答指导
						</a>
					</li>
					<li>
						<a href="${ctx}/service2-1.html" >
							政策专区
						</a>

					<li>
						<a href="${ctx}/service3-1.html" >
							政策指导
						</a>
					</li>
					<li>
						<a href="${ctx}/service4-1.html" >
							学员天地
						</a>
					</li>
					<li>
						<a href="${ctx}/service5-1.html" >
							用户专区
						</a>
					</li>
					<li>
						<a href="${ctx}/service6-1.html" >
							学员专区
						</a>
					</li>
				</ul>
			</li>
			<li class="navdrop">
				<div class="navdrop_icon">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</div>
				<a class="navdrop_toggle">最新活动<span class="menu_ico menu_ico2"></span></a>
				<ul class="navdrop_menu" style="display: none;">
					<li>
						<a href="${ctx}/activity-1.html" >
							品牌活动
						</a>
					</li>
					<li>
						<a href="${ctx}/activity-2.html" >
							商户活动
						</a>

					<li>
						<a href="${ctx}/activity-3.html" >
							用户活动
						</a>
					</li>

				</ul>
			</li>
			<li>
				<div class="navdrop_icon">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</div>
				<a href="${ctx}/profile-9.html" >招商加盟 <span class="menu_ico menu_ico1"></span></a>
			</li>
			<li class="navdrop">
				<div class="navdrop_icon">
					<span class="icon-bar top"></span>
					<span class="icon-bar middle"></span>
					<span class="icon-bar bottom"></span>
				</div>
				<a class="navdrop_toggle">公司简介<span class="menu_ico menu_ico2"></span></a>
				<ul class="navdrop_menu" style="display: none;">
					<li>
						<a href="${ctx}/profile-1.html" >
							了解我们
						</a>
					</li>
					<li>
						<a href="${ctx}/profile-2.html" >
							企业荣誉
						</a>

					<li>
						<a href="${ctx}/profile-3.html" >
							企业文化
						</a>
					</li>
					<li>
						<a href="${ctx}/profile-4.html" >
							联系我们
						</a>
					</li>

				</ul>
			</li>

		</ul>

	</div>
</header>

<!-- banner -->
<div id="banner" class="men2">
	<div class="meun men2">
		<ul>
			<li class="banner2"><img src="${ctxStatic}/imgs/jq-banner.jpg"></li>
		</ul>
	</div>
	<div>
		<div class="row">
			<div class="col-xs-9">

			</div>
		</div>
	</div>

	<div>
		<form class="form-inline" id="searchForm3" method="post" action="${frontUrl}/search/service_pc">
			<div class="form-group ">
				<div class="input-group services-search">
					<input type="text" class="form-control" name="serviceName"  value="${serviceName}" id="searchName3" placeholder="查找您需要的服务">
					<div class="input-group-addon" onclick="search3()">查找</div>
				</div>
			</div>

		</form>
	</div>
	<div class="jqico">
		<a href="${ctx}/course0-1.html"><img src="${ctxStatic}/imgs/jq-ico01.png" alt=""><h1>教程专区</h2></a>
		<a href="${ctx}/service3-1.html"><img src="${ctxStatic}/imgs/jq-ico02.png" alt=""><h1>政策指导</h2></a>
		<a href="${ctx}/service4-1.html"><img src="${ctxStatic}/imgs/jq-ico03.png" alt=""><h1>学员天地</h2></a>
		<a href="${ctx}/service5-1.html"><img src="${ctxStatic}/imgs/jq-ico04.png" alt=""><h1>商户中心</h2></a>
		<a href="${ctx}/service6-1.html"><img src="${ctxStatic}/imgs/jq-ico05.png" alt=""><h1>经验分享</h2></a>
	</div>


</div>
<!-- / banner -->


<div class="container-fluid bg_f5">
	<div class="container  position">
		<img src="${ctxStatic}/imgs/home.png" alt=""><h3 class="inlineblock"><a>首页&gt;</a><a>服务中心</a></h3>
	</div>
</div>




<div class="container bg_fff top30 jq">
	<div class=" center ">
		<div class="caption1">
			<ul class="nav nav-pills">
				<li role="presentation"><a href="${ctx}/service1-1.html">问答指导</a></li>
				<li role="presentation"><a href="${ctx}/service2-1.html">政策专区</a></li>
				<li role="presentation"><a href="${ctx}/service3-1.html">政策指导</a></li>
				<li role="presentation"><a href="${ctx}/service4-1.html">学员天地</a></li>
				<li role="presentation"><a href="${ctx}/service5-1.html">用户专区</a></li>
				<li role="presentation"><a href="${ctx}/service6-1.html">商户专区</a></li>
			</ul>
			<div class="clear"></div>
		</div>
	</div>
<c:forEach items="${newsList}" var="news">
	<div class="row no-gutter top30  jq-list  ">
		<div class="col-lg-12 col-sm-12 no-gutter">
			<div class="jq-ico"><img src="$news.picUrl" alt=""></div>
			<div class="inlineblock middle">
				<div><a href=""><h3><a href="${ctx}/service/${news.smallType}-${news.newsId}.html">${news.title}</a></h3></a></div>
				<div class="text-info"><span class="gray">${news.author}</span><span  class="gray pad_lr10">|</span><span  class="gray">${news.publishDate}</span><span  class="gray pad_lr10">|</span><span class="green">${news.newsTypeLabel}</span></div>
			</div>
		</div>
	</div>
</c:forEach>


</div>



<!-- footer 1 -->
<div class="container-fluid  footer0">
	<div class="container ad">

		<div class="row item3 ">
			<div class="col-lg-6 col-md-5 col-sm-6 ">
				<div class="inlineblock">
					<h2>消费者服务</h2>
					<h4><a href="">订单查询</a>
						<a href="">代理服务</a>
						<a href="">产品防伪</a>
					</h4>
				</div>
			</div>

			<div class="col-lg-6 col-md-7 col-sm-6">
				<div class="inlineblock">
					<h2>代理服务</h2>
					<h4><a href="${ctx}/course0-1.html">教程专区</a>
						<a href="">物料素材下载</a>
						<a href="">实体店</a>
						<a href="">商学院</a>
					</h4>
				</div>
			</div>

		</div>

	</div>
</div>
<!-- END  footer 1 -->

<script type="text/javascript" src="${ctxStatic}/js/lvbo.js"></script>
<script>
    function go(type,pageNo) {
        pageNo = pageNo-1;
        window.location.href="service"+type+"-"+pageNo+".html";
    }

</script>
</body>
</html>
