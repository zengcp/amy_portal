package cn.gzjp.modules.portal.service;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.portal.dao.ItemDao;
import cn.gzjp.modules.portal.entity.Item;
import com.google.common.collect.Lists;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 类型Service
 * @author zengcp
 * @version 2017-08-17
 */
@Service
@Transactional(readOnly = true)
public class ItemService extends CrudService<ItemDao, Item> {

	public Item get(String id) {
		return super.get(id);
	}

	public Item getByItemId(Integer itemId){
		return dao.getByItemId(itemId);
	}
	
	public List<Item> findList(Item item) {
		return super.findList(item);
	}
	
	public Page<Item> findPage(Page<Item> page, Item item) {
		return super.findPage(page, item);
	}
	
	@Transactional(readOnly = false)
	public void save(Item item) {
		super.save(item);
	}
	
	@Transactional(readOnly = false)
	public void delete(Item item) {
		super.deleteByLogic(item);
	}
	
	public List<Item> findTreeList() {
		Item search = new Item();
		search.setIsShow("1");
		search.setParent(new Item("0"));
		List<Item> roots = findList(search);
		List<Item> results = Lists.newArrayList();
		for(Item item:roots){
			search.setParent(new Item(item.getId()));
			item.setChildren(findList(search));
			results.add(item);
		}
		return results;
	}


	public List<Item> findTreeList(Integer itemId) {
		Item search = new Item();
		Item parent = dao.getByItemId(itemId);
		search.setParent(parent);
		List<Item> roots = findList(search);
		List<Item> results = Lists.newArrayList();
		for(Item item:roots){
			search.setParent(new Item(item.getId()));
			item.setChildren(findList(search));
			results.add(item);
		}
		return results;
	}


	public List<Item> findTreeList(String type) {
		Item search = new Item();
		search.setParent(new Item("0"));
		search.setType(type);
		List<Item> roots = findList(search);
		List<Item> results = Lists.newArrayList();
		for(Item item:roots){
			search.setParent(new Item(item.getId()));
			search.setType(type);
			item.setChildren(findList(search));
			results.add(item);
		}
		return results;
	}

	public List<Item> findProfileList(){
		return dao.findProfileList();
	}
	
}