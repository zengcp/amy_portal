package cn.gzjp.modules.portal.web;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.constant.Constants;
import cn.gzjp.common.mapper.JsonMapper;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.utils.*;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.portal.entity.News;
import cn.gzjp.modules.portal.entity.Product;
import cn.gzjp.modules.portal.entity.Store;
import cn.gzjp.modules.portal.service.StoreService;
import cn.gzjp.modules.sys.entity.Dict;
import cn.gzjp.modules.sys.utils.DictUtils;
import com.xiaoleilu.hutool.http.HtmlUtil;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.StringWriter;
import java.util.List;

/**
 * 实体店Controller
 * @author zengcp
 * @version 2017-08-14
 */
@Controller
@RequestMapping(value = "${adminPath}/portal/store")
public class StoreController extends BaseController {

	@Autowired
	private StoreService storeService;
	
	

	@RequestMapping(value = {"list", ""})
	public String list(Store store, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Store> page = storeService.findPage(new Page<Store>(request, response), store);
		model.addAttribute("page", page);
		return "modules/portal/storeList";
	}

	@RequestMapping(value = "form")
	public String form(Store store, Model model) {
		model.addAttribute("store", store);
		return "modules/portal/storeForm";
	}

	@RequestMapping(value = "save")
	public String save(Store store, Model model, RedirectAttributes redirectAttributes) throws Exception{

		if(!store.getIsNewRecord()){//编辑表单保存
			Store t = storeService.get(store.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(store, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			storeService.save(t);//保存
		}else{//新增表单保存
			storeService.save(store);//保存
		}
		addMessage(redirectAttributes, "操作成功");
		return redirect("/portal/store");
	}


	@RequestMapping(value = "delete")
	public String delete(Store store, RedirectAttributes redirectAttributes) {
		storeService.delete(store);
		addMessage(redirectAttributes, "删除成功");
		return redirect("/portal/product/");
	}


	@ModelAttribute
	public Store get(@RequestParam(required=false) String id) {
		Store entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = storeService.get(id);
		}
		if (entity == null){
			entity = new Store();
		}
		return entity;
	}


	@RequestMapping(value = "createIndexHtml")
	@ResponseBody
	public String createIndexHtml(){
		VelocityContext context = this.getContext(4);
		Store search = new Store();
		List<Store> storeList = storeService.findList(search);

		context.put("storeList", storeList);
		if(storeList!=null&&storeList.size()>0){
			context.put("store",storeList.get(0));
		}

		List<String> provinceList = storeService.selectDistinctProvince();
		context.put("provinceList", provinceList);
		if(provinceList!=null&&provinceList.size()>0){
			context.put("cityList", storeService.selectDistinctCity(provinceList.get(0)));
		}

		StringWriter sw = new StringWriter();
		String template=Constants.TEMPLATES+"store.html";
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		String fileName="store.html";
		String htmlFile = Global.getConfig("htmlPath")+fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)){
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}


		//mobile

		sw = new StringWriter();
		template=Constants.TEMPLATES+"mobile/store.html";
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		fileName="mobile/store.html";
		htmlFile = Global.getConfig("htmlPath")+fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)){
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}

		return JsonMapper.getInstance().toJson("生成成功");
	}


	@RequestMapping(value = "findLngAndLatByAddress")
	@ResponseBody
	public String findLngAndLatByAddress(@RequestParam(value = "address") String address) {
		return JsonMapper.nonDefaultMapper().toJson(BaiduAPI.getLngAndLat(address));
	}

}