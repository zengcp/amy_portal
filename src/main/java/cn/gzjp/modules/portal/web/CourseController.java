package cn.gzjp.modules.portal.web;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.constant.Constants;
import cn.gzjp.common.constant.Constants.Status;
import cn.gzjp.common.mapper.JsonMapper;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.utils.Encodes;
import cn.gzjp.common.utils.FileUtils;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.common.utils.VelocityInitializer;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.portal.entity.News;
import cn.gzjp.modules.portal.service.NewsService;
import cn.gzjp.modules.sys.entity.Dict;
import cn.gzjp.modules.sys.utils.DictUtils;
import com.xiaoleilu.hutool.http.HtmlUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;

/**
 * 教程专区Controller
 * @author zengcp
 * @version 2017-08-17
 */
@Controller
@RequestMapping(value = "${adminPath}/portal/course")
public class CourseController extends BaseController {

	@Autowired
	private NewsService newsService;


	@RequiresPermissions("portal:news:list")
	@RequestMapping(value = {"list", ""})
	public String list(News news, HttpServletRequest request, HttpServletResponse response, Model model) {
		news.setType(Constants.NewsType.COURSE);
		Page<News> apage = new Page<News>(request, response);
		Page<News> page = newsService.findPage(apage, news);
		model.addAttribute("page", page);
		return "modules/portal/courseList";
	}


	@RequiresPermissions(value = {"portal:news:view", "portal:news:add", "portal:news:edit"}, logical = Logical.OR)
	@RequestMapping(value = "form")
	public String form(News news, Model model) {
		if (news.getNewsId() != 0) {
			news = newsService.get(news.getNewsId());
		}
		model.addAttribute("news", news);
		return "modules/portal/courseForm";
	}


	/**
	 * 保存教程
	 */
	@RequiresPermissions(value = {"portal:news:add", "portal:news:edit"}, logical = Logical.OR)
	@RequestMapping(value = "save")
	public String save(News news, Model model, RedirectAttributes redirectAttributes) throws Exception {
		news.setContent(Encodes.unescapeHtml(news.getContent()));
		news.setType(Constants.NewsType.COURSE);
		if (StringUtils.isNoneBlank(news.getContent()) && !news.getContent().startsWith("<p")) {
			String content = HtmlUtil.cleanHtmlTag(news.getContent());
			news.setContent(content);
		}

		if (!beanValidator(model, news)) {
			return form(news, model);
		}
		if (news.getCreateDate() == null) {
			news.setCreateDate(new Date());//新增
			news.setStatus(Status.START);//默认不启用
			newsService.save(news);
		} else {
			news.setUpdateDate(new Date());
			newsService.save(news);
		}
		//html(news.getNewsId());
		addMessage(redirectAttributes, "保存成功");
		return redirect("/portal/course/");
		//return redirect("/portal/news/?repage");
	}


	/**
	 * 删除教程
	 */
	@RequiresPermissions("portal:news:del")
	@RequestMapping(value = "delete")
	public String delete(News news, RedirectAttributes redirectAttributes) {
		newsService.delete(news);
		addMessage(redirectAttributes, "删除成功");
		return redirect("/portal/course/");
	}

	@ModelAttribute
	public News get(@RequestParam(required = false) String id) {
		News entity = null;
		if (StringUtils.isNotBlank(id)) {
			entity = newsService.get(id);
		}
		if (entity == null) {
			entity = new News();
		}
		return entity;
	}


	@RequestMapping(value = "createIndexHtml")
	@ResponseBody
	public String createIndexHtml() {
		String template = Constants.TEMPLATES + "course.html";
		VelocityInitializer.initVelocity();
		VelocityContext context = this.getContext(0);
		News news = new News();
		news.setType(Constants.NewsType.COURSE);
		Page<News> apage = new Page<News>(1, 6);
		Page<News> page = newsService.findPage(apage, news);
		int totalPage = page.getTotalPage() > 0 ? page.getTotalPage() : 1;
		context.put("totalPage", totalPage);
		context.put("type", "0");
		for (int pageNo = 1; pageNo <= totalPage; pageNo++) {
			String fileName = "course0-" + pageNo + ".html";
			apage = new Page<News>(1, 6);
			List<News> newsList = newsService.findPage(apage, news).getList();
			context.put("newsList", newsList);
			context.put("pageNo", pageNo);
			StringWriter sw = new StringWriter();
			Template tpl = Velocity.getTemplate(template, Constants.UTF8);
			tpl.merge(context, sw);
			String htmlFile = Global.getConfig("htmlPath") + fileName;
			FileUtils.deleteFile(htmlFile);
			if (FileUtils.createFile(htmlFile)) {
				FileUtils.writeToFile(htmlFile, sw.toString(), true);
			}
		}
		//所有类型的来一波
		List<Dict> dictList = DictUtils.getDictList("course_type");
		for (Dict dict : dictList) {
			news.setSmallType(dict.getValue());
			context.put("type", dict.getValue());
			page = newsService.findPage(apage, news);
			totalPage = page.getTotalPage() > 0 ? page.getTotalPage() : 1;
			//template=Constants.TEMPLATES+"course"+type+".html";
			context.put("totalPage", totalPage);
			for (int pageNo = 1; pageNo <= totalPage; pageNo++) {
				String fileName = "course" + dict.getValue() + "-" + pageNo + ".html";
				apage = new Page<News>(pageNo, 6);
				List<News> newsList = newsService.findPage(apage, news).getList();
				context.put("newsList", newsList);
				context.put("pageNo", pageNo);
				StringWriter sw = new StringWriter();
				Template tpl = Velocity.getTemplate(template, Constants.UTF8);
				tpl.merge(context, sw);
				String htmlFile = Global.getConfig("htmlPath") + fileName;
				FileUtils.deleteFile(htmlFile);
				if (FileUtils.createFile(htmlFile)) {
					FileUtils.writeToFile(htmlFile, sw.toString(), true);
				}
			}
		}
		news = new News();
		news.setType(Constants.NewsType.COURSE);
		List<News> allList = newsService.findList(news);
		for (News activity : allList) {
			html(activity,false,context);
		}
        //mobile
		createMobileHtml(context);
		return JsonMapper.getInstance().toJson("生成成功");
	}




	public void html(News news,boolean ismobile,VelocityContext context) {
		String template = Constants.TEMPLATES + "course_detail.html";
		String fileName = "course/" + news.getSmallType() + "-" + news.getNewsId() + ".html";
		if(ismobile){
			fileName = "mobile/course/" + news.getSmallType() + "-" + news.getNewsId() + ".html";
			template = Constants.TEMPLATES + "mobile/course_detail.html";
		}
		context.put("ctx", "..");
		context.put("news", news);

		StringWriter sw = new StringWriter();
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		String htmlFile = Global.getConfig("htmlPath") + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}
	}


	private void createMobileHtml(VelocityContext context) {
		String template = Constants.TEMPLATES + "mobile/course.html";
		VelocityInitializer.initVelocity();

		News news = new News();
		news.setType(Constants.NewsType.COURSE);
		Page<News> apage = new Page<News>(1, 6);
		Page<News> page = newsService.findPage(apage, news);
		int totalPage = page.getTotalPage() > 0 ? page.getTotalPage() : 1;
		context.put("totalPage", totalPage);
		context.put("type", "0");
		for (int pageNo = 1; pageNo <= totalPage; pageNo++) {
			String fileName = "mobile/course0-" + pageNo + ".html";
			apage = new Page<News>(1, 6);
			List<News> newsList = newsService.findPage(apage, news).getList();
			context.put("newsList", newsList);
			context.put("pageNo", pageNo);
			context.put("nextPage", "course0-" + (pageNo + 1) + ".html");
			if (pageNo == totalPage) {
				context.put("nextPage", "no");
			}
			StringWriter sw = new StringWriter();
			Template tpl = Velocity.getTemplate(template, Constants.UTF8);
			tpl.merge(context, sw);
			String htmlFile = Global.getConfig("htmlPath") + fileName;
			FileUtils.deleteFile(htmlFile);
			if (FileUtils.createFile(htmlFile)) {
				FileUtils.writeToFile(htmlFile, sw.toString(), true);
			}

		}
		//所有类型的来一波
		List<Dict> dictList = DictUtils.getDictList("course_type");
		for (Dict dict : dictList) {
			news.setSmallType(dict.getValue());
			context.put("type", dict.getValue());
			page = newsService.findPage(apage, news);
			totalPage = page.getTotalPage() > 0 ? page.getTotalPage() : 1;
			//template=Constants.TEMPLATES+"course"+type+".html";
			context.put("totalPage", totalPage);
			for (int pageNo = 1; pageNo <= totalPage; pageNo++) {
				String fileName = "mobile/course" + dict.getValue() + "-" + pageNo + ".html";
				apage = new Page<News>(pageNo, 6);
				List<News> newsList = newsService.findPage(apage, news).getList();
				context.put("newsList", newsList);
				context.put("pageNo", pageNo);
				context.put("nextPage", "course" + dict.getValue() + "-" + (pageNo + 1) + ".html");
				if (pageNo == totalPage) {
					context.put("nextPage", "no");
				}
				StringWriter sw = new StringWriter();
				Template tpl = Velocity.getTemplate(template, Constants.UTF8);
				tpl.merge(context, sw);
				String htmlFile = Global.getConfig("htmlPath") + fileName;
				FileUtils.deleteFile(htmlFile);
				if (FileUtils.createFile(htmlFile)) {
					FileUtils.writeToFile(htmlFile, sw.toString(), true);
				}
			}
		}
		news = new News();
		news.setType(Constants.NewsType.COURSE);
		List<News> allList = newsService.findList(news);
		for (News activity : allList) {
			html(activity,true,context);
		}
	}
}