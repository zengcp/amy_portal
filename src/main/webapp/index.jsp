<%@ page language="java" contentType="text/html; charset=UTF-8"
         import="cn.gzjp.common.utils.UserAgentUtils"
         pageEncoding="UTF-8"%>
<%@ page trimDirectiveWhitespaces="true" %>
<%

    if(UserAgentUtils.isMobile(request)){
        request.getRequestDispatcher("/mindex.html").forward(request,response);
    }else{
        request.getRequestDispatcher("/index.html").forward(request,response);
    }

%>