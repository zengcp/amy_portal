package cn.gzjp.modules.portal.entity.vo;

import cn.gzjp.modules.portal.entity.Download;

import java.io.Serializable;
import java.util.List;

/**
 * 用于渲染下载页面
 * @Author zengcp
 * @Date 2018/10/13
 */
public class DownLoadVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private String typeId;

    private List<Download> downloadList;

    private String nextPage;

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public List<Download> getDownloadList() {
        return downloadList;
    }

    public void setDownloadList(List<Download> downloadList) {
        this.downloadList = downloadList;
    }

    public String getNextPage() {
        return nextPage;
    }

    public void setNextPage(String nextPage) {
        this.nextPage = nextPage;
    }
}
