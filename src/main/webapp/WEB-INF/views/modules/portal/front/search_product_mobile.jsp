<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/modules/portal/front/include/taglib2.jsp"%>
<c:set var="ctx" value="${htmlUrl}"/>

<!DOCTYPE html>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="x-ua-compatible" content="IE=Edge,chrome=1" />
<meta name="Author" content="C8创发软件设计制作" />
<title>安慕茵---产品搜索</title>
<meta name="keywords" content="安慕茵" />
<meta name="Description" content="安慕茵" />

	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1,user-scalable=no">
	<link href="${ctxStatic}/mobile/bootstrap-3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	<link href="${ctxStatic}/mobile/css/media-c8.css" rel="stylesheet" type="text/css" />
	<link href="${ctxStatic}/mobile/css/style-c8.css" rel="stylesheet" type="text/css" />
	<link href="${ctxStatic}/mobile/css/style.css" rel="stylesheet" type="text/css" />


	<script src="${ctxStatic}/mobile/js/jquery-1.10.2.min.js" type="text/javascript"></script>
	<script src="${ctxStatic}/mobile/bootstrap-3.3.7/js/bootstrap.js"></script>

	<script>

        //html root的字体计算应该放在最前面，这样计算就不会有误差了/
        var _htmlFontSize = (function(){
            var clientWidth = document.documentElement ? document.documentElement.clientWidth : document.body.clientWidth;
            if(clientWidth > 750) clientWidth = 750;
            document.documentElement.style.fontSize = clientWidth * 1/16+"px";
            return clientWidth * 1/16;
        })();

        // 320/750=0.426666667       750/750=1
        //  1/16 =0.0625
        //  0.02666                  1px = 0.0266 rem

        function search1() {
            var key_word = $('#searchName').val();
            if (key_word == "" || key_word == "输入你所查找的内容") {
                alert("请输入你所查找的内容！");
                return false;
            }
            $('#searchForm').submit();
        };
	</script>

</head>
<body>
<div class="homepage ">
	<header class="cc " >
		<div class="logo"><img src="${ctxStatic}/mobile/img/LOGO123-82.png"  alt=""></div>
		<div class="top-right">
			<img id="search-ico" src="${ctxStatic}/mobile/img/search-ico.png" alt="">
			<img id="menu-ico" src="${ctxStatic}/mobile/img/menu-ico.png" alt="">
		</div>
		<form  id="searchForm" method="post" action="${frontUrl}/search/product_mobile">
			<div class=" col-xs-12 mdseatch sechbox" style="border-bottom: 1px solid #71ae27;width: 90%;margin: 0 auto;margin-left: 5%;height: 40px">
				<div class="col-xs-9">
					<input type="text" id="searchName" name="name" style="border: 0" placeholder="搜索你需要的产品">
				</div>
				<div class="col-xs-3 text-center">
					<button type="button" class="sebtn" style="background: transparent" onclick="search1()"><img id="" src="${ctxStatic}/mobile/img/search-ico.png" alt="" style="width: 25px;
    margin-bottom: 16px;"></button>
				</div>
			</div>
		</form>

	</header>
	<!-- 导航菜单开始 -->
	<nav class="transition">
		<div class="sidebarInfo font-no-bold">
			<ul id="accordion" class="accordion clear">
				<li ><div class="link navtop"><img src="${ctxStatic}/mobile/img/LOGO123-82.png"  alt=""><i class="fa close"><img src="${ctxStatic}/mobile/img/close.png" alt=""></i></div></li>
				<li><div class="link"><a href="${ctx}/mobile/index.html">首页</a><i class="fa fa-chevron-down"></i></div></li>


				<li>
					<div class="link">全系产品<i class="fa fa-chevron-down">></i></div>
					<ul class="submenu bg_f3">
						#foreach($productTyp in $productTypeList)
						<li><a href="${ctx}/mobile/product-${productTyp.id}.html">${productTyp.name}</a></li>
						#end
					</ul>
				</li>


				<li>
					<div class="link">品牌相关<i class="fa fa-chevron-down">></i></div>
					<ul class="submenu bg_f3">
						<li ><a href="${ctx}/mobile/brand-1.html">品牌故事</a></li>
						<li ><a href="${ctx}/mobile/brand-2.html">品牌理念</a></li>
						<li ><a href="${ctx}/mobile/brand-3.html">品牌大事件</a></li>
						<li ><a href="${ctx}/mobile/brand-4.html">健康理念</a></li>
					</ul>
				</li>
				<li>
					<div class="link">媒体中心<i class="fa fa-chevron-down">></i></div>
					<ul class="submenu bg_f3">
						<li>
							<a href="${ctx}/mobile/news.html" >
								新闻动态
							</a>
						</li>
						<li>
							<a href="${ctx}/mobile/download.html" >
								下载中心
							</a>

						<li>
							<a href="${ctx}/mobile/news4-1.html" >
								公司通告
							</a>
						</li>
						<li>
							<a href="${ctx}/mobile/news5-1.html" >
								公告栏
							</a>
						</li>
						<li>
							<a href="${ctx}/mobile/store.html" >
								实体店展示录入
							</a>
						</li>

					</ul>
				</li>
				<li>
					<div class="link">服务中心<i class="fa fa-chevron-down">></i></div>
					<ul class="submenu bg_f3">
						<li>
							<a href="${ctx}/mobile/service1-1.html" >
								问答指导
							</a>
						</li>
						<li>
							<a href="${ctx}/mobile/service2-1.html" >
								政策专区
							</a>

						<li>
							<a href="${ctx}/mobile/service3-1.html" >
								政策指导
							</a>
						</li>
						<li>
							<a href="${ctx}/service4-1.html" >
								学员天地
							</a>
						</li>
						<li>
							<a href="${ctx}/mobile/service5-1.html" >
								用户专区
							</a>
						</li>
						<li>
							<a href="${ctx}/mobile/service6-1.html" >
								学员专区
							</a>
						</li>
					</ul>
				</li>

				<li>
					<div class="link">最新活动<i class="fa fa-chevron-down">></i></div>
					<ul class="submenu bg_f3">
						<li>
							<a href="${ctx}/mobile/activity-1.html" >
								品牌活动
							</a>
						</li>
						<li>
							<a href="${ctx}/mobile/activity-2.html" >
								商户活动
							</a>

						<li>
							<a href="${ctx}/mobile/activity-3.html" >
								用户活动
							</a>
						</li>

					</ul>
				</li>
				<li>
					<div class="link">招商加盟<i class="fa fa-chevron-down">></i></div>
					<ul class="submenu bg_f3">
						<li>
							<a href="${ctx}/mobile/profile-9.html" >
								招商加盟
							</a>
						</li>
					</ul>
				</li>
				<li>
					<div class="link">公司简介<i class="fa fa-chevron-down">></i></div>
					<ul class="submenu bg_f3">
						<li>
							<a href="${ctx}/mobile/profile-1.html" >
								了解我们
							</a>
						</li>
						<li>
							<a href="${ctx}/mobile/profile-2.html" >
								企业荣誉
							</a>

						<li>
							<a href="${ctx}/mobile/profile-3.html" >
								企业文化
							</a>
						</li>
						<li>
							<a href="${ctx}/mobile/profile-4.html" >
								联系我们
							</a>
						</li>

					</ul>
				</li>
			</ul>
			<div class="sidebarInfoBottom"></div>

		</div>
	</nav>
	<!-- 导航菜单结束 -->


<div class="subnav">
	<a href="${ctx}/mobile/index.html"> 首页 ></a>
	<a href="#"> 全系产品 ></a>
	<a href="#" class="navlast">产品查询</a>
</div>



<div class="download bg_fff">
	<div id="myTabContent" class="tab-content">
		<div class="tab-pane fade in active" id="home">
			<div class=" col-xs-12 download ">
				<div class=" pad20">
					<div class="brand-list" id="brand-waterfall">
						<ul id="rows " style="overflow: hidden">
							<c:forEach items="${productList}" var="product">
							<div class="  col-lg-3 col-xs-6">

								<div class="boxgrid celllist caption logob2 text-left" >
									<a href="javascript:;"  class="a-pic">
										<img class="imgload1" src="${product.picUrl}" />
									</a>
									<div class="pro" style="text-align: left">
										<h3>${product.title}</h3>

										<div class="price2 ">

											<span>￥${product.price2}</span>
											<div  class="oldprice2">
												原价:<i style="font-style: normal;text-decoration: line-through;">￥${product.price}</i>
											</div>
										</div>
									</div>
									<div class="downinfo" style="display: block"><span><a href="${ctx}/mobile/product/${product.productId}.html">立即购买</a></span>
									</div>
								</div>

							</div>
							</c:forEach>

						</ul>
						<!--<div class="more" style="margin: 20px 0"><h3><a href="">更多>></a></h3></div>-->
					</div>

				</div>

			</div>
		</div>


	</div>

</div>
	<div class="start_pro">
		<div class="gray mg_top60 djrx-title">
			<div class="mg_auto "><h2>向你推荐</h2><h5>找到肌肤的完善护理方案</h5></div>
		</div>
		<div class="djrx">
			<ul class=" clear">
				<c:forEach items="${topList}" var="product">
					<li><a href="${ctx}/mobile/product/${product.productId}.html"><img src="${product.picUrl}" alt=""></a></li>
				</c:forEach>
			</ul>
		</div>
	</div>

	<div class="gard mg_auto loomore"><h4><a href="${ctx}/mobile/product-top.html">查看推荐产品</a></h4></div>




</div>

<div class="slides" id="goToTop">
	<img src="${ctxStatic}/mobile/imgs/backtop.png">
</div>



<footer>
	<div class="row no-gutter" >
		<div class="guanzhu clear">
			<div class="col-xs-6"><span class="h3 c888 pad_L40">点击右方图标，关注我们</span></div>
			<div class="col-xs-4 col-xs-offset-2 ico ">
				<a href=""><img src="${ctxStatic}/mobile/img/ico-weibo.png" alt=""></a>
				<a href=""><img src="${ctxStatic}/mobile/img/ico-tmaill.png" alt=""></a>
				<a href=""><img src="${ctxStatic}/mobile/img/ico-jd.png" alt=""></a>

			</div>
		</div>
		<div class="col-xs-12 mg_auto pad_ud40 c888 bg_f5 clear">
			<h3>© 广州安慕茵生物科技有限公司</h3>
			<h3>备案号：*****************</h4>
				<h3>官方网站：WWW.anmoyin.com  电话：400-000-000</h3>
		</div>
	</div>


	<div class="one ">
		<a href=""><img src="${ctxStatic}/mobile/img/btm01.jpg" alt=""></a>
		<a href=""><img src="${ctxStatic}/mobile/img/btm02.jpg" alt=""></a>
		<a href=""><img src="${ctxStatic}/mobile/img/btm03.jpg" alt=""></a>
		<a href=""><img src="${ctxStatic}/mobile/img/btm04.jpg" alt=""></a>
		<div class="clear"></div>
	</div>
</footer>


<script src='${ctxStatic}/mobile/js/lunbo.js'></script>
<script src='${ctxStatic}/mobile/js/nav.js'></script>
</body>
</html>
