package cn.gzjp.modules.portal.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.portal.entity.News;
import cn.gzjp.modules.portal.entity.Product;

/**
 * 产品DAO接口
 * @author zengcp
 * @version 2017-08-15
 */
@MyBatisDao
public interface ProductDao extends CrudDao<Product> {

    Product get(long id);
}