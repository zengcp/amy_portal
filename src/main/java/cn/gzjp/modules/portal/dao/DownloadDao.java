package cn.gzjp.modules.portal.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.portal.entity.Download;

/**
 */
@MyBatisDao
public interface DownloadDao extends CrudDao<Download> {
	
}