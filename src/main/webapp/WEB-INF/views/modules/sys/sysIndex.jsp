<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>

<!DOCTYPE html>
<html>

                        <head>
                            <meta charset="utf-8">
                            <meta name="viewport" content="width=device-width, initial-scale=1.0">
                            <meta name="renderer" content="webkit">
                            <title>安幕茵官网后台管理系统</title>

                        	<%@ include file="/WEB-INF/views/include/head.jsp"%>
                        	<script src="${ctxStatic}/common/inspinia.js?v=3.2.0"></script>
                        	<script src="${ctxStatic}/common/contabs.js"></script>
                            <script type="text/javascript">
                        	$(document).ready(function() {
		 if('${fns:getDictLabel(cookie.theme.value,'theme','默认主题')}' == '天蓝主题'){
			    // 蓝色主题
			        $("body").removeClass("skin-2");
			        $("body").removeClass("skin-3");
			        $("body").addClass("skin-1");
		 }else  if('${fns:getDictLabel(cookie.theme.value,'theme','默认主题')}' == '橙色主题'){
			    // 黄色主题
			        $("body").removeClass("skin-1");
			        $("body").removeClass("skin-2");
			        $("body").addClass("skin-3");
		 }else {
			 // 默认主题
			        $("body").removeClass("skin-2");
			        $("body").removeClass("skin-3");
			        $("body").removeClass("skin-1");
		 };
	 });
			
	</script>

</head>

<body class="fixed-sidebar full-height-layout gray-bg">
    <div id="wrapper">
        <!--左侧导航开始-->
        <nav class="navbar-default navbar-static-side" role="navigation">
            <div class="nav-close"><i class="fa fa-times-circle"></i>
            </div>
            <div class="sidebar-collapse">
                <ul class="nav" id="side-menu">
                    <li class="nav-header">
                        <img src="static/common/img/logo_icon.png"  style="width: 95px;"/>
                    </li>
     
                  <t:menu  menu="${fns:getTopMenu()}"></t:menu>
                </ul>
            </div>
        </nav>
        <!--左侧导航结束-->
        <!--右侧部分开始-->
        <div id="page-wrapper" class="gray-bg dashbard-1">
            <!--
            <div class="row border-bottom">
                <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                    <div class="navbar-header" style="height: 60px;"><a class="navbar-minimalize minimalize-styl-2 btn  " href="#">
                     <img src="static/common/img/icon.png"  style="height: 52px;"/>
                     </a>
                    <form role="search" class="navbar-form-custom" method="post" action="search_results.html">
                        <div class="form-group">
                            <input type="text" placeholder="${fns:getConfig('productName')}" class="form-control" name="top-search" id="top-search">
                        </div>
                    </form>
                   </div>
                   </nav>
                </nav>
            </div>
            -->
            <div class="row content-tabs">
                <nav class="page-tabs J_menuTabs">
                    <div class="page-tabs-content">
                       <a href="javascript:;" class="active J_menuTab" data-id="${ctx}/portal/item">栏目管理</a>
                    </div>
                </nav>
                <!-- 
                <div class="btn-group roll-nav roll-right">
                    <button class="dropdown J_tabClose"  data-toggle="dropdown">关闭操作<span class="caret"></span>

                    </button>
                    <ul role="menu" class="dropdown-menu dropdown-menu-right">
                        <li class="J_tabShowActive"><a>定位当前选项卡</a>
                        </li>
                        <li class="divider"></li>
                        <li class="J_tabCloseAll"><a>关闭全部选项卡</a>
                        </li>
                        <li class="J_tabCloseOther"><a>关闭其他选项卡</a>
                        </li>
                    </ul>
                </div> -->
                <a href="${ctx}/logout" onclick="return confirmx('确认要退出吗？', this.href)" class="roll-nav roll-right J_tabExit"><i class="fa fa fa-sign-out"></i> 退出</a>
                <a href="${ctx }/sys/user/info" class="roll-nav roll-right J_tabExit" style="right:120px">${fns:getUser().name}</a>
            </div>
            <div class="row J_mainContent" id="content-main">
                <iframe class="J_iframe" name="iframe0" width="100%" height="100%" src="${ctx}/portal/item" frameborder="0" data-id="${ctx}/portal/item" seamless></iframe>
            </div>
            <div class="footer">
                <div class="center">安幕茵官网后台管理系统 &copy; 2015-2025</div>
            </div>
        </div>
        <!--右侧部分结束-->
       
       
    </div>
</body>

<script type="text/javascript">

function changeStyle(){
   $.get('${pageContext.request.contextPath}/theme/ace?url='+window.top.location.href,function(result){   window.location.reload();});
}

</script>




</html>