package cn.gzjp.modules.portal.web;

import cn.gzjp.common.utils.CacheUtils;
import cn.gzjp.common.utils.Encodes;
import cn.gzjp.common.utils.MyBeanUtils;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.portal.entity.Item;
import cn.gzjp.modules.portal.entity.Mdict;
import cn.gzjp.modules.portal.service.ItemService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.xiaoleilu.hutool.util.ObjectUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;
import java.util.Map;

/**
 * 栏目管理Controller
 * @author zengcp
 * @version 2017-08-17
 */
@Controller
@RequestMapping(value = "${adminPath}/portal/item")
public class ItemController extends BaseController {

	@Autowired
	private ItemService itemService;
	
	
	/**
	 * 栏目管理列表页面
	 */
	@RequestMapping(value = {"list", ""})
	public String list(Item item, String type, Model model) {
		item.setType(type);
		List<Item> list = itemService.findList(item);
		model.addAttribute("type", type);
		model.addAttribute("list", list);
		return "modules/portal/itemList";
	}

	/**
	 * 查看，增加，编辑栏目管理表单页面
	 */
	@RequestMapping(value = "form")
	public String form(Item item,String type, Model model,Integer parentItemId) {
		if (item.getParent()!=null && StringUtils.isNotBlank(item.getParent().getId())){
			item.setParent(itemService.get(item.getParent().getId()));
			// 获取排序号，最末节点排序号+30
			if (StringUtils.isBlank(item.getId())){
				Item search = new Item();
				search.setParent(new Item(item.getParent().getId()));
				List<Item> list = itemService.findList(search);
				if (list.size() > 0){
					item.setSort(list.get(list.size()-1).getSort());
					item.setItemId(list.get(list.size()-1).getItemId());
					if (item.getSort() != null){
						item.setSort(item.getSort() + 10);
					}
					item.setItemId(list.get(list.size()-1).getItemId());
					if (item.getItemId() != null){
						item.setItemId(item.getItemId() + 1);
					}
				}
			}
		}
		if (item.getSort() == null){
			item.setSort(10);
		}
		if (item.getItemId() == null&&ObjectUtil.isNotNull(item.getParent())){
			item.setItemId(item.getParent().getItemId()*10+1);
		}
		item.setParentItemId(parentItemId);
		model.addAttribute("item", item);
		model.addAttribute("type", type);
		return "modules/portal/itemForm";
	}




	/**
	 * 保存栏目管理
	 */
	@RequestMapping(value = "save")
	public String save(Item item, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if(!item.getIsNewRecord()){
			Item t = itemService.get(item.getId());
			MyBeanUtils.copyBeanNotNull2Bean(item, t);
			if(ObjectUtil.isNotNull(t.getParent())&&StrUtil.isNotBlank(t.getParent().getId())){
				Item parent = itemService.get(t.getParent().getId());
				t.setParentItemId(parent.getItemId());
			}
			if(t.getParent()==null|| StrUtil.isBlank(t.getParent().getId())){
				t.setParent(new Item("0"));
			}

			itemService.save(t);
		}else{//新增表单保存
			Item exists = itemService.getByItemId(item.getItemId());
			if(ObjectUtil.isNotNull(exists)){
				addMessage(redirectAttributes, "栏目编号存在，操作失败");
				return redirect("/portal/item/");
			}

			if(item.getParent()==null|| StrUtil.isBlank(item.getParent().getId())){
				item.setParent(new Item("0"));
			}
			itemService.save(item);
		}
		CacheUtils.remove("itemList");
		CacheUtils.remove("itemList_"+item.getItemId());
		addMessage(redirectAttributes, "保存栏目成功");
		return redirect("/portal/item");
	}
	
	/**
	 * 删除栏目管理
	 */
	@RequestMapping(value = "delete")
	public String delete(Item item, RedirectAttributes redirectAttributes) {
		itemService.delete(item);
		CacheUtils.remove("itemList_"+item.getItemId());
		CacheUtils.remove("itemList");
		addMessage(redirectAttributes, "删除栏目成功");
		return redirect("/portal/item");
		//return redirect("/portal/item?type="+item.getType());
	}
	

//
//	@ResponseBody
//	@RequestMapping(value = "treeData")
//	public List<Map<String, Object>> treeData(String type) {
//		List<Map<String, Object>> mapList = Lists.newArrayList();
//		Item search = new Item();
//		search.setType(type);
//		List<Item> list = itemService.findList(search);
//		for (int i=0; i<list.size(); i++){
//			Item e = list.get(i);
//				Map<String, Object> map = Maps.newHashMap();
//				map.put("id", e.getId());
//				map.put("pId", e.getParentId());
//				map.put("name", e.getName());
//				mapList.add(map);
//		}
//		return mapList;
//	}

	@ModelAttribute
	public Item get(@RequestParam(required=false) String id) {
		Item entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = itemService.get(id);
		}
		if (entity == null){
			entity = new Item();
		}
		return entity;
	}



	@RequestMapping(value = "contentForm")
	public String contentForm(Item item, Model model) {
		item = itemService.get(item.getId());
		model.addAttribute("item", item);
		return "modules/portal/itemContentForm";
	}

	@RequestMapping(value = "saveContent")
	public String save(Item item,RedirectAttributes redirectAttributes) throws Exception{
		item.setContent(Encodes.unescapeHtml(item.getContent()));
		itemService.save(item);
		addMessage(redirectAttributes, "保存成功");
		return redirect("/portal/item/");
	}


	@ResponseBody
	@RequestMapping(value = "treeData")
	public List<Map<String, Object>> treeData(Integer itemId) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		Item search = new Item();
		search.setItemId(itemId);
		List<Item> list = itemService.findTreeList(itemId);
		for (Item item:list){
			Item e = item;
			Map<String, Object> map = Maps.newHashMap();
			map.put("id", e.getItemId());
			map.put("pId", e.getParentItemId());
			map.put("name", e.getName());
			map.put("itemId", e.getItemId());
			mapList.add(map);
			for(Item chilid:item.getChildren()){
				e = chilid;
				map = Maps.newHashMap();
				map.put("id", e.getItemId());
				map.put("pId", e.getParentItemId());
				map.put("name", e.getName());
				map.put("itemId", e.getItemId());
				mapList.add(map);
			}
		}
		return mapList;
	}

}