<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>新闻资讯管理</title>
	<meta name="decorator" content="default"/>
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/webuploader-0.1.5/webuploader.css">
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/webuploader-0.1.5/demo.css">
	<script type="text/javascript">
    // 添加全局站点信息
    var BASE_URL = '/webuploader';
    </script>
	<script type="text/javascript" src="${ctxStatic}/webuploader-0.1.5/webuploader.js"></script>
	<script type="text/javascript" src="${ctxStatic}/jquery-jcrop/jquery.Jcrop.min.js"></script>
    <link rel="stylesheet" href="${ctxStatic}/jquery-jcrop/jquery.Jcrop.min.css" type="text/css" />
	<script type="text/javascript" charset="utf-8" src="${ctxStatic}/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="${ctxStatic}/ueditor/ueditor.all.min.js"> </script>
	<script type="text/javascript">
	var toolbars= [[
                    'fullscreen', 'source', '|', 
                    'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'removeformat', 'formatmatch',  'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|',
                    'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                    'paragraph', 'fontfamily', 'fontsize', '|',
                    'indent', '|',
                    'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                    'link',  '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
                    'simpleupload', 'insertimage', 'emotion', 'insertvideo',  'attachment',    '|',
                    'horizontal', 'spechars', 'preview'
                ]];
	    var validateForm;
		var submited=false;
	    function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()&&!submited){
			  submited=true;
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}

		$(document).ready(function() {
			 //外部js调用
	        laydate({
	            elem: '#date', //目标元素。由于laydate.js封装了一个轻量级的选择器引擎，因此elem还允许你传入class、tag但必须按照这种方式 '#id .class'
	            event: 'focus' //响应事件。如果没有传入event，则按照默认的click
	        });
			 

			<c:if test="${empty news.pic}">
			  $("#prePic").hide();
		    </c:if>
			var ueditor1 = UE.getEditor('editor1',{
		        scaleEnabled :true,
		        toolbars: toolbars
		    });
			
			ueditor1.addListener( 'ready', function( editor ) {
				ueditor1.setContent('${news.content}');
    		 } );

			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
		});

	</script>

</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="news" action="${ctx}/portal/course/save" method="post" class="form-horizontal">
		<form:hidden path="newsId"/>
		<form:hidden path="pic"/>
		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right">分类：</label></td>
					<td class="width-35" >
						<form:select path="smallType" class="form-control required" >
							<form:option value="" label=""/>
							<form:options items="${fns:getDictList('course_type')}" itemLabel="label"
										  itemValue="value" htmlEscape="false" />
						</form:select>
					</td>
					<td class="width-15 active"><label class="pull-right">作者：</label></td>
					<td class="width-35">
						<form:input path="author" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">缩略图：</label></td>
					<td class="width-35">
						<img id="prePic" src="${news.pic}" width="240px"/>
						<div id="uploader-demo" style="width: 500px;">
						    <!--用来存放item-->
						     <div class="progress-small" id="bannerPicDiv" style="display: none;">
								<div id="bannerPic" class="progress-bar" role="progressbar" style="width: 0%;">
								</div>
							</div>
							<br/>
						    <div id="filePicker">选择图片</div>
						</div>
					</td>
					<td class="width-15 active"><label class="pull-right">发布日期：</label></td>
					<td class="width-35">
						<input id="date" name="date" type="text" maxlength="20" class="laydate-icon form-control layer-date input-sm required"
							   value="<fmt:formatDate value="${news.date}" pattern="yyyy-MM-dd"/>"/>
					</td>

				</tr>

				<tr class="zh">
					<td class="width-15 active"><label class="pull-right">标题：</label></td>
					<td class="width-35" colspan="3">
						<form:input path="title" htmlEscape="false"    class="form-control required"/>
					</td>
				</tr>
				<tr class="zh">
					<td class="width-15 active"><label class="pull-right">内容：</label></td>
					<td class="width-35" colspan="3">
						<script id="editor1" name="content" type="text/plain" htmlEscape="false"  style="height:450px;width: 100%">
				      	</script>
					</td>
				</tr>



				
		 	</tbody>
		</table>
	</form:form>
	<script>
var uploader = WebUploader.create({

    // 选完文件后，是否自动上传。
    auto: true,

    // swf文件路径
    swf: BASE_URL + '/js/Uploader.swf',

    // 文件接收服务端。
    server: '${ctx}/portal/banner/imageUpload',

    // 选择文件的按钮。可选。
    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
    pick: '#filePicker',
    fileSizeLimit: 5 * 1024 * 1024,    // 5 M
    fileSingleSizeLimit: 5 * 1024 * 1024 ,   // 1 M
    compress: false,//不启用压缩
    resize: false,//尺寸不改变
    // 只允许选择图片文件。
    accept: {
        title: 'Images',
        extensions: 'gif,jpg,jpeg,png',
        mimeTypes: 'image/jpg,image/jpeg,image/png'
    }
});



//文件上传过程中创建进度条实时显示。
uploader.on( 'uploadProgress', function( file, percentage ) {
	$('#bannerPicDiv').show();
    $('#bannerPic').css( 'width', percentage * 100 + '%' );
});

// 文件上传成功，给item添加成功class, 用样式标记上传成功。
uploader.on( 'uploadSuccess', function( file,response ) {
    if(response.success){
    	$("#pic").val(response.msg);
    	$("#prePic").show();
    	$("#prePic").attr('src',response.msg);
    	layer.msg("上传完成");
    }
});

//文件上传失败，显示上传出错。
uploader.on( 'uploadError', function( file ) {
    top.layer.msg("图片上传失败,请重新上传", {icon: 2});
});
uploader.on("error", function (type) {
    if (type == "Q_TYPE_DENIED") {
        layer.msg("请上传JPG、PNG、GIF、jpeg格式文件");
    } else if (type == "Q_EXCEED_SIZE_LIMIT") {
        layer.msg("文件大小不能超过5M");
    }else {
        layer.msg("上传出错！请检查后重新上传！错误代码"+type);
    }
});
</script>
</body>
</html>