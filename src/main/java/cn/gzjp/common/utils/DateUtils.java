/**
 * Copyright &copy; 2015-2020 <a href="http://www..org/"></a> All rights reserved.
 */
package cn.gzjp.common.utils;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang3.time.DateFormatUtils;

/**
 * 日期工具类, 继承org.apache.commons.lang.time.DateUtils类
 * @author 
 * @version 2014-4-15
 */
public class DateUtils extends org.apache.commons.lang3.time.DateUtils {
	
	private static String[] parsePatterns = {
		"yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM", 
		"yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
		"yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};

	/**
	 * 得到当前日期字符串 格式（yyyy-MM-dd）
	 */
	public static String getDate() {
		return getDate("yyyy-MM-dd");
	}
	
	/**
	 * 得到当前日期字符串 格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 */
	public static String getDate(String pattern) {
		return DateFormatUtils.format(new Date(), pattern);
	}
	
	/**
	 * 得到日期字符串 默认格式（yyyy-MM-dd） pattern可以为："yyyy-MM-dd" "HH:mm:ss" "E"
	 */
	public static String formatDate(Date date, Object... pattern) {
		String formatDate = null;
		if (pattern != null && pattern.length > 0) {
			formatDate = DateFormatUtils.format(date, pattern[0].toString());
		} else {
			formatDate = DateFormatUtils.format(date, "yyyy-MM-dd");
		}
		return formatDate;
	}
	
	public static String formatMenuDate(String menDate){
		String today = DateFormatUtils.format(new Date(), "yyyy-MM-dd");
		if(menDate.equals(today)){
			return "今天";
		}
		String tomorrow = DateFormatUtils.format(addDate(1), "yyyy-MM-dd");
		if(menDate.equals(tomorrow)){
			return "明天";
		}
		menDate = menDate.substring(5, menDate.length());
		menDate = menDate.replace("-", "月");
		menDate = menDate.replace("0", "");
		menDate=menDate+"日";
		return menDate;
	}
	
	public static boolean isTomorrow(Date date){
		String menDate = DateFormatUtils.format(date, "yyyy-MM-dd");
		String tomorrow = DateFormatUtils.format(addDate(1), "yyyy-MM-dd");
		if(menDate.equals(tomorrow)){
			return true;
		}
		return false;
	}
	
	public static String formatMenuDate(Date menDate2){
		String menDate = DateFormatUtils.format(menDate2, "MM月dd日");
		String today = DateFormatUtils.format(new Date(), "MM月dd日");
		if(menDate.equals(today)){
			return menDate+"(今天)";
		}
		String tomorrow = DateFormatUtils.format(addDate(1), "MM月dd日");
		if(menDate.equals(tomorrow)){
			return menDate+"(明天)";
		}
//		menDate = menDate.substring(5, menDate.length());
//		menDate = menDate.replace("-", "月");
//		menDate = menDate.replace("0", "");
//		menDate=menDate+"日";
		return menDate;
	}
	
	
	/**
	 * 
	 * 方法用途和描述: 在当前的日期点上加入指定的天数
	 * 
	 * @param days
	 *            天数，正数为向后；负数为向前
	 * @return 返回改变后的时间点
	 * @author zengcp 新增日期：Apr 18, 2008
	 * @author 你的姓名 修改日期：Apr 18, 2008
	 * @since CE-Common version(1.0)
	 */
	public static Date addDate(int days) {

		Locale loc = Locale.getDefault();
		GregorianCalendar cal = new GregorianCalendar(loc);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		int month = cal.get(Calendar.MONTH);
		int year = cal.get(Calendar.YEAR);
		cal.set(year, month, day + days);
		return cal.getTime();
	}
	
	/**
	 * 得到日期时间字符串，转换格式（yyyy-MM-dd HH:mm:ss）
	 */
	public static String formatDateTime(Date date) {
		return formatDate(date, "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 得到当前时间字符串 格式（HH:mm:ss）
	 */
	public static String getTime() {
		return formatDate(new Date(), "HH:mm:ss");
	}

	/**
	 * 得到当前日期和时间字符串 格式（yyyy-MM-dd HH:mm:ss）
	 */
	public static String getDateTime() {
		return formatDate(new Date(), "yyyy-MM-dd HH:mm:ss");
	}

	/**
	 * 得到当前年份字符串 格式（yyyy）
	 */
	public static String getYear() {
		return formatDate(new Date(), "yyyy");
	}

	/**
	 * 得到当前月份字符串 格式（MM）
	 */
	public static String getMonth() {
		return formatDate(new Date(), "MM");
	}

	/**
	 * 得到当天字符串 格式（dd）
	 */
	public static String getDay() {
		return formatDate(new Date(), "dd");
	}

	/**
	 * 得到当前星期字符串 格式（E）星期几
	 */
	public static String getWeek() {
		// 创建“简体中文”的Locale  
		String week =  formatDate(new Date(), "E");
		if(week.toUpperCase().trim().equals("MON")){
			week="星期一";
		}
		if(week.toUpperCase().trim().equals("TUE")){
			week="星期二";
		}
		if(week.toUpperCase().trim().equals("WED")){
			week="星期三";
		}
		if(week.toUpperCase().trim().equals("THU")){
			week="星期四";
		}
		if(week.toUpperCase().trim().equals("FRI")){
			week="星期五";
		}
		if(week.toUpperCase().trim().equals("SAT")){
			week="星期六";
		}
		if(week.toUpperCase().trim().equals("SUN")){
			week="星期天";
		}
		return week;
		
	}
	
	/**
	 * 日期型字符串转化为日期 格式
	 * { "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", 
	 *   "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm",
	 *   "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm" }
	 */
	public static Date parseDate(Object str) {
		if (str == null){
			return null;
		}
		try {
			return parseDate(str.toString(), parsePatterns);
		} catch (ParseException e) {
			return null;
		}
	}
	
	
	
	

	/**
	 * 获取过去的天数
	 * @param date
	 * @return
	 */
	public static long pastDays(Date date) {
		long t = new Date().getTime()-date.getTime();
		return t/(24*60*60*1000);
	}

	/**
	 * 获取过去的小时
	 * @param date
	 * @return
	 */
	public static long pastHour(Date date) {
		long t = new Date().getTime()-date.getTime();
		return t/(60*60*1000);
	}
	
	/**
	 * 获取过去的分钟
	 * @param date
	 * @return
	 */
	public static long pastMinutes(Date date) {
		long t = new Date().getTime()-date.getTime();
		return t/(60*1000);
	}
	
	/**
	 * 转换为时间（天,时:分:秒.毫秒）
	 * @param timeMillis
	 * @return
	 */
    public static String formatDateTime(long timeMillis){
		long day = timeMillis/(24*60*60*1000);
		long hour = (timeMillis/(60*60*1000)-day*24);
		long min = ((timeMillis/(60*1000))-day*24*60-hour*60);
		long s = (timeMillis/1000-day*24*60*60-hour*60*60-min*60);
		long sss = (timeMillis-day*24*60*60*1000-hour*60*60*1000-min*60*1000-s*1000);
		return (day>0?day+",":"")+hour+":"+min+":"+s+"."+sss;
    }
	
	/**
	 * 获取两个日期之间的天数
	 * 
	 * @param before
	 * @param after
	 * @return
	 */
	public static double getDistanceOfTwoDate(Date before, Date after) {
		long beforeTime = before.getTime();
		long afterTime = after.getTime();
		return (afterTime - beforeTime) / (1000 * 60 * 60 * 24);
	}
	/**
	 * 得到小时
	 */
	public static int  getHour(Date date) {
		String dd = formatDate(date, "HH");
		if(dd.startsWith("0")){
			dd = dd.substring(1, dd.length());
		}
		return Integer.parseInt(dd);
	}
	
	/**
	 * @param args
	 * @throws ParseException
	 */
	public static void main(String[] args) throws ParseException {
//		String time="2017-04-23 12:00:12";
//		System.out.println(time.length());
		System.out.println(DateUtils.getWeek());
		//2017-05-17 10:21:17
		System.out.println(DateUtils.getDate("yyyy-MM-dd"));
		
		String ymd = DateUtils.getDate("yyyy-MM-dd");
		String deadLine = ymd+" "+"10:20:49";
		System.out.println(DateUtils.parseDate(deadLine));
//		long time = new Date().getTime()-parseDate("2012-11-19").getTime();
//		System.out.println(time/(24*60*60*1000));
		
		Set<String> taskIds = new HashSet<String>();
		taskIds.add("1111111");taskIds.add("1111111");
		taskIds.add("1111111");taskIds.add("1111111");
		taskIds.add("2111111");
		taskIds.add("211111");
		taskIds.add("2111111");
		System.out.println(taskIds.size());
	}
}
