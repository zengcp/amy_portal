<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>栏目管理</title>
	<meta name="decorator" content="default"/>
	<%@include file="/WEB-INF/views/include/treetable.jsp" %>
	<script src="${ctxStatic}/common/contabs.js"></script>
	<script type="text/javascript">
		$(document).ready(function() {
			var tpl = $("#treeTableTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
			var data = ${fns:toJson(list)}, ids = [], rootIds = [];
			for (var i=0; i<data.length; i++){
				ids.push(data[i].id);
			}
			ids = ',' + ids.join(',') + ',';
			for (var i=0; i<data.length; i++){
				if (ids.indexOf(','+data[i].parentId+',') == -1){
					if ((','+rootIds.join(',')+',').indexOf(','+data[i].parentId+',') == -1){
						rootIds.push(data[i].parentId);
					}
				}
			}
			for (var i=0; i<rootIds.length; i++){
				addRow("#treeTableList", tpl, data, rootIds[i], true);
			}
			$("#treeTable").treeTable({expandLevel : 2});
			$('.zml0').show();
            $('.d70a114351e149a387acbc1bcd7aba35').show();
           // $('.d19fcbf3ca37431dbfc1c6dcd2059d43').show();

            $('.news').show();


            $('.简介页').show();
		});
		function addRow(list, tpl, data, pid, root){
			for (var i=0; i<data.length; i++){
				var row = data[i];
				if ((${fns:jsGetVal('row.parentId')}) == pid){
					$(list).append(Mustache.render(tpl, {
						dict: {
							type: getDictLabel(${fns:toJson(fns:getDictList('wx_menu_type'))}, row.type),
						blank123:0}, pid: (root?0:pid), row: row
					}));
					addRow(list, tpl, data, row.id);
				}
			}
		}
		
		
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	
    <div class="ibox-content">
	<sys:message content="${message}"/>


	
	<!-- 工具栏-->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
				<table:addRow url="${ctx}/portal/item/form?type=${type}" title="栏目管理" width="550px" height="380px" label="新增栏目"></table:addRow>

			<button type="button" onclick="createHomeHtml()"  class="btn btn-primary btn-xs">生成首页html</button>
			<button type="button" onclick="createProfileHtml()"  class="btn btn-primary btn-xs">生成简介页html</button>
			<button type="button" onclick="createProductHtml()"  class="btn btn-primary btn-xs">生成全系产品html</button>

			<button type="button" onclick="createBrandHtml()"  class="btn btn-primary btn-xs">生成品牌相关html</button>
			<button type="button" onclick="createActivityHtml()"  class="btn btn-primary btn-xs">生成最新活动html</button>
			<button type="button" onclick="createNewsHtml()"  class="btn btn-primary btn-xs">新闻动态html</button>

			<button type="button" onclick="createServiceHtml()"  class="btn btn-primary btn-xs">服务中心html</button>
		</div>
	</div>
	</div>

	<table id="treeTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th>栏目名称</th>
				<th>栏目编号</th>
				<th>页面</th>
				<th>url</th>
				<th>排序</th>
				<th width="35%">操作</th>
			</tr>
		</thead>
		<tbody id="treeTableList"></tbody>
	</table>
	<script type="text/template" id="treeTableTpl">
		<tr id="{{row.id}}" pId="{{pid}}">
			<td>
				{{row.name}}
			</td>
			<td>
				{{row.itemId}}
			</td>
			<td>
				{{row.pageName}}
			</td>
			<td>
				{{row.url}}
			</td>
			<td>
				{{row.sort}}
			</td>
			<td>
   				<a href="#" onclick="openDialog('修改', '${ctx}/portal/item/form?id={{row.id}}&type={{row.type}}&parentItemId={{row.parentItemId}}','550px', '380px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>
				<a href="${ctx}/portal/item/delete?id={{row.id}}" onclick="return confirmx('确认要删除该栏目吗？', this.href)" class="btn btn-danger btn-xs" ><i class="fa fa-trash"></i> 删除</a>
			    <span class="zml{{row.parentId}}" style="display:none" ><a href="#" onclick="openDialog('{{row.name}}_新增子栏目', '${ctx}/portal/item/form?parent.id={{row.id}}&type={{row.type}}&parentItemId={{row.itemId}}','550px', '380px')" class="btn btn-warning btn-xs" ><i class="fa fa-plus"></i> 新增子栏目</a>
                </span>
				<span class="{{row.parentId}}" style="display:none" ><a href="#" onclick="openDialog('{{row.name}}_新增子栏目', '${ctx}/portal/item/form?parent.id={{row.id}}&type={{row.type}}&parentItemId={{row.itemId}}','550px', '380px')" class="btn btn-warning btn-xs" ><i class="fa fa-plus"></i> 新增子栏目</a>
                </span>
				<span class="{{row.id}}" style="display:none" ><a href="#" onclick="openDialog('{{row.name}}_新闻类型', '${ctx}/portal/item/form?parent.id={{row.id}}&type={{row.type}}&parentItemId={{row.itemId}}','550px', '380px')" class="btn btn-warning btn-xs" ><i class="fa fa-plus"></i> 新增子栏目</a>
                </span>
				<span class="{{row.pageName}}" style="display:none" ><a href="#" onclick="openDialog('{{row.name}}_编辑页面内容', '${ctx}/portal/item/contentForm?id={{row.id}}&type={{row.type}}','100%', '100%')" ><i class="fa fa-plus"></i> 编辑内容</a>
                </span>

				<span class="{{row.isNewsList}}" style="display:none" ><a  onclick="openTab('${ctx}/portal/news?itemId={{row.itemId}}&type={{row.type}}','{{row.name}}',false)"  ><i class="fa fa-plus"></i> 编辑内容</a>
                </span>

			</td>
		</tr>
	</script>

	<script>
            function createHomeHtml(){
                layer.confirm('确认生成首页html?',{icon: 3, title:'系统提示'},   function(){
                    $.get('${ctx}/html/create/home',function(result){layer.msg('操作成功', {time: 2000}); search(); });
                });
            }

            function createProfileHtml(){
                layer.confirm('确认生成简介页html?',{icon: 3, title:'系统提示'},   function(){
                    $.get('${ctx}/html/create/profile',function(result){layer.msg('操作成功', {time: 2000}); search(); });
                });
            }

            function createProductHtml(){
                layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
                    $.get('${ctx}/html/create/product',function(result){layer.msg('操作成功', {time: 2000}); search(); });
                });
            }

            function createBrandHtml(){
                layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
                    $.get('${ctx}/html/create/brand',function(result){layer.msg('操作成功', {time: 2000}); search(); });
                });
            }
            function createActivityHtml(){
                layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
                    $.get('${ctx}/html/create/activity',function(result){layer.msg('操作成功', {time: 2000}); search(); });
                });
            }

            function createNewsHtml(){
                layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
                    $.get('${ctx}/html/create/news',function(result){layer.msg('操作成功', {time: 2000}); search(); });
                });
            }

            function createServiceHtml(){
                layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
                    $.get('${ctx}/html/create/service',function(result){layer.msg('操作成功', {time: 2000}); search(); });
                });
            }

		</script>
</body>
</html>