package cn.gzjp.modules.portal.web;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.constant.Constants;
import cn.gzjp.common.mapper.JsonMapper;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.utils.*;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.portal.entity.Item;
import cn.gzjp.modules.portal.entity.Mdict;
import cn.gzjp.modules.portal.entity.News;
import cn.gzjp.modules.portal.entity.Product;
import cn.gzjp.modules.portal.service.*;
import cn.gzjp.modules.sys.entity.Dict;
import cn.gzjp.modules.sys.utils.DictUtils;
import com.xiaoleilu.hutool.util.ObjectUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import java.io.StringWriter;
import java.util.List;

/***
 * html生成
 */
@Controller
@RequestMapping(value = "${adminPath}/html/create")
public class HtmlCreateController extends BaseController {


	@Autowired
	private NewsService newsService;
	@Autowired
	private ProductService productService;




	@RequestMapping(value = "home")
	@ResponseBody
	public String homeHtml() {
		Integer itemId = 1;
		VelocityContext context = getContext(itemId);
		//最新资讯
		News news = new News();
		news.setType(Constants.NewsType.NEWS);
		Page<News> apage = new Page<News>(1, 3);
		List<News> newsList = newsService.findPage(apage, news).getList();
		context.put("newsList", newsList);

		//明星产品
		Product product = new Product();
		product.setStar("1");
		Page<Product> page1 = new Page<Product>(1, 3);
		List<Product> starList = productService.findPage(page1, product).getList();
		if (starList != null && starList.size() == 3) {
			context.put("star1", starList.get(0));
			context.put("star2", starList.get(1));
			context.put("star3", starList.get(2));
		}

		//热销产品
		product.setHot("1");
		page1 = new Page<Product>(1, 3);
		List<Product> hotList = productService.findPage(page1, product).getList();
		context.put("hotList", hotList);


		//最新活动
		news = new News();
		news.setType(Constants.NewsType.ACTIVITY);
		apage = new Page<News>(1, 5);
		List<News> activityList = newsService.findPage(apage, news).getList();
		context.put("activityList", activityList);


		//pc
		String template = Constants.TEMPLATES + "index.html";
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		StringWriter sw = new StringWriter();
		tpl.merge(context, sw);
		String fileName = "index.html";
		String htmlFile = Global.getConfig("htmlPath") + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}

		//mobile
		template = Constants.TEMPLATES + "/mobile/index.html";
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		sw = new StringWriter();
		tpl.merge(context, sw);
		fileName = "mobile/index.html";
		htmlFile = Global.getConfig("htmlPath") + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}
		return JsonMapper.getInstance().toJson("生成成功");
	}


	@RequestMapping(value = "profile")
	@ResponseBody
	public String profileHtml() {
		List<Item> allList = itemService.findProfileList();
		for (Item profile : allList) {
			profile(profile, false);
			profile(profile, true);
		}
		return JsonMapper.getInstance().toJson("生成成功");
	}

	private void profile(Item profile, boolean ismobile) {
		String fileName = "profile" + profile.getItemId() + ".html";
		String template = Constants.TEMPLATES + "profile.html";
		if (ismobile) {
			fileName = "mobile/" + fileName;
			template = Constants.TEMPLATES + "mobile/profile.html";
		}
        Integer itemId = profile.getItemId();
		if(ObjectUtil.isNotNull(profile.getParentItemId())){
			itemId = profile.getParentItemId();
		}
		VelocityContext context = getContext(itemId);
		context.put("profile", profile);
		context = getCrumb(profile, context);
		StringWriter sw = new StringWriter();
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		String htmlFile = Global.getConfig("htmlPath") + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}
	}


	@RequestMapping(value = "product")
	@ResponseBody
	public String productHtml() {
		Integer itemId = 2;
		VelocityContext context = getContext(itemId);
		List<Item> productTypeList = this.getItemList(Constants.ItemId.PRODUCT);
		context.put("types", productTypeList);

		Product search = new Product();
		search.setTop("1");
		List<Product> topList = productService.findList(search);
		context.put("topList", topList);


		//pc  推荐产品
		String fileName = "product-top.html";
		context.put("productList", topList);
		context.put("typeName", "推荐产品");
		context.put("typeUrl", fileName);

		String template = Constants.TEMPLATES + "product.html";
		StringWriter sw = new StringWriter();
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		String htmlFile = Global.getConfig("htmlPath") + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}
		//mobile
		template = Constants.TEMPLATES + "mobile/product.html";
		sw = new StringWriter();
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		htmlFile = Global.getConfig("htmlPath") + "mobile/" + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}


		//pc 明星产品
		fileName = "product-star.html";
		search = new Product();
		search.setStar("1");
		List<Product> starList = productService.findList(search);

		context.put("productList", starList);
		context.put("typeName", "明星产品");
		context.put("typeUrl", fileName);
		template = Constants.TEMPLATES + "product.html";
		sw = new StringWriter();
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		htmlFile = Global.getConfig("htmlPath") + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}
		//mobile
		template = Constants.TEMPLATES + "mobile/product.html";
		sw = new StringWriter();
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		htmlFile = Global.getConfig("htmlPath") + "mobile/" + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}


		//pc 热销产品
		fileName = "product-hot.html";
		search = new Product();
		search.setHot("1");
		List<Product> hotList = productService.findList(search);
		context.put("productList", hotList);
		context.put("typeName", "热销产品");
		context.put("typeUrl", fileName);
		template = Constants.TEMPLATES + "product.html";
		sw = new StringWriter();
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		htmlFile = Global.getConfig("htmlPath") + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}

		//mobile
		template = Constants.TEMPLATES + "mobile/product.html";
		sw = new StringWriter();
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		htmlFile = Global.getConfig("htmlPath") + "mobile/" + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}

		//产品类型
		for (Item item : productTypeList) {
			context.put("type", item);
			context.put("typeName", item.getName());
			context.put("typeUrl", fileName);

			search = new Product();
			search.setItemAll(item.getItemId());
			Page<Product> apage1 = productService.findPage(new Page<Product>(1, 6), search);
			int totalPage = apage1.getTotalPage() > 0 ? apage1.getTotalPage() : 1;
			for (int pageNo = 1; pageNo <= totalPage; pageNo++) {
				//pc
				template = Constants.TEMPLATES + "product.html";
				fileName = "product" + item.getItemId() + ".html";
				if (pageNo > 1) {
					fileName = "product" + item.getItemId() + "-" + pageNo + ".html";
				}

				List<Product> productList = productService.findPage(new Page<Product>(pageNo, 6), search).getList();
				context.put("productList", productList);
				context.put("nextPage", "no");
				if (pageNo < totalPage) {
					context.put("nextPage", "product" + item.getItemId() + "-" + (pageNo + 1) + ".html");
				}
				sw = new StringWriter();
				tpl = Velocity.getTemplate(template, Constants.UTF8);
				tpl.merge(context, sw);
				htmlFile = Global.getConfig("htmlPath") + fileName;
				FileUtils.deleteFile(htmlFile);
				if (FileUtils.createFile(htmlFile)) {
					FileUtils.writeToFile(htmlFile, sw.toString(), true);
				}

				//mobile
				template = Constants.TEMPLATES + "mobile/product.html";
				fileName = "mobile/product" + item.getItemId() + ".html";
				if (pageNo > 1) {
					fileName = "mobile/product" + item.getItemId() + "-" + pageNo + ".html";
				}

				sw = new StringWriter();
				tpl = Velocity.getTemplate(template, Constants.UTF8);
				tpl.merge(context, sw);
				htmlFile = Global.getConfig("htmlPath") + fileName;
				FileUtils.deleteFile(htmlFile);
				if (FileUtils.createFile(htmlFile)) {
					FileUtils.writeToFile(htmlFile, sw.toString(), true);
				}

			}


			//二级类型
			for (Item child : item.getChildren()) {
				item = child;
				context.put("type", item);
				context.put("typeName", item.getName());
				context.put("typeUrl", fileName);

				search = new Product();
				search.setItemAll(item.getItemId());
				apage1 = productService.findPage(new Page<Product>(1, 6), search);
				totalPage = apage1.getTotalPage() > 0 ? apage1.getTotalPage() : 1;
				for (int pageNo = 1; pageNo <= totalPage; pageNo++) {
					//pc
					template = Constants.TEMPLATES + "product.html";

					fileName = "product" + item.getItemId() + ".html";
					if (pageNo > 1) {
						fileName = "product" + item.getItemId() + "-" + pageNo + ".html";
					}
					List<Product> productList = productService.findPage(new Page<Product>(pageNo, 6), search).getList();
					context.put("productList", productList);
					context.put("nextPage", "no");
					if (pageNo < totalPage) {
						context.put("nextPage", "product" + item.getItemId() + "-" + (pageNo + 1) + ".html");
					}
					sw = new StringWriter();
					tpl = Velocity.getTemplate(template, Constants.UTF8);
					tpl.merge(context, sw);
					htmlFile = Global.getConfig("htmlPath") + fileName;
					FileUtils.deleteFile(htmlFile);
					if (FileUtils.createFile(htmlFile)) {
						FileUtils.writeToFile(htmlFile, sw.toString(), true);
					}

					//mobile
					template = Constants.TEMPLATES + "mobile/product.html";
					fileName = "mobile/product" + item.getItemId() + ".html";
					if (pageNo > 1) {
						fileName = "mobile/product" + item.getItemId() + "-" + pageNo + ".html";
					}
					sw = new StringWriter();
					tpl = Velocity.getTemplate(template, Constants.UTF8);
					tpl.merge(context, sw);
					htmlFile = Global.getConfig("htmlPath") + fileName;
					FileUtils.deleteFile(htmlFile);
					if (FileUtils.createFile(htmlFile)) {
						FileUtils.writeToFile(htmlFile, sw.toString(), true);
					}

				}
			}

		}

		//每个产品生成一下
		List<Product> productList = productService.findList(new Product());
		for (Product product : productList) {
			product(context, product, topList, false);
			product(context, product, topList, true);
		}
		return JsonMapper.getInstance().toJson("生成成功");
	}


	void product(VelocityContext context, Product product, List<Product> topList, boolean ismobile) {
		String fileName = "product/" + product.getProductId() + ".html";
		String template = Constants.TEMPLATES + "product_detail.html";
		if (ismobile) {
			fileName = "mobile/product/" + product.getProductId() + ".html";
			template = Constants.TEMPLATES + "mobile/product_detail.html";
		}
		Item item = itemService.getByItemId(product.getItemId());
		context = getCrumb(item, context);
		context.put("ctx", "..");
		//推荐
		context.put("topList", topList);
		context.put("product", product);
		StringWriter sw = new StringWriter();
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		String htmlFile = Global.getConfig("htmlPath") + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}
	}


	@RequestMapping(value = "brand")
	@ResponseBody
	public String createBrandHtml() {
		String brandOrctivity = "brand";
		Integer itemId = Constants.ItemId.BRAND;
		VelocityContext context = getContext(itemId);
		List<Item> itemList = itemService.findTreeList(itemId);
		for (Item item : itemList) {
			News news = new News();
			news.setType(Constants.NewsType.BRAND);
			news.setItemId(item.getItemId());
			List<News> newsList = newsService.findList(news);
			context.put("url", brandOrctivity);
			context.put("item", item);
			context.put("list", newsList);
			context.put("ctx", ".");
			context = getCrumb(item, context);
			//pc
			String template = Constants.TEMPLATES + "activity.html";
			StringWriter sw = new StringWriter();
			Template tpl = Velocity.getTemplate(template, Constants.UTF8);
			tpl.merge(context, sw);
			String fileName = brandOrctivity + item.getItemId() + ".html";
			String htmlFile = Global.getConfig("htmlPath") + fileName;
			FileUtils.deleteFile(htmlFile);
			if (FileUtils.createFile(htmlFile)) {
				FileUtils.writeToFile(htmlFile, sw.toString(), true);
			}
			//mobile
			template = Constants.TEMPLATES + "mobile/activity.html";
			sw = new StringWriter();
			tpl = Velocity.getTemplate(template, Constants.UTF8);
			tpl.merge(context, sw);
			htmlFile = Global.getConfig("htmlPath") + "mobile/" + fileName;
			FileUtils.deleteFile(htmlFile);
			if (FileUtils.createFile(htmlFile)) {
				FileUtils.writeToFile(htmlFile, sw.toString(), true);
			}
			for(News news1:newsList){
				brand(news1, context, brandOrctivity);
			}


		}

		return JsonMapper.getInstance().toJson("生成成功");
	}

	public void brand(News news, VelocityContext context, String brandOrctivity) {
		context.put("news", news);
		context.put("ctx", "..");
		context.put("url", brandOrctivity);
		Item item = itemService.getByItemId(news.getItemId());
		context = getCrumb(item, context);
		context.put("item", item);

		//pc
		StringWriter sw = new StringWriter();
		String fileName = brandOrctivity + "/" + news.getNewsId() + ".html";
		String template = Constants.TEMPLATES + "activity_detail.html";
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		String htmlFile = Global.getConfig("htmlPath") + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}
		//mobile
		sw = new StringWriter();
		template = Constants.TEMPLATES + "mobile/activity_detail.html";
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		htmlFile = Global.getConfig("htmlPath") + "mobile/" + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}

	}


	@RequestMapping(value = "activity")
	@ResponseBody
	public String createActivityHtml() {
		String brandOrctivity = "activity";
		Integer itemId =  Constants.ItemId.ACTIVITY;
		VelocityContext context = getContext(itemId);
		List<Item> itemList = itemService.findTreeList(itemId);
		for (Item item : itemList) {
			News news = new News();
			news.setType(Constants.NewsType.ACTIVITY);
			news.setItemId(item.getItemId());
			List<News> newsList = newsService.findList(news);
			context.put("url", brandOrctivity);
			context.put("item", item);
			context.put("list", newsList);
			context.put("ctx", ".");
			context = getCrumb(item, context);
			//pc
			String template = Constants.TEMPLATES + "activity.html";
			StringWriter sw = new StringWriter();
			Template tpl = Velocity.getTemplate(template, Constants.UTF8);
			tpl.merge(context, sw);
			String fileName = brandOrctivity + item.getItemId() + ".html";
			String htmlFile = Global.getConfig("htmlPath") + fileName;
			FileUtils.deleteFile(htmlFile);
			if (FileUtils.createFile(htmlFile)) {
				FileUtils.writeToFile(htmlFile, sw.toString(), true);
			}
			//mobile
			template = Constants.TEMPLATES + "mobile/activity.html";
			sw = new StringWriter();
			tpl = Velocity.getTemplate(template, Constants.UTF8);
			tpl.merge(context, sw);
			htmlFile = Global.getConfig("htmlPath") + "mobile/" + fileName;
			FileUtils.deleteFile(htmlFile);
			if (FileUtils.createFile(htmlFile)) {
				FileUtils.writeToFile(htmlFile, sw.toString(), true);
			}

			for(News news1:newsList){
				brand(news1, context, brandOrctivity);
			}
		}

		return JsonMapper.getInstance().toJson("生成成功");
	}


	/***
	 * 新闻动态
	 * @return
	 */
	@RequestMapping(value = "news")
	@ResponseBody
	public String createNewsHtml() {
		Integer itemId = Constants.ItemId.NEWS;
		VelocityContext context = getContext(itemId);
		itemId = 41;
		Item item = itemService.getByItemId(itemId);

		News news = new News();
		news.setItemId(411);
		Page<News> apage1 = new Page<News>(1, 6);
		apage1 = newsService.findPage(apage1, news);
		context.put("item", item);
		context = this.getCrumb(item, context);
		context.put("gfNewsList", apage1.getList());

		context.put("nextPage1", "no");
		if (apage1.getTotalPage() >= 2) {
			context.put("nextPage", "news411-2.html");
		}

		news.setItemId(412);
		Page<News> apage2 = new Page<News>(1, 6);
		apage2 = newsService.findPage(apage2, news);
		context.put("mediaNewsList", apage2.getList());
		context.put("nextPage2", "no");
		if (apage2.getTotalPage() >= 2) {
			context.put("nextPage2", "news412-2.html");
		}

		news.setItemId(413);
		Page<News> apage3 = new Page<News>(1, 6);
		apage3 = newsService.findPage(apage3, news);
		context.put("industryNewsList", apage3.getList());
		context.put("nextPage3", "no");
		if (apage3.getTotalPage() >= 2) {
			context.put("nextPage3", "news413-2.html");
		}


		//pc
		StringWriter sw = new StringWriter();
		String template = Constants.TEMPLATES + "news.html";
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		String fileName = "news.html";
		String htmlFile = Global.getConfig("htmlPath") + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}
		//mobile
		sw = new StringWriter();
		template = Constants.TEMPLATES + "mobile/news.html";
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		fileName = "mobile/news.html";
		htmlFile = Global.getConfig("htmlPath") + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}

		List<Item> childItemList = getItemList(itemId);
		for (Item childItem : childItemList) {
			item = childItem;
			context.put("item", item);
			news = new News();
			news.setItemId(item.getItemId());
			Page<News> itemPage = new Page<News>(1, 6);
			itemPage = newsService.findPage(itemPage, news);
			int totalPage = itemPage.getTotalPage() > 0 ? itemPage.getTotalPage() : 1;
			for (int pageNo = 1; pageNo <= totalPage; pageNo++) {
				itemPage = new Page<News>(pageNo, 6);
				List<News> newsList = newsService.findPage(itemPage, news).getList();
				context.put("newsList", newsList);
				context = this.getCrumb(item, context);

				template = Constants.TEMPLATES + "news_list.html";
				fileName = "news" + item.getItemId() + "-" + pageNo + ".html";
				context.put("nextPage", "no");
				if (pageNo < totalPage) {
					context.put("nextPage", "news" + item.getItemId() + "-" + (pageNo + 1) + ".html");
				}
				if (pageNo == 1) {
					fileName = "news" + item.getItemId() + ".html";
				}

				sw = new StringWriter();
				tpl = Velocity.getTemplate(template, Constants.UTF8);
				tpl.merge(context, sw);
				htmlFile = Global.getConfig("htmlPath") + fileName;
				FileUtils.deleteFile(htmlFile);
				if (FileUtils.createFile(htmlFile)) {
					FileUtils.writeToFile(htmlFile, sw.toString(), true);
				}

				//mobile
				sw = new StringWriter();
				template = Constants.TEMPLATES + "mobile/news_list.html";
				tpl = Velocity.getTemplate(template, Constants.UTF8);
				tpl.merge(context, sw);
				htmlFile = Global.getConfig("htmlPath") + "mobile/" + fileName;
				FileUtils.deleteFile(htmlFile);
				if (FileUtils.createFile(htmlFile)) {
					FileUtils.writeToFile(htmlFile, sw.toString(), true);
				}
			}


			news = new News();
			news.setItemId(childItem.getItemId());
			List<News> allList = newsService.findList(news);
			for (News news2 : allList) {
				news(news2, context,childItem,true);
				news(news2, context,childItem,false);
			}
		}
		return JsonMapper.getInstance().toJson("生成成功");
	}



	private void news(News news,VelocityContext context,Item item,boolean ismobile){
		String fileName ="news/"+news.getNewsId()+".html";
		String template=Constants.TEMPLATES+"news_detail.html";
		if(ismobile){
			fileName ="mobile/news/"+news.getNewsId()+".html";
			template=Constants.TEMPLATES+"mobile/news_detail.html";
		}

		context.put("news", news);
		context.put("ctx", "..");
		context = this.getCrumb(item, context);
		context.put("item", item);
		StringWriter sw = new StringWriter();
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		String htmlFile = Global.getConfig("htmlPath")+fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)){
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}
	}


	@RequestMapping(value = "service")
	@ResponseBody
	public String createServiceHtml() {
        Integer itemId = 5;
		VelocityContext context = this.getContext(itemId);
		News news = new News();
		news.setType(Constants.NewsType.COURSE);
		Page<News> apage = new Page<News>(1, 6);
		List<News> courseList = newsService.findPage(apage, news).getList();
		//教程
		context.put("service1", new News());
		if (courseList != null && courseList.size() > 0) {
			context.put("service1", courseList.get(0));
			courseList = courseList.subList(1, courseList.size());
		}

		news.setType(Constants.NewsType.SERVICE);
		news.setItemId(53);
		//政策指导
		List<News> policyList = newsService.findPage(apage, news).getList();
		context.put("service2", new News());
		if (policyList != null && policyList.size() > 0) {
			context.put("service2", policyList.get(0));
			policyList = policyList.subList(1, policyList.size());
		}

		news.setType(Constants.NewsType.SERVICE);
		news.setItemId(54);
		//学员天地
		List<News> studyList = newsService.findPage(apage, news).getList();
		context.put("service3", new News());
		if (studyList != null && studyList.size() > 0) {
			context.put("service3", studyList.get(0));
			studyList = studyList.subList(1, studyList.size());
		}

		context.put("courseList", courseList);
		context.put("policyList", policyList);
		context.put("studyList", studyList);


		//pc
		String template = Constants.TEMPLATES + "service_index.html";
		String fileName = "service.html";
		StringWriter sw = new StringWriter();
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		String htmlFile = Global.getConfig("htmlPath") + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}

		//mobile
		template = Constants.TEMPLATES + "mobile/service_index.html";
		fileName = "mobile/service.html";
		sw = new StringWriter();
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		htmlFile = Global.getConfig("htmlPath") + fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)) {
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}

		return JsonMapper.getInstance().toJson("生成成功");
	}


}