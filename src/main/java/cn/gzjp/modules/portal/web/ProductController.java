package cn.gzjp.modules.portal.web;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.constant.Constants;
import cn.gzjp.common.mapper.JsonMapper;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.utils.*;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.portal.entity.*;
import cn.gzjp.modules.portal.service.ProductService;
import com.xiaoleilu.hutool.http.HtmlUtil;
import com.xiaoleilu.hutool.util.ObjectUtil;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;

/**
 * 产品Controller
 * @author zengcp
 * @version 2017-08-17
 */
@Controller
@RequestMapping(value = "${adminPath}/portal/product")
public class ProductController extends BaseController {

	@Autowired
	private ProductService productService;
	
	

	@RequestMapping(value = {"list", ""})
	public String list(Product product, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Product> apage = new Page<Product>(request, response);
		//apage.setOrderBy("a.create_date DESC");

		Page<Product> page = productService.findPage(apage, product);
		model.addAttribute("page", page);

		model.addAttribute("product", product);
		return "modules/portal/productList";
	}


	@RequestMapping(value = "form")
	public String form(Product product, Model model) {
		if(product.getProductId()!=0){
			product = productService.get(product.getProductId());
		}
		model.addAttribute("product", product);
		return "modules/portal/productForm";
	}


	/**
	 * 保存
	 */
	@RequestMapping(value = "save")
	public String save(Product product, Model model, RedirectAttributes redirectAttributes) throws Exception{
		product.setContent(Encodes.unescapeHtml(product.getContent()));
		if(StringUtils.isNoneBlank(product.getContent())&&!product.getContent().startsWith("<p")){
			String content = HtmlUtil.cleanHtmlTag(product.getContent());
			product.setContent(content);
		}
		if (!beanValidator(model, product)){
			return form(product,model);
		}
		if(product.getCreateDate()==null){
			product.setCreateDate(new Date());//新增

		}else{
			product.setUpdateDate(new Date());
		}
		productService.save(product);//保存
		addMessage(redirectAttributes, "保存成功");
		return redirect("/portal/product/");
	}
	

	/**
	 * 删除新闻资讯
	 */
	@RequestMapping(value = "delete")
	public String delete(Product product, RedirectAttributes redirectAttributes) {
		productService.delete(product);
		addMessage(redirectAttributes, "删除成功");
		return redirect("/portal/product/");
	}
	
	@ModelAttribute
	public Product get(@RequestParam(required=false) String id) {
		Product entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = productService.get(id);
		}
		if (entity == null){
			entity = new Product();
		}
		return entity;
	}


}