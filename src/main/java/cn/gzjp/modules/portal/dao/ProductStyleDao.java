package cn.gzjp.modules.portal.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.portal.entity.Product;
import cn.gzjp.modules.portal.entity.ProductStyle;

/**
 * 产品DAO接口
 * @author zengcp
 * @version 2017-08-15
 */
@MyBatisDao
public interface ProductStyleDao extends CrudDao<ProductStyle> {

}