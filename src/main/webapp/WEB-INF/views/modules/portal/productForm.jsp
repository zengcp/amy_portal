<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>全系产品管理</title>
	<meta name="decorator" content="default"/>
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/webuploader-0.1.5/webuploader.css">
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/webuploader-0.1.5/demo.css">
	  <script type="text/javascript">
    // 添加全局站点信息
    var BASE_URL = '/webuploader';
    </script>
	<script type="text/javascript" src="${ctxStatic}/webuploader-0.1.5/webuploader.js"></script>
	<script type="text/javascript" src="${ctxStatic}/jquery-jcrop/jquery.Jcrop.min.js"></script>
    <link rel="stylesheet" href="${ctxStatic}/jquery-jcrop/jquery.Jcrop.min.css" type="text/css" />
	<script type="text/javascript" charset="utf-8" src="${ctxStatic}/ueditor/ueditor.config.js"></script>
	<script type="text/javascript" charset="utf-8" src="${ctxStatic}/ueditor/ueditor.all.min.js"> </script>
	<script type="text/javascript">
        var toolbars= [[
            'fullscreen', 'source', '|',
            'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'removeformat', 'formatmatch',  'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|',
            'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
            'paragraph', 'fontfamily', 'fontsize', '|',
            'indent', '|',
            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
            'link',  '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
            'simpleupload', 'insertimage', 'emotion', 'insertvideo',  'attachment',    '|',
            'horizontal', 'spechars', 'preview'
        ]];
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}

		$(document).ready(function() {

			<c:if test="${empty product.pic}">
				$("#prePic").hide();
			</c:if>
            <c:if test="${empty product.pic2}">
            $("#prePic2").hide();
            </c:if>
            var ueditor1 = UE.getEditor('editor1',{
                scaleEnabled :true,
                toolbars: toolbars
            });

            ueditor1.addListener( 'ready', function( editor ) {
                ueditor1.setContent('${product.content}');
            } );
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
		});

        function addRow(list, idx, tpl, row){
            $(list).append(Mustache.render(tpl, {
                idx: idx, delBtn: true, row: row
            }));
            $(list+idx).find("select").each(function(){
                $(this).val($(this).attr("data-value"));
            });
            $(list+idx).find("input[type='checkbox'], input[type='radio']").each(function(){
                var ss = $(this).attr("data-value").split(',');
                for (var i=0; i<ss.length; i++){
                    if($(this).val() == ss[i]){
                        $(this).attr("checked","checked");
                    }
                }
            });
        }
        function delRow(obj, prefix){
            var id = $(prefix+"_id");
            var delFlag = $(prefix+"_delFlag");
            if (id.val() == ""){
                $(obj).parent().parent().remove();
            }else if(delFlag.val() == "0"){
                delFlag.val("1");
                $(obj).html("&divide;").attr("title", "撤销删除");
                $(obj).parent().parent().addClass("error");
            }else if(delFlag.val() == "1"){
                delFlag.val("0");
                $(obj).html("&times;").attr("title", "删除");
                $(obj).parent().parent().removeClass("error");
            }
        }
	</script>


</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="product" action="${ctx}/portal/product/save" method="post" class="form-horizontal">
		<form:hidden path="productId"/>
		<form:hidden path="pic"/>
		<form:hidden path="pic2"/>

		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>

			  <tr>
				  <td class="width-15 active"><label class="pull-right">产品名称：</label></td>
				  <td class="width-35" colspan="3">
					  <form:input path="title" htmlEscape="false"  maxlength="128"    class="form-control required"/>
				  </td>
			  </tr>
			    <tr>

				   <td class="width-15 active"><label class="pull-right">产品分类：</label></td>
				   <td class="width-35">
					   <sys:treeselect id="item" name="itemId" value="${product.itemId}" labelName="product.itemName" labelValue="${product.itemName}"
									   title="类型" url="/portal/item/treeData?itemId=2" extId="${itemId}" cssClass="form-control required" allowClear="true" notAllowSelectParent="true"/>
				     </td>
				   <td class="width-15 active"><label class="pull-right">购买链接：</label></td>
				   <td class="width-35">
					   <form:input path="url" htmlEscape="false"  maxlength="256"     class="form-control url required"/>
				   </td>

			   </tr>
			   <tr>
				   <td class="width-15 active"><label class="pull-right">小图：</label></td>
				   <td class="width-35" >
					   <img id="prePic" src="${product.pic}" width="320px"/>
					   <div id="uploader-demo">
						   <div class="progress-small" id="productPicDiv" style="display: none;">
							   <div id="productPic" class="progress-bar" role="progressbar" style="width: 0%;">
							   </div>
						   </div>
						   <br/>
						   <div id="picPicker">选择图片</div>
					   </div>
				   </td>
				   <td class="width-15 active"><label class="pull-right">大图：</label></td>
				   <td class="width-35" >
					   <img id="prePic2" src="${product.pic2}" width="320px"/>
					   <div id="uploader-demo2">
						   <div class="progress-small" id="productPicDiv2" style="display: none;">
							   <div id="productPic2" class="progress-bar" role="progressbar" style="width: 0%;">
							   </div>
						   </div>
						   <br/>
						   <div id="picPicker2">选择图片</div>
					   </div>
				   </td>
			   </tr>

			  <tr>
				  <td class="width-15 active"><label class="pull-right">详细图：</label></td>
				  <td class="width-35">
					  <form:hidden id="pics" path="pics" htmlEscape="false" maxlength="1024" class="form-control required"/>
					  <sys:ckfinder input="pics" type="images" uploadPath="/product/pics" selectMultiple="true"/>
				  </td>
				  <td class="width-15 active"><label class="pull-right">产品规格：</label></td>
				  <td class="width-35" >
				  <div class="tab-content">
					  <div id="tab-1" class="tab-pane active">
						  <a class="btn btn-primary btn-sm" onclick="addRow('#styleList', styleRowIdx, styleTpl);styleRowIdx = styleRowIdx + 1;" title="添加巡查内容"><i class="fa fa-plus"></i> 新增产品规格</a>
						  <table id="contentTable" class="table table-striped table-bordered table-condensed">

							  <tbody id="styleList">

							  </tbody>
						  </table>
						  <script type="text/template" id="styleTpl">//<!--
						   <tr id="styleList{{idx}}">
							<td class="hide">
								<input id="styleList{{idx}}_id" name="styleList[{{idx}}].id" type="hidden" value="{{row.id}}"/>
								<input id="styleList{{idx}}_delFlag" name="styleList[{{idx}}].delFlag" type="hidden" value="0"/>
							</td>

							<td>
								<input id="styleList{{idx}}_name"  name="styleList[{{idx}}].name" type="text" value="{{row.name}}"    class="form-control required"/>
							</td>

							<td class="text-center" width="10">
								{{#delBtn}}<span class="close" onclick="delRow(this, '#styleList{{idx}}')" title="删除">&times;</span>{{/delBtn}}
							</td>
						</tr>//-->



						  </script>
						  <script type="text/javascript">
                              var styleRowIdx = 0, styleTpl = $("#styleTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
                              $(document).ready(function() {
                                  var data = ${fns:toJson(product.styleList)};
                                  for (var i=0; i<data.length; i++){
                                      addRow('#styleList', styleRowIdx, styleTpl, data[i]);
                                      styleRowIdx = styleRowIdx + 1;
                                  }
                              });
						  </script>

					  </div>
				  </div>
				  </td>
			  </tr>

		        <tr>

					<td class="width-15 active"><label class="pull-right">明星产品：</label></td>
					<td class="width-35">
						<form:select path="star" class="form-control required" >
							<form:options items="${fns:getDictList('home_position')}" itemLabel="label"
										  itemValue="value" htmlEscape="false" />
						</form:select>
					</td>

					<td class="width-15 active"><label class="pull-right">向你推荐：</label></td>
					<td class="width-35">
						<form:select path="top" class="form-control required" >
							<form:options items="${fns:getDictList('home_position')}" itemLabel="label"
										  itemValue="value" htmlEscape="false" />
						</form:select>
					</td>
			   </tr>
			  <tr>

				  <td class="width-15 active"><label class="pull-right">热销：</label></td>
				  <td class="width-35">
					  <form:select path="hot" class="form-control required" >
						  <form:options items="${fns:getDictList('home_position')}" itemLabel="label"
										itemValue="value" htmlEscape="false" />
					  </form:select>
				  </td>
			  </tr>
			   <tr>
				   <td class="width-15 active"><label class="pull-right">原价：</label></td>
				   <td class="width-35">
					   <form:input path="price" htmlEscape="false"  maxlength="8"    class="form-control  required"/>
				   </td>


				   <td class="width-15 active"><label class="pull-right">现价：</label></td>
				   <td class="width-35">
					   <form:input path="price2" htmlEscape="false"  maxlength="8"    class="form-control  required"/>
				   </td>

			   </tr>

			   <tr>
				   <td class="width-15 active"><label class="pull-right">产品摘要：</label></td>
				   <td class="width-35" colspan="3">
					   <form:textarea path="summary" htmlEscape="false" rows="3"    class="form-control "/>
				   </td>
			   </tr>

			   <tr class="zh">
				   <td class="width-15 active"><label class="pull-right">产品详情：</label></td>
				   <td class="width-35" colspan="3">
					   <script id="editor1" name="content" type="text/plain" htmlEscape="false"  style="height:450px;width: 100%">
					   </script>
                       </td>
                       </tr>
					   </tbody>
		</table>



	</form:form>

<script>
var uploader1 = WebUploader.create({

    // 选完文件后，是否自动上传。
    auto: true,

    // swf文件路径
    swf: BASE_URL + '/js/Uploader.swf',

    // 文件接收服务端。1940*540
    server: '${ctx}/portal/banner/imageUpload',

    // 选择文件的按钮。可选。
    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
    pick: '#picPicker',
    compress: false,//不启用压缩
    resize: false,//尺寸不改变
    fileSizeLimit: 5 * 1024 * 1024,    // 10 M
    fileSingleSizeLimit: 5 * 1024 * 1024 ,   // 10 M
    // 只允许选择图片文件。
    accept: {
        title: 'Images',
        extensions: 'gif,jpg,jpeg,png',
        mimeTypes: 'image/jpg,image/jpeg,image/png'
    }
});


//文件上传过程中创建进度条实时显示。
uploader1.on( 'uploadStart', function( file) {
	$('#productPicDiv').show();
    $('#productPic').css( 'width', '0%' );
});

//文件上传过程中创建进度条实时显示。
uploader1.on( 'uploadProgress', function( file, percentage ) {
	$('#productPicDiv').show();
    $('#productPic').css( 'width', percentage * 100 + '%' );
});

// 文件上传成功，给item添加成功class, 用样式标记上传成功。
uploader1.on( 'uploadSuccess', function( file,response ) {
    if(response.success){
    	$("#pic").val(response.msg);
    	$("#prePic").show();
    	$("#prePic").attr('src',response.msg);

    	layer.msg("上传完成");
    }
});

//文件上传失败，显示上传出错。
uploader1.on( 'uploadError', function( file ) {
    top.layer.msg("图片上传失败,请重新上传", {icon: 2});
});

uploader1.on("error", function (type) {
    if (type == "Q_TYPE_DENIED") {
        layer.msg("请上传JPG、PNG、GIF、jpeg格式文件");
    } else if (type == "Q_EXCEED_SIZE_LIMIT") {
        layer.msg("文件大小不能超过5M");
    }else {
        layer.msg("上传出错！请检查后重新上传！错误代码"+type);
    }
});


   var uploader2 = WebUploader.create({

	   // 选完文件后，是否自动上传。
	   auto: true,

	   // swf文件路径
	   swf: BASE_URL + '/js/Uploader.swf',

	   // 文件接收服务端。1940*540
	   server: '${ctx}/portal/banner/imageUpload',

	   // 选择文件的按钮。可选。
	   // 内部根据当前运行是创建，可能是input元素，也可能是flash.
	   pick: '#picPicker2',
	   compress: false,//不启用压缩
	   resize: false,//尺寸不改变
	   fileSizeLimit: 5 * 1024 * 1024,    // 10 M
	   fileSingleSizeLimit: 5 * 1024 * 1024 ,   // 10 M
	   // 只允许选择图片文件。
	   accept: {
		   title: 'Images',
		   extensions: 'gif,jpg,jpeg,png',
		   mimeTypes: 'image/jpg,image/jpeg,image/png'
	   }
   });


   //文件上传过程中创建进度条实时显示。
   uploader2.on( 'uploadStart', function( file) {
	   $('#productPicDiv2').show();
	   $('#productPic2').css( 'width', '0%' );
   });

   //文件上传过程中创建进度条实时显示。
   uploader2.on( 'uploadProgress', function( file, percentage ) {
	   $('#productPicDiv2').show();
	   $('#productPic2').css( 'width', percentage * 100 + '%' );
   });

   // 文件上传成功，给item添加成功class, 用样式标记上传成功。
   uploader2.on( 'uploadSuccess', function( file,response ) {
	   if(response.success){
		   $("#pic2").val(response.msg);
		   $("#prePic2").show();
		   $("#prePic2").attr('src',response.msg);

		   layer.msg("上传完成");
	   }
   });

   //文件上传失败，显示上传出错。
   uploader2.on( 'uploadError', function( file ) {
	   top.layer.msg("图片上传失败,请重新上传", {icon: 2});
   });

   uploader2.on("error", function (type) {
	   if (type == "Q_TYPE_DENIED") {
		   layer.msg("请上传JPG、PNG、GIF、jpeg格式文件");
	   } else if (type == "Q_EXCEED_SIZE_LIMIT") {
		   layer.msg("文件大小不能超过5M");
	   }else {
		   layer.msg("上传出错！请检查后重新上传！错误代码"+type);
	   }
   });

</script>
</body>
</html>