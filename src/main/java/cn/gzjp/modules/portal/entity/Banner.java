package cn.gzjp.modules.portal.entity;

import org.hibernate.validator.constraints.Length;

import cn.gzjp.common.persistence.DataEntity;
import cn.gzjp.common.utils.excel.annotation.ExcelField;

/**
 * 轮播图Entity
 * @author zengcp
 * @version 2017-08-22
 */
public class Banner extends DataEntity<Banner> {
	
	private static final long serialVersionUID = 1L;
	private String pic;		// 图片地址
	private int sort;		// 顺序
	private String url;		// 链接地址
	private String type;// 0 banner轮播 1企业价值观
	
	private String w1;
	private String w2;
	

	
	public Banner() {
		super();
	}

	public Banner(String id){
		super(id);
	}

	@Length(min=0, max=256, message="图片地址长度必须介于 0 和 256 之间")
	@ExcelField(title="图片地址", align=2, sort=1)
	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}
	
	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}
	
	@Length(min=0, max=256, message="链接地址长度必须介于 0 和 256 之间")
	@ExcelField(title="链接地址", align=2, sort=4)
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getW1() {
		return w1;
	}

	public void setW1(String w1) {
		this.w1 = w1;
	}

	public String getW2() {
		return w2;
	}

	public void setW2(String w2) {
		this.w2 = w2;
	}

}