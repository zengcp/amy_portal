package cn.gzjp.common.constant;

public interface Constants {
	

	public interface NewsType{
		public final static String PRODUCT = "2";
		public final static String BRAND = "3";
		public final static String NEWS = "4";//新闻动态
		public final static String SERVICE = "5";
		public final static String ACTIVITY = "6";
		public final static String COURSE = "9";// 教程专区




	}
	


	public interface Status{
		public final static String START = "0";//启用
		public final static String STOP = "1";//暂停
	}
	/**
	 * UTF-8 字符集
	 */
	public static final String UTF8 = "UTF-8";


	public static final String TEMPLATES = "templates/html/";



	public interface  ItemId{
		public final static Integer PRODUCT = 2;
		public final static Integer BRAND = 3;
		public final static Integer NEWS = 4;
		public final static Integer SERVICE = 5;
		public final static Integer ACTIVITY = 6;
	}

}
