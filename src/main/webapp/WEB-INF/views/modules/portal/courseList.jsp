<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>教程专区管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">

    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="news" action="${ctx}/portal/course/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
			<span>标题：</span>
				<form:input path="title" htmlEscape="false" maxlength="256"  class=" form-control input-sm"/>

			<span>教程分类：</span>
			<form:select path="smallType" class="form-control" >
				<option></option>
				<form:options items="${fns:getDictList('course_type')}" itemLabel="label"
							  itemValue="value" htmlEscape="false" />
			</form:select>
			<div class="pull-right">
				<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>
			</div>
		 </div>	
	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
				<table:addRow url="${ctx}/portal/course/form" title="教程" width="100%" height="100%" label="添加教程"></table:addRow><!-- 增加按钮 -->
			<button type="button" onclick="createIndexHtml()"  class="btn btn-primary btn-xs">生成html</button>
		</div>
		
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th>教程分类</th>
				<th>教程名称</th>
				<th>发布日期</th>
				<th>创建时间</th>
				<th width="20%">操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="news">
			<tr>
				<td>
						${fns:getDictLabel(news.smallType, 'course_type', '')}
				</td>
				<td>
					${news.title}
				</td>

				<td>
					<fmt:formatDate value="${news.date}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					<fmt:formatDate value="${news.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>

				<td>
    					<a href="#" onclick="openDialog('修改', '${ctx}/portal/course/form?newsId=${news.newsId}','100%', '100%')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>
						<a href="${ctx}/portal/course/delete?newsId=${news.newsId}" onclick="return confirmx('确认要删除吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>

				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
<script type="text/javascript">

    function createHtml(id){
        layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
            $.get('${ctx}/portal/course/createHtml?id='+id,function(result){layer.msg('操作成功', {time: 2000}); search(); });
        });
    }

    function createIndexHtml(){
        layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
            $.get('${ctx}/portal/course/createIndexHtml',function(result){layer.msg('操作成功', {time: 2000}); search(); });
        });
    }
</script>
</body>
</html>