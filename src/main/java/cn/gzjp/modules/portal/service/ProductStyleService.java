package cn.gzjp.modules.portal.service;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.portal.dao.ProductStyleDao;
import cn.gzjp.modules.portal.entity.ProductStyle;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
@Transactional(readOnly = true)
public class ProductStyleService extends CrudService<ProductStyleDao, ProductStyle> {


	public List<ProductStyle> findList(ProductStyle partner) {
		return super.findList(partner);
	}

	public Page<ProductStyle> findPage(Page<ProductStyle> page, ProductStyle partner) {
		return super.findPage(page, partner);
	}


	@Transactional(readOnly = false)
	public void delete(ProductStyle partner) {
		super.deleteByLogic(partner);
	}



	@Transactional(readOnly = false)
	public void save(ProductStyle news) {
		super.save(news);
	}
    @Override
	public ProductStyle get(String id) {
		return super.get(id);
	}
	
}