package cn.gzjp.modules.portal.entity;


import cn.gzjp.common.config.Global;
import cn.gzjp.common.persistence.DataEntity;
import com.google.common.collect.Lists;
import com.xiaoleilu.hutool.util.StrUtil;

import java.util.Arrays;
import java.util.List;

/**
 * 产品
 * @author zengcp
 * @version 2017-08-15
 */
public class Product extends DataEntity<Product> {
	
	private static final long serialVersionUID = 1L;

	private long productId;

	/**栏目id**/
	private Integer itemId;

	private String itemName;

	private String title;		// 标题
	private String summary;  //摘要
	private String content;		// 内容
	private int sort;		// 排序
	private String price; // 原价格
	private String price2;//优惠价
	private String url;

	private Integer itemAll;//类型，包含父类型；



	private String pic2;//小图
	private String top;//推荐 1 为设置  0不设置
	private String hot;//热销 1是
	private String star;//明星产品 1 为设置  0不设置

	private String pics;		//明细图


	private List<ProductStyle> styleList;




	public Product() {
		super();
	}

	public Product(String id){
		super(id);
	}



	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}


	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getPrice2() {
		return price2;
	}

	public void setPrice2(String price2) {
		this.price2 = price2;
	}


	public String getStar() {
		return star;
	}

	public void setStar(String star) {
		this.star = star;
	}

	public String getPic2() {
		return pic2;
	}

	public void setPic2(String pic2) {
		this.pic2 = pic2;
	}

	public String getPic2Url(){
		if(StrUtil.isNotBlank(this.getPic2())){
			return Global.getConfig("frontUrl")+this.getPic2();
		}
		return "";
	}

	public String getTop() {
		return top;
	}

	public void setTop(String top) {
		this.top = top;
	}

	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}


	public String getPics() {
		return pics;
	}

	public void setPics(String pics) {
		this.pics = pics;
	}

	public List<String> getPicsList() {
		if(StrUtil.isNotBlank(this.getPics())){
			String pics[] = this.getPics().split("\\|");
			List<String> picsList = Arrays.asList(pics);
			List<String> result = Lists.newArrayList();
			for(String pic:picsList){
				if(StrUtil.isNotBlank(pic)){
					result.add(Global.getConfig("frontUrl")+pic);
				}
			}
			return result;
		}
		return null;
	}

	public String getHot() {
		return hot;
	}

	public void setHot(String hot) {
		this.hot = hot;
	}

	public List<ProductStyle> getStyleList() {
		return styleList;
	}

	public void setStyleList(List<ProductStyle> styleList) {
		this.styleList = styleList;
	}


	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Integer getItemAll() {
		return itemAll;
	}

	public void setItemAll(Integer itemAll) {
		this.itemAll = itemAll;
	}
}