package cn.gzjp.modules.portal.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.portal.entity.Profile;

/**
 * 集团概况DAO接口
 * @author zengcp
 * @version 2017-08-14
 */
@MyBatisDao
public interface ProfileDao extends CrudDao<Profile> {
	
}