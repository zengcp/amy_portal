package cn.gzjp.modules.portal.web;

import java.io.StringWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.constant.Constants;
import cn.gzjp.common.utils.FileUtils;
import cn.gzjp.common.utils.VelocityInitializer;
import cn.gzjp.modules.portal.entity.Item;
import com.xiaoleilu.hutool.util.ObjectUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.xiaoleilu.hutool.http.HtmlUtil;

import cn.gzjp.common.utils.Encodes;
import cn.gzjp.common.constant.Constants.Status;
import cn.gzjp.common.mapper.JsonMapper;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.modules.portal.entity.News;
import cn.gzjp.modules.portal.service.NewsService;

/**
 * 服务中心Controller
 * @author zengcp
 * @version 2017-08-17
 */
@Controller
@RequestMapping(value = "${adminPath}/portal/service")
public class ServiceController extends BaseController {

	@Autowired
	private NewsService newsService;



	@RequestMapping(value = {"list", ""})
	public String list(News news,Integer itemId, HttpServletRequest request, HttpServletResponse response, Model model) {
		news.setType(Constants.NewsType.SERVICE);
		if(ObjectUtil.isNotNull(itemId)){
			news.setItemId(itemId);
			Item item = itemService.getByItemId(itemId);
			model.addAttribute("item", item);
		}
		Page<News> apage = new Page<News>(request, response);
		Page<News> page = newsService.findPage(apage, news);
		model.addAttribute("page", page);
		model.addAttribute("itemId", itemId);
		return "modules/portal/serviceList";
	}

	/**
	 * 查看，增加，编辑新闻资讯表单页面
	 */
	@RequestMapping(value = "form")
	public String form(News news,Integer itemId, Model model) {
		news.setType(Constants.NewsType.SERVICE);
		news.setItemId(itemId);
		if(news.getNewsId()!=0){
			news = newsService.get(news.getNewsId());
		}
		model.addAttribute("news", news);
		return "modules/portal/serviceForm";
	}



	@RequestMapping(value = "save")
	public String save(News news, Model model, RedirectAttributes redirectAttributes) throws Exception{
		news.setContent(Encodes.unescapeHtml(news.getContent()));
		if(StringUtils.isNoneBlank(news.getContent())&&!news.getContent().startsWith("<p")){
			String content = HtmlUtil.cleanHtmlTag(news.getContent());
			news.setContent(content);
		}

		if(news.getCreateDate()==null){
			news.setCreateDate(new Date());//新增

		}else{
			news.setUpdateDate(new Date());
		}
		newsService.save(news);//保存
		addMessage(redirectAttributes, "保存成功");
		return redirect("/portal/service?itemId="+news.getItemId());
	}



	/**
	 * 删除
	 */
	@RequestMapping(value = "delete")
	public String delete(News news, RedirectAttributes redirectAttributes) {
		newsService.delete(news);
		addMessage(redirectAttributes, "删除成功");
		return redirect("/portal/service?itemId="+news.getItemId());
	}

	@ModelAttribute
	public News get(@RequestParam(required=false) String id) {
		News entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = newsService.get(id);
		}
		if (entity == null){
			entity = new News();
		}
		return entity;
	}


	@RequestMapping(value = "createHtml")
	@ResponseBody
	public String createHtml(Integer itemId){
		Item item = itemService.getByItemId(itemId);
		String templateFile = "service_list.html";
		String pre = "service";
		VelocityContext context = getContext(item.getParentItemId());
		context.put("item", item);
		News news = new News();
		news.setItemId(itemId);
		news = new News();
		news.setItemId(item.getItemId());
		Page<News> itemPage = new Page<News>(1, 6);
		itemPage = newsService.findPage(itemPage, news);
		int totalPage = itemPage.getTotalPage() > 0 ? itemPage.getTotalPage() : 1;
		for (int pageNo = 1; pageNo <= totalPage; pageNo++) {
			itemPage = new Page<News>(pageNo, 6);
			List<News> newsList = newsService.findPage(itemPage, news).getList();
			context.put("newsList", newsList);
			context = this.getCrumb(item, context);

			String template = Constants.TEMPLATES + templateFile;
			String fileName = pre + item.getItemId() + "-" + pageNo + ".html";
			if (pageNo == 1) {
				fileName = pre + item.getItemId() + ".html";
			}
			context.put("nextPage", "no");
			if (pageNo < totalPage) {
				context.put("nextPage", "news" + item.getItemId() + "-" + (pageNo + 1) + ".html");
			}

			context.put("totalPage", totalPage);

			StringWriter sw = new StringWriter();
			Template tpl = Velocity.getTemplate(template, Constants.UTF8);
			tpl.merge(context, sw);
			String htmlFile = Global.getConfig("htmlPath") + fileName;
			FileUtils.deleteFile(htmlFile);
			if (FileUtils.createFile(htmlFile)) {
				FileUtils.writeToFile(htmlFile, sw.toString(), true);
			}

			//mobile
			sw = new StringWriter();
			template = Constants.TEMPLATES + "mobile/"+templateFile;
			tpl = Velocity.getTemplate(template, Constants.UTF8);
			tpl.merge(context, sw);
			htmlFile = Global.getConfig("htmlPath") + "mobile/" + fileName;
			FileUtils.deleteFile(htmlFile);
			if (FileUtils.createFile(htmlFile)) {
				FileUtils.writeToFile(htmlFile, sw.toString(), true);
			}
		}

		news = new News();
		news.setItemId(itemId);
		List<News> allList = newsService.findList(news);
		for (News news2 : allList) {
			html(news2, context,item,true,pre);
			html(news2, context,item,false,pre);
		}



		return JsonMapper.getInstance().toJson("生成成功");
	}
	private void html(News news,VelocityContext context,Item item,boolean ismobile,String pre){
		String fileName =pre+"/"+news.getNewsId()+".html";
		String template=Constants.TEMPLATES+"news_detail.html";
		if(ismobile){
			fileName ="mobile/"+pre+"/"+news.getNewsId()+".html";
			template=Constants.TEMPLATES+"mobile/news_detail.html";
		}

		context.put("news", news);
		context.put("ctx", "..");
		context = this.getCrumb(item, context);
		context.put("item", item);
		StringWriter sw = new StringWriter();
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		String htmlFile = Global.getConfig("htmlPath")+fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)){
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}
	}
}