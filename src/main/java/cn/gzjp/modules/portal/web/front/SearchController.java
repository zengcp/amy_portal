package cn.gzjp.modules.portal.web.front;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.constant.Constants;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.portal.entity.Mdict;
import cn.gzjp.modules.portal.entity.News;
import cn.gzjp.modules.portal.entity.Product;
import cn.gzjp.modules.portal.service.NewsService;
import cn.gzjp.modules.portal.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * 搜索专用Controller
 * @author zengcp
 * @version 2017-08-17
 */
@Controller("searchController")
@RequestMapping(value = "${frontPath}/search")
public class SearchController extends BaseController {

	@Autowired
	private ProductService productService;
	@Autowired
	private NewsService newsService;




	@RequestMapping("/product_pc")
	public String product_pc(String name, Model model) {

		Product search = new Product();
		search.setTitle(name);

		List<Product> productList = productService.findList(search);

		model.addAttribute("productList", productList);
		model.addAttribute("types", this.getProductTypeList());
		model.addAttribute("bannerList", getBannerList());
		model.addAttribute("productTypeList", this.getProductTypeList());
		model.addAttribute("htmlUrl", Global.getConfig("htmlUrl"));
		model.addAttribute("ctxStatic", Global.getConfig("ctxStatic"));
		model.addAttribute("frontUrl", Global.getConfig("frontUrl"));

		model.addAttribute("name", name);
		search = new Product();
		search.setTop("1");
		List<Product> topList = productService.findList(search);
		//推荐
		model.addAttribute("topList", topList);

		return forward("search_product_pc");
	}


	@RequestMapping("/product_mobile")
	public String product_mobile(String name, Model model) {

		Product search = new Product();
		search.setTitle(name);

		List<Product> productList = productService.findList(search);

		model.addAttribute("productList", productList);
		model.addAttribute("types", this.getProductTypeList());
		model.addAttribute("bannerList", getBannerList());
		model.addAttribute("productTypeList", this.getProductTypeList());
		model.addAttribute("htmlUrl", Global.getConfig("htmlUrl"));
		model.addAttribute("ctxStatic", Global.getConfig("ctxStatic"));
		model.addAttribute("frontUrl", Global.getConfig("frontUrl"));

		model.addAttribute("name", name);
		search = new Product();
		search.setTop("1");
		List<Product> topList = productService.findList(search);
		//推荐
		model.addAttribute("topList", topList);

		return forward("search_product_mobile");
	}


	@RequestMapping("/service_pc")
	public String service_pc(String serviceName, Model model) {
		News news = new News();
		news.setTitle(serviceName);
		news.setLang(Constants.NewsType.COURSE);
		List<News> newsList = newsService.findList(news);

		model.addAttribute("newsList", newsList);
		model.addAttribute("bannerList", getBannerList());
		model.addAttribute("productTypeList", this.getProductTypeList());
		model.addAttribute("htmlUrl", Global.getConfig("htmlUrl"));
		model.addAttribute("frontUrl", Global.getConfig("frontUrl"));
		model.addAttribute("serviceName", serviceName);
		model.addAttribute("ctxStatic", Global.getConfig("ctxStatic"));
		model.addAttribute("frontUrl", Global.getConfig("frontUrl"));

		return forward("search_service_pc");
	}

	@RequestMapping("/service_mobile")
	public String service_mobile(String serviceName, Model model) {
		News news = new News();
		news.setTitle(serviceName);
		news.setLang(Constants.NewsType.COURSE);
		List<News> newsList = newsService.findList(news);

		model.addAttribute("newsList", newsList);
		model.addAttribute("bannerList", getBannerList());
		model.addAttribute("productTypeList", this.getProductTypeList());
		model.addAttribute("htmlUrl", Global.getConfig("htmlUrl"));
		model.addAttribute("frontUrl", Global.getConfig("frontUrl"));
		model.addAttribute("serviceName", serviceName);
		model.addAttribute("ctxStatic", Global.getConfig("ctxStatic"));
		model.addAttribute("frontUrl", Global.getConfig("frontUrl"));

		return forward("search_service_mobile");
	}
}