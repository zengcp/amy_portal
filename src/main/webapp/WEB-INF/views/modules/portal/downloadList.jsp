<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>下载中心管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<!--
	<div class="ibox-title">
		<h5>轮播图列表 </h5>
	</div>
    -->
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="download" action="${ctx}/portal/download/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">


		</div>

	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
				<table:addRow url="${ctx}/portal/download/form" title="" width="800px" height="580px"></table:addRow><!-- 增加按钮 -->
			<button type="button" onclick="createIndexHtml()"  class="btn btn-primary btn-xs">生成html</button>
		</div>
		
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead style="background:#fff">
			<tr>
				<th>封面图</th>
				<th>名称</th>
				<th>描述内容</th>
				<th>类型</th>
				<th>顺序</th>
				<th width="30%">操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="download">
			<tr>
				<td>
					<img alt="" src="${download.pic}" width="320px" height="90px" >	
				</td>
				<td>
					${download.name}
				</td>
				<td>
					${download.content}
				</td>
				<td>
					${download.type.name}
				</td>
				<td>
					${download.sort}
				</td>
				<td>
					<a href="#" onclick="openDialog('编辑', '${ctx}/portal/download/form?id=${download.id}&lang=zh&type=${download.type}','800px', '580px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 编辑</a>
					<a href="${ctx}/portal/download/delete?id=${download.id}&type=${download.type}" onclick="return confirmx('确认要删除吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
	<script type="text/javascript">

        function createIndexHtml(){
            layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
                $.get('${ctx}/portal/download/createIndexHtml',function(result){layer.msg('操作成功', {time: 2000}); search(); });
            });
        }
	</script>
</body>
</html>