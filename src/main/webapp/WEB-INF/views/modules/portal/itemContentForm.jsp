<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>集团概况管理</title>
	<meta name="decorator" content="default"/>
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/webuploader-0.1.5/webuploader.css">
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/webuploader-0.1.5/demo.css">
	<script type="text/javascript">
    // 添加全局站点信息
    var BASE_URL = '/webuploader';
    </script>
	<script type="text/javascript" charset="utf-8" src="${ctxStatic}/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" charset="utf-8" src="${ctxStatic}/ueditor/ueditor.all.min.js"> </script>
	<script type="text/javascript">
	var toolbars= [[
                    'fullscreen', 'source', '|', 
                    'bold', 'italic', 'underline', 'fontborder', 'strikethrough', 'removeformat', 'formatmatch',  'pasteplain', '|', 'forecolor', 'backcolor', 'insertorderedlist', 'insertunorderedlist', 'selectall', '|',
                    'rowspacingtop', 'rowspacingbottom', 'lineheight', '|',
                    'paragraph', 'fontfamily', 'fontsize', '|',
                    'indent', '|',
                    'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
                    'link',  '|', 'imagenone', 'imageleft', 'imageright', 'imagecenter', '|',
                    'simpleupload', 'insertimage', 'emotion', 'insertvideo',  'attachment',    '|',
                    'horizontal', 'spechars'
                ]];
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}
		$(document).ready(function() {
			var ueditor1 = UE.getEditor('editor1',{
		        scaleEnabled :true,
		        toolbars: toolbars
		    });
			ueditor1.addListener( 'ready', function( editor ) {
				ueditor1.setContent('${item.content}');
    		 } );
		
			
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
		});

	</script>
</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="item" action="${ctx}/portal/item/saveContent" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>

				<tr class="zh">
					<td class="width-10 active"><label class="pull-right">内容：</label></td>
					<td class="width-40" colspan="3">
						<script id="editor1" name="content" type="text/plain" htmlEscape="false"  style="height:450px;width: 100%">
				      	</script>
					</td>
				</tr>

				
		 	</tbody>
		</table>
	</form:form>
</body>
</html>