package cn.gzjp.modules.portal.web;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.gzjp.common.constant.Constants;
import cn.gzjp.common.utils.*;
import cn.gzjp.modules.portal.entity.Item;
import cn.gzjp.modules.portal.entity.News;
import cn.gzjp.modules.portal.entity.Product;
import cn.gzjp.modules.portal.service.NewsService;
import cn.gzjp.modules.portal.service.ProductService;
import cn.gzjp.modules.sys.entity.Dict;
import cn.gzjp.modules.sys.utils.DictUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import cn.gzjp.common.config.Global;
import cn.gzjp.common.mapper.JsonMapper;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.web.AjaxResult;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.portal.entity.Banner;
import cn.gzjp.modules.portal.service.BannerService;

/**
 * 轮播图Controller
 * @author zengcp
 * @version 2017-08-22
 */
@Controller
@RequestMapping(value = "${adminPath}/portal/banner")
public class BannerController extends BaseController {

	@Autowired
	private BannerService bannerService;
	@Autowired
	private NewsService newsService;
	@Autowired
	private ProductService productService;
	
	/**
	 * 轮播图列表页面
	 */
	@RequiresPermissions("portal:banner:list")
	@RequestMapping(value = {"list", ""})
	public String list(Banner banner,String type, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Banner> page = bannerService.findPage(new Page<Banner>(request, response), banner);
		model.addAttribute("page", page);
		return "modules/portal/bannerList";
	}

	/**
	 * 查看，增加，编辑轮播图表单页面
	 */
	@RequiresPermissions(value={"portal:banner:view","portal:banner:add","portal:banner:edit"},logical=Logical.OR)
	@RequestMapping(value = "form")
	public String form(Banner banner,String type, Model model) {
		banner.setType(type);
		model.addAttribute("banner", banner);
		return "modules/portal/bannerForm";
	}

	/**
	 * 保存轮播图
	 */
	@RequiresPermissions(value={"portal:banner:add","portal:banner:edit"},logical=Logical.OR)
	@RequestMapping(value = "save")
	public String save(Banner banner, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, banner)){
			return form(banner, banner.getType(),model);
		}
		if(!banner.getIsNewRecord()){//编辑表单保存
			Banner t = bannerService.get(banner.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(banner, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			bannerService.save(t);//保存
		}else{//新增表单保存
			bannerService.save(banner);//保存
		}
		CacheUtils.remove("bannerList");

		addMessage(redirectAttributes, "保存轮播图成功");
		return redirect("/portal/banner/?type="+banner.getType());
	}
	
	/**
	 * 删除轮播图
	 */
	@RequiresPermissions("portal:banner:del")
	@RequestMapping(value = "delete")
	public String delete(Banner banner, RedirectAttributes redirectAttributes) {
		bannerService.delete(banner);
		addMessage(redirectAttributes, "删除轮播图成功");
		CacheUtils.remove("bannerList");
		return redirect("/portal/banner/?type="+banner.getType());
	}
	
	
	@ModelAttribute
	public Banner get(@RequestParam(required=false) String id) {
		Banner entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = bannerService.get(id);
		}
		if (entity == null){
			entity = new Banner();
		}
		return entity;
	}
	

	/**
	 * 图片上传
	 * @param request
	 * @param file
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	@RequestMapping(value = "imageUpload")
	@ResponseBody
	public String imageUpload( HttpServletRequest request,MultipartFile file) throws IllegalStateException, IOException {
		AjaxResult result = new AjaxResult(false,"");
		// 判断文件是否为空  
        if (!file.isEmpty()) {  
			// 文件保存路径
			String realPath = Global.USERFILES_BASE_URL+"/upload_images/" ;
			// 转存文件
			FileUtils.createDirectory(Global.getUserfilesBaseDir()+realPath);
			String extension = FilenameUtils.getExtension(file
					.getOriginalFilename());
			String mmddss = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
			String fileName = mmddss+"."+extension;

			file.transferTo(new File( Global.getUserfilesBaseDir() +realPath +  fileName));
			String img = request.getContextPath()+realPath + fileName;
			result.setSuccess(true);
			result.setMsg(img);
        }  
    	return JsonMapper.getInstance().toJson(result);
	}
	/**
	 * 文件上传
	 * @param request
	 * @param file
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	@RequestMapping(value = "fileUpload")
	@ResponseBody
	public String fileUpload(HttpServletRequest request,MultipartFile file) throws IllegalStateException, IOException {
		AjaxResult result = new AjaxResult(false,"");
		// 判断文件是否为空  
        if (!file.isEmpty()) {  
			// 文件保存路径
			String realPath = Global.USERFILES_BASE_URL+"/file/" ;
			// 转存文件
			FileUtils.createDirectory(Global.getUserfilesBaseDir()+realPath);
			String extension = FilenameUtils.getExtension(file
					.getOriginalFilename());
			String newFileName = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date())+"."+extension;
			file.transferTo(new File( Global.getUserfilesBaseDir() +realPath +  newFileName));
			String img = request.getContextPath()+realPath + newFileName;
			result.setSuccess(true);
			result.setMsg(img);
        }  
    	return JsonMapper.getInstance().toJson(result);
	}


	@RequestMapping(value = "createIndexHtml")
	@ResponseBody
	public String createIndexHtml(){
		VelocityInitializer.initVelocity();
		VelocityContext context = new VelocityContext();
		context.put("bannerList", this.getBannerList());
		context.put("productTypeList", this.getProductTypeList());
		List<Item> allItemList = this.getItemList();
		context.put("allItemList", allItemList);
		context.put("ctxStatic", Global.getConfig("ctxStatic"));
		context.put("frontUrl", Global.getConfig("frontUrl"));
		context.put("ctx", ".");
		context.put("menu",1);

        //最新资讯
		News news = new News();
		news.setType(Constants.NewsType.NEWS);
		Page<News> apage = new Page<News>(1,3);
		List<News> newsList = newsService.findPage(apage, news).getList();
		context.put("newsList", newsList);

		//明星产品
		Product product = new Product();
		product.setStar("1");
		Page<Product> page1 = new Page<Product>(1,3);
		List<Product> starList = productService.findPage(page1, product).getList();
		if(starList!=null&&starList.size()==3){
			context.put("star1", starList.get(0));
			context.put("star2", starList.get(1));
			context.put("star3", starList.get(2));
		}

		//热销产品
		product.setHot("1");
		page1 = new Page<Product>(1,3);
		List<Product> hotList = productService.findPage(page1, product).getList();
		context.put("hotList", hotList);


		//最新活动
		news = new News();
		news.setType(Constants.NewsType.ACTIVITY);
		apage = new Page<News>(1,5);
		List<News> activityList = newsService.findPage(apage, news).getList();
		context.put("activityList", activityList);


        //pc
		String template=Constants.TEMPLATES+"index.html";
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		StringWriter sw = new StringWriter();
		tpl.merge(context, sw);
		String fileName="index.html";
		String htmlFile = Global.getConfig("htmlPath")+fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)){
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}

		//mobile
		template=Constants.TEMPLATES+"/mobile/index.html";
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		sw = new StringWriter();
		tpl.merge(context, sw);
		fileName="mobile/index.html";
		htmlFile = Global.getConfig("htmlPath")+fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)){
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}
		return JsonMapper.getInstance().toJson("生成成功");
	}

}