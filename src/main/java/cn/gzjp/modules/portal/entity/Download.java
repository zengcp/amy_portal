package cn.gzjp.modules.portal.entity;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.persistence.DataEntity;
import cn.gzjp.common.utils.excel.annotation.ExcelField;
import com.xiaoleilu.hutool.util.StrUtil;
import org.hibernate.validator.constraints.Length;

/**
 * 下载中心
 */
public class Download extends DataEntity<Download> {

	private static final long serialVersionUID = 1L;
	private String pic;		// 封面图
	private int sort;		// 顺序
	private String filePath;    //文件路径
	private String content;

	private Mdict type;		// 业务类型
	private String name;

	private String item;

	public Download() {
		super();
	}

	public Download(String id){
		super(id);
	}

	@Override
	public String getPic() {
		return pic;
	}

	@Override
	public void setPic(String pic) {
		this.pic = pic;
	}

	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public Mdict getType() {
		return type;
	}

	public void setType(Mdict type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getFilePathUrl(){
		if(StrUtil.isNotBlank(this.getFilePath())){
			return Global.getConfig("frontUrl")+this.getPic();
		}
		return "";
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}
}