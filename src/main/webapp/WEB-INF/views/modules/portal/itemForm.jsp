<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>新闻类型管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}
		$(document).ready(function() {
			$("#name").focus();
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox") || element.is(":radio") || element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body class="hideScroll">
	<form:form id="inputForm" modelAttribute="item" action="${ctx}/portal/item/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="parentItemId"/>
		<form:hidden path="parent.id"/>

		<sys:message content="${message}"/>
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
			<tr>
				<td class="width-15 active"><label class="pull-right"><font color="red">*</font>栏目名称：</label></td>
				<td class="width-35 has-warning" >
					<form:input path="name" htmlEscape="false"    class="form-control required"/>
				</td>
			</tr>
           <c:if test="${not empty item.itemId}">
			<tr>
				<td class="width-15 active"><label class="pull-right"><font color="red">*</font>栏目编号(唯一)：</label></td>
				<td class="width-35">
					<form:input path="itemId" htmlEscape="false"  readonly="true"   class="form-control required digits"/>

				</td>
			</tr>
		   </c:if>
			<c:if test="${empty item.itemId}">
				<tr>
					<td class="width-15 active"><label class="pull-right"><font color="red">*</font>栏目编号(唯一)：</label></td>
					<td class="width-35">
						<form:input path="itemId" htmlEscape="false"    class="form-control required digits"/>

					</td>
				</tr>
			</c:if>
			<tr>
				<td class="width-15 active"><label class="pull-right"><font color="red">*</font>排序：</label></td>
				<td class="width-35">
					<form:input path="sort" htmlEscape="false"    class="form-control required digits"/>
				
				</td>
			</tr>

			<tr>
				<td class="width-15 active"><label class="pull-right"><font color="red"></font>页面：</label></td>
				<td class="width-35">
					<form:select path="type" class="form-control" >
						<form:option value="" label=""/>
						<form:options items="${fns:getDictList('item_type')}" itemLabel="label"
									  itemValue="value" htmlEscape="false" />
					</form:select>
				</td>
			</tr>

			<tr>
				<td  class="width-15 active"><label class="pull-right">可见:</label></td>
				<td class="width-35" ><form:radiobuttons path="isShow" items="${fns:getDictList('show_hide')}" itemLabel="label" itemValue="value" htmlEscape="false" class="required i-checks "/>
					<span class="help-inline">是否显示在前端栏目中</span></td>

			</tr>

		</tbody>
	</table>
	</form:form>
</body>
</html>