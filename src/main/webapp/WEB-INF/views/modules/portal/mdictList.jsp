<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>数据类型管理</title>
	<meta name="decorator" content="default"/>
	<%@include file="/WEB-INF/views/include/treetable.jsp" %>
	<script type="text/javascript">
		$(document).ready(function() {
			var tpl = $("#treeTableTpl").html().replace(/(\/\/\<!\-\-)|(\/\/\-\->)/g,"");
			var data = ${fns:toJson(list)}, ids = [], rootIds = [];
			for (var i=0; i<data.length; i++){
				ids.push(data[i].id);
			}
			ids = ',' + ids.join(',') + ',';
			for (var i=0; i<data.length; i++){
				if (ids.indexOf(','+data[i].parentId+',') == -1){
					if ((','+rootIds.join(',')+',').indexOf(','+data[i].parentId+',') == -1){
						rootIds.push(data[i].parentId);
					}
				}
			}
			for (var i=0; i<rootIds.length; i++){
				addRow("#treeTableList", tpl, data, rootIds[i], true);
			}
			$("#treeTable").treeTable({expandLevel : 5});
			$('.zml0').show();
		});
		function addRow(list, tpl, data, pid, root){
			for (var i=0; i<data.length; i++){
				var row = data[i];
				if ((${fns:jsGetVal('row.parentId')}) == pid){
					$(list).append(Mustache.render(tpl, {
						dict: {
							type: getDictLabel(${fns:toJson(fns:getDictList('wx_menu_type'))}, row.type),
						blank123:0}, pid: (root?0:pid), row: row
					}));
					addRow(list, tpl, data, row.id);
				}
			}
		}
		
		
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	
    <div class="ibox-content">
	<sys:message content="${message}"/>


	
	<!-- 工具栏-->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
				<table:addRow url="${ctx}/portal/mdict/form?type=${type}" title="数据类型" width="550px" height="380px" label="新增类型"></table:addRow>

		</div>
	</div>
	</div>

	<table id="treeTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th>名称</th>
				<th>排序</th>
				
				<th width="35%">操作</th>
			</tr>
		</thead>
		<tbody id="treeTableList"></tbody>
	</table>
	<script type="text/template" id="treeTableTpl">
		<tr id="{{row.id}}" pId="{{pid}}">
			<td>
				{{row.name}}
			</td>
			<td>
				{{row.sort}}
			</td>
			<td>
   				<a href="#" onclick="openDialog('修改', '${ctx}/portal/mdict/form?id={{row.id}}&type={{row.type}}','550px', '380px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>
				<a href="${ctx}/portal/mdict/delete?id={{row.id}}" onclick="return confirmx('确认要删除该类型吗？', this.href)" class="btn btn-danger btn-xs" ><i class="fa fa-trash"></i> 删除</a>
			    <span class="zml{{row.parentId}}" style="display:none" ><a href="#" onclick="openDialog('{{row.name}}_新增子类型', '${ctx}/portal/mdict/form?parent.id={{row.id}}&type={{row.type}}','550px', '380px')" class="btn btn-warning btn-xs" ><i class="fa fa-plus"></i> 新增子类型</a>
                </span> 

          </td>
		</tr>
	</script>
</body>
</html>