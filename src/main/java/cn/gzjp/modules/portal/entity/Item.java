package cn.gzjp.modules.portal.entity;

import cn.gzjp.common.persistence.DataEntity;
import cn.gzjp.common.utils.excel.annotation.ExcelField;
import cn.gzjp.modules.portal.entity.vo.DownLoadVo;
import cn.gzjp.modules.sys.utils.DictUtils;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.xiaoleilu.hutool.util.StrUtil;
import org.hibernate.validator.constraints.Length;

import java.util.List;

/**
 * 新闻类型Entity
 * @author zengcp
 * @version 2017-08-17
 */
public class Item extends DataEntity<Item> {

	private static final long serialVersionUID = 1L;
	private String type;

	private String name;		// 名称
	private Item parent;		// 上级id
	private Integer sort;		// 排序
	private String parentIds;		// 所有父级编号
	private List<Item> children;	// 子菜单

	private String content;
	private Integer itemId; //栏目编号

	private Integer parentItemId;

	private String isShow; 	//是否在菜单中显示（1：显示；0：不显示）



	public Item() {
		super();
	}

	public Item(String id){
		super(id);
	}

	@Length(min=0, max=64, message="名称长度必须介于 0 和 64 之间")
	@ExcelField(title="名称", align=2, sort=1)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	
	@JsonBackReference
	@ExcelField(title="上级id", align=2, sort=3)
	public Item getParent() {
		return parent;
	}

	public void setParent(Item parent) {
		this.parent = parent;
	}

	public Integer getSort() {
		return sort;
	}

	public void setSort(Integer sort) {
		this.sort = sort;
	}

	public String getParentIds() {
		return parentIds;
	}

	public void setParentIds(String parentIds) {
		this.parentIds = parentIds;
	}

	public List<Item> getChildren() {
		return children;
	}

	public void setChildren(List<Item> children) {
		this.children = children;
	}
	public String getParentId() {
		return parent != null && parent.getId() != null ? parent.getId() : "0";
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getUrl(){
		if(StrUtil.isNotBlank(this.getType())){
			String url = this.getType().replace("_id",this.getItemId()+"");
			return url;
		}
		return "";

	}


	public String getIsNewsList(){
		if("news_id.html".equals(this.getType())||"service_id.html".equals(this.getType())||"brand_id.html".equals(this.getType())||"activity_id.html".equals(this.getType())){
			return "news";
		}

		return "";
	}




	public String getPageName(){
		return DictUtils.getDictLabel(this.getType(),"item_type","");
	}

	public Integer getParentItemId() {
		return parentItemId;
	}

	public void setParentItemId(Integer parentItemId) {
		this.parentItemId = parentItemId;
	}

	public String getIsShow() {
		return isShow;
	}

	public void setIsShow(String isShow) {
		this.isShow = isShow;
	}
}