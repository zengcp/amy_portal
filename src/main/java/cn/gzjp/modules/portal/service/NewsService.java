package cn.gzjp.modules.portal.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.portal.entity.News;
import cn.gzjp.modules.portal.dao.NewsDao;

/**
 * 新闻资讯Service
 * @author zengcp
 * @version 2017-08-17
 */
@Service
@Transactional(readOnly = true)
public class NewsService extends CrudService<NewsDao, News> {


	
	public List<News> findList(News news) {
		return super.findList(news);
	}
	

	
	
	public Page<News> findPage(Page<News> page, News news) {
		return super.findPage(page, news);
	}
	
	@Transactional(readOnly = false)
	public void save(News news) {
		if (news.getNewsId()==0){
			dao.insert(news);
		}else{
			dao.update(news);
		}
	}

	public News get(long id) {
		return dao.get(id);
	}
	
	@Transactional(readOnly = false)
	public void delete(News news) {
		super.deleteByLogic(news);
	}

}