/**
 * Copyright &copy; 2015-2020 <a href="http://www..org/"></a> All rights reserved.
 */
package cn.gzjp.common.web;

import java.awt.Image;
import java.awt.Rectangle;
import java.beans.PropertyEditorSupport;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Date;
import java.util.List;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;
import javax.validation.Validator;

import cn.gzjp.common.utils.VelocityInitializer;
import cn.gzjp.modules.portal.entity.Item;
import cn.gzjp.modules.portal.entity.Mdict;
import cn.gzjp.modules.portal.service.ItemService;
import com.xiaoleilu.hutool.util.ObjectUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.velocity.VelocityContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.xiaoleilu.hutool.util.ImageUtil;

import cn.gzjp.common.beanvalidator.BeanValidators;
import cn.gzjp.common.config.Global;
import cn.gzjp.common.mapper.JsonMapper;
import cn.gzjp.common.utils.CacheUtils;
import cn.gzjp.common.utils.DateUtils;
import cn.gzjp.modules.portal.entity.Banner;
import cn.gzjp.modules.portal.service.BannerService;
import cn.gzjp.modules.portal.service.MdictService;

/**
 * 控制器支持类
 * @author 
 * @version 2013-3-23
 */
public abstract class BaseController {


	@Autowired
	private BannerService bannerService;

	@Autowired
	protected MdictService mdictService;


	@Autowired
	protected ItemService itemService;
	/**
	 * 日志对象
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());

	/**
	 * 管理基础路径
	 */
	@Value("${adminPath}")
	protected String adminPath;
	
	/**
	 * 前端基础路径
	 */
	@Value("${frontPath}")
	protected String frontPath;
	
	/**
	 * 前端URL后缀
	 */
	@Value("${urlSuffix}")
	protected String urlSuffix;
	
	/**
	 * 验证Bean实例对象
	 */
	@Autowired
	protected Validator validator;

	/**
	 * 服务端参数有效性验证
	 * @param object 验证的实体对象
	 * @param groups 验证组
	 * @return 验证成功：返回true；严重失败：将错误信息添加到 message 中
	 */
	protected boolean beanValidator(Model model, Object object, Class<?>... groups) {
		try{
			BeanValidators.validateWithException(validator, object, groups);
		}catch(ConstraintViolationException ex){
			List<String> list = BeanValidators.extractPropertyAndMessageAsList(ex, ": ");
			list.add(0, "数据验证失败：");
			addMessage(model, list.toArray(new String[]{}));
			return false;
		}
		return true;
	}
	
	/**
	 * 服务端参数有效性验证
	 * @param object 验证的实体对象
	 * @param groups 验证组
	 * @return 验证成功：返回true；严重失败：将错误信息添加到 flash message 中
	 */
	protected boolean beanValidator(RedirectAttributes redirectAttributes, Object object, Class<?>... groups) {
		try{
			BeanValidators.validateWithException(validator, object, groups);
		}catch(ConstraintViolationException ex){
			List<String> list = BeanValidators.extractPropertyAndMessageAsList(ex, ": ");
			list.add(0, "数据验证失败：");
			addMessage(redirectAttributes, list.toArray(new String[]{}));
			return false;
		}
		return true;
	}
	
	/**
	 * 服务端参数有效性验证
	 * @param object 验证的实体对象
	 * @param groups 验证组，不传入此参数时，同@Valid注解验证
	 * @return 验证成功：继续执行；验证失败：抛出异常跳转400页面。
	 */
	protected void beanValidator(Object object, Class<?>... groups) {
		BeanValidators.validateWithException(validator, object, groups);
	}
	
	/**
	 * 添加Model消息
	 */
	protected void addMessage(Model model, String... messages) {
		StringBuilder sb = new StringBuilder();
		for (String message : messages){
			sb.append(message).append(messages.length>1?"<br/>":"");
		}
		model.addAttribute("message", sb.toString());
	}
	
	/**
	 * 添加Flash消息
	 */
	protected void addMessage(RedirectAttributes redirectAttributes, String... messages) {
		StringBuilder sb = new StringBuilder();
		for (String message : messages){
			sb.append(message).append(messages.length>1?"<br/>":"");
		}
		redirectAttributes.addFlashAttribute("message", sb.toString());
	}
	
	/**
	 * 客户端返回JSON字符串
	 * @param response
	 * @param object
	 * @return
	 */
	protected String renderString(HttpServletResponse response, Object object) {
		return renderString(response, JsonMapper.toJsonString(object));
	}
	
	/**
	 * 客户端返回字符串
	 * @param response
	 * @param string
	 * @return
	 */
	protected String renderString(HttpServletResponse response, String string) {
		try {
			response.reset();
	        response.setContentType("application/json");
	        response.setCharacterEncoding("utf-8");
			response.getWriter().print(string);
			return null;
		} catch (IOException e) {
			return null;
		}
	}

	/**
	 * 参数绑定异常
	 */
	@ExceptionHandler({BindException.class, ConstraintViolationException.class, ValidationException.class})
    public String bindException() {  
        return "error/400";
    }
	
	/**
	 * 授权登录异常
	 */
	@ExceptionHandler({AuthenticationException.class})
    public String authenticationException() {  
        return "error/403";
    }
	
	/**
	 * 初始化数据绑定
	 * 1. 将所有传递进来的String进行HTML编码，防止XSS攻击
	 * 2. 将字段中Date类型转换为String类型
	 */
	@InitBinder
	protected void initBinder(WebDataBinder binder) {
		// String类型转换，将所有传递进来的String进行HTML编码，防止XSS攻击
		binder.registerCustomEditor(String.class, new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) {
				setValue(text == null ? null : StringEscapeUtils.escapeHtml4(text.trim()));
			}
			@Override
			public String getAsText() {
				Object value = getValue();
				return value != null ? value.toString() : "";
			}
		});
		// Date 类型转换
		binder.registerCustomEditor(Date.class, new PropertyEditorSupport() {
			@Override
			public void setAsText(String text) {
				setValue(DateUtils.parseDate(text));
			}
//			@Overrid
//			public String getAsText() {
//				Object value = getValue();
//				return value != null ? DateUtils.formatDateTime((Date)value) : "";
//			}
		});
	}
	/**
	 * SpingMVC重定向
	 * @param format
	 * @param arguments
	 * @return
	 */
	public String redirect(String format, Object... arguments) {
		return new StringBuffer("redirect:").append(adminPath).append(MessageFormat.format(format, arguments)).toString();
	}
	
	public String redirectFront(String format, Object... arguments) {
		return new StringBuffer("redirect:").append(frontPath).append(MessageFormat.format(format, arguments)).toString();
	}
	
	
	
	public String forward(String lang,String page) {
		return new StringBuffer("modules/portal/front/").append(!"".equals(lang)?(lang+"/"):"").append(page).toString();
	}

	public String forward(String page) {
		return new StringBuffer("modules/portal/front/").append(page).toString();
	}
	

	public List<Banner> getBannerList(){
		List<Banner> bannerList = (List<Banner>) CacheUtils.get("bannerList");
		if(bannerList ==null){
			Banner banner = new Banner();
			bannerList = bannerService.findList(banner);
			CacheUtils.put("bannerList", bannerList);
		}
		return bannerList;
	}

	public List<Mdict> getProductTypeList(){
		List<Mdict> productTypeList = (List<Mdict>) CacheUtils.get("productType");
		if(productTypeList ==null){
			productTypeList = mdictService.findTreeList("product_type");
			CacheUtils.put("productTypeList", productTypeList);
		}
		return productTypeList;
	}

	public List<Item> getItemList(){
		List<Item> itemList = (List<Item>) CacheUtils.get("itemList");
		if(itemList ==null){
			itemList = itemService.findTreeList();
			CacheUtils.put("itemList", itemList);
		}
		return itemList;
	}


	public List<Item> getItemList(Integer itemId){
		List<Item> itemList = (List<Item>) CacheUtils.get("itemList_"+itemId);
		if(itemList ==null){
			itemList = itemService.findTreeList(itemId);
			CacheUtils.put("itemList_"+itemId, itemList);
		}
		return itemList;
	}





	public String resizePic(int x,int y,int w,int h,int width,int height,HttpServletRequest request,String oldPic){
		Rectangle rectangle = new Rectangle();
		rectangle.x = x;
		rectangle.y = y;
		rectangle.width=w;
		rectangle.height=h;
		//String realPath = Global.USERFILES_BASE_URL+"/upload_images/" ;
		String fileName = oldPic.replaceAll(request.getContextPath(), "");
		String srcImgFile = Global.getUserfilesBaseDir()+fileName;
		String fileNameNew = fileName.replace(".", "_new.");
		String destImgFile = Global.getUserfilesBaseDir()+fileNameNew;
		ImageUtil.cut(new File(srcImgFile), new File(destImgFile), rectangle);//裁剪
		if(width!=0&& height!=0&&width<(w-x)&&height<(h-y)){//缩小
			String srcImageFile =destImgFile;
			fileNameNew = fileNameNew.replace("_new.", "_new_s.");
			String  destImageFile = Global.getUserfilesBaseDir()+fileNameNew;
			ImageUtil.scale(new File(srcImageFile), new File(destImageFile), width, height, null);
		}
		
	    String newPic = request.getContextPath() + fileNameNew;
	    return newPic;
	}
	
	public String resizePic(int width,int height,HttpServletRequest request,String oldPic){
		//String realPath = Global.USERFILES_BASE_URL+"/upload_images/" ;
		String fileName = oldPic.replaceAll(request.getContextPath(), "");
		String fileNameNew = fileName;
		String srcImgFile = Global.getUserfilesBaseDir()+fileName;
		File srcFile = new File(srcImgFile);
		Image image;
		try {
			image = ImageIO.read(srcFile);
			// 构造Image对象
			int imageWidth = image.getWidth(null);// 得到源图宽
			int imageHeight = image.getHeight(null);// 得到源图长
			if(width!=0&& height!=0&&width<imageWidth&&height<imageHeight){//缩小
				fileNameNew = fileName.replace(".", "_new.");
				String  destImageFile = Global.getUserfilesBaseDir()+fileNameNew;
				ImageUtil.scale(new File(srcImgFile), new File(destImageFile), width, height, null);
			}
			if(width!=0&& height==0&&width<imageWidth&&height<imageHeight){//宽度固定，高度按比例
				height = (width*100/imageWidth)*imageHeight/100;
				fileNameNew = fileName.replace(".", "_new.");
				String  destImageFile = Global.getUserfilesBaseDir()+fileNameNew;
				ImageUtil.scale(new File(srcImgFile), new File(destImageFile), width, height, null);
			}
			if(width==0&& height!=0&&width<imageWidth&&height<imageHeight){//高度固定，宽度按比例
				width = (height*100/imageHeight)*imageWidth/100;
				fileNameNew = fileName.replace(".", "_new.");
				String  destImageFile = Global.getUserfilesBaseDir()+fileNameNew;
				ImageUtil.scale(new File(srcImgFile), new File(destImageFile), width, height, null);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	    String newPic = request.getContextPath() + fileNameNew;
	    return newPic;
	}


	 protected  VelocityContext getContext(Integer itemId) {
		VelocityInitializer.initVelocity();
		VelocityContext context = new VelocityContext();
		context.put("bannerList", this.getBannerList());
		List<Item> allItemList = this.getItemList();
		context.put("allItemList", allItemList);
		context.put("ctxStatic", Global.getConfig("ctxStatic"));
		context.put("frontUrl", Global.getConfig("frontUrl"));
		context.put("ctx", ".");
		context.put("menu", itemId);
		return context;
	}

	protected VelocityContext getCrumb(Item item,VelocityContext context){
		String ctx = (String)context.get("ctx");
		String crumbPc="<a href=\""+ctx+"/index.html\">首页&gt;</a><a>"+item.getName()+"</a>";
		String crumbMobile="<a href=\""+ctx+"/index.html\"> 首页 &gt;</a><a  class=\"navlast\">"+item.getName()+"</a>";
		if(ObjectUtil.isNotNull(item.getParentItemId())){
			Item parent = itemService.getByItemId(item.getParentItemId());
			if(ObjectUtil.isNotNull(parent)) {
				if(StrUtil.isBlank(parent.getUrl())){
					crumbPc = "<a href=\""+ctx+"/index.html\">首页&gt;</a><a >"+parent.getName()+"&gt;</a><a >"+item.getName()+"</a>";
					crumbMobile="<a href=\""+ctx+"/index.html\"> 首页 &gt;</a><a >"+parent.getName()+"&gt;</a><a  class=\"navlast\">"+item.getName()+"</a>";

				}else{
					crumbPc = "<a href=\""+ctx+"/index.html\">首页&gt;</a><a ><a href=\""+ctx+"/"+
							parent.getUrl()+"\">"+parent.getName()+"&gt;</a><a >"+item.getName()+"</a>";
					crumbMobile="<a href=\""+ctx+"/index.html\"> 首页 &gt;</a><a href=\""+ctx+"/" +
							parent.getUrl()+"\">"+parent.getName()+"&gt;</a><a  class=\"navlast\">"+item.getName()+"</a>";
				}
			}
		}
		context.put("crumbPc", crumbPc);
		context.put("crumbMobile", crumbMobile);
		return context;
	}
}
