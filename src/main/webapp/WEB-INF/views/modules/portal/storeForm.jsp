<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>全系产品管理</title>
	<meta name="decorator" content="default"/>
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/webuploader-0.1.5/webuploader.css">
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/webuploader-0.1.5/demo.css">
	  <script type="text/javascript">
    // 添加全局站点信息
    var BASE_URL = '/webuploader';
    </script>
	<script type="text/javascript" src="${ctxStatic}/webuploader-0.1.5/webuploader.js"></script>
	<script src="${ctxStatic}/common/PCASClass.js" type="text/javascript"></script>
	<script type="text/javascript" src="https://api.map.baidu.com/api?v=2.0&ak=NslEPQL2D68j5LPqjlngIpCChU0yIdgM"></script>

	<script type="text/javascript">

		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}

		$(document).ready(function() {

			<c:if test="${empty store.pic}">
				$("#prePic").hide();
			</c:if>

            <c:if test="${not empty store.province}">
            new PCAS("province","city","${store.province}","${store.city}") ;

            </c:if>
            <c:if test="${empty store.province}">
            new PCAS("province","city");
            </c:if>

			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
		});
	</script>

	<style>
		#allmap {
			/*百度地图*/
			width:99%;
			height:400px;
			text-align:center;
		}
	</style>
</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="store" action="${ctx}/portal/store/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
			<form:hidden path="pointx"/>
			<form:hidden path="pointy"/>
		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
		   <tr>
			   <td class="width-15 active"><label class="pull-right">门店名称：</label></td>
			   <td class="width-35">
				   <form:input path="name" htmlEscape="false"  maxlength="128"    class="form-control required"/>
			   </td>
			   <td class="width-15 active"><label class="pull-right">电话：</label></td>
			   <td class="width-35" >
				   <form:input path="tel" htmlEscape="false"  maxlength="32"    class="form-control required"/>
			   </td>
		   </tr>
		   <tr>
			   <td class="width-15 active"><label class="pull-right">省：</label></td>
			   <td class="width-35">
				   <form:select path="province" class="form-control required" >

				   </form:select>
			   </td>


			   <td class="width-15 active"><label class="pull-right">市：</label></td>
			   <td class="width-35">
				   <form:select path="city" class="form-control required" >

				   </form:select>
			   </td>

		   </tr>

		   <tr>
			   <td class="width-15 active"><label class="pull-right">详细地址：</label></td>
			   <td class="width-35" colspan="3">
				   <form:input path="address" htmlEscape="false"  maxlength="256"    class="form-control required"/>
			   </td>
		   </tr>
		   <tr>
			   <td class="width-15 active"><label class="pull-right">地图：</label></td>
			   <td class="width-35" colspan="3">
				   <div id="allmap"></div>
			   </td>
		   </tr>
		   <tr>
			   <td class="width-15 active"><label class="pull-right">展示图片：</label></td>
			   <td class="width-35" colspan="3">
				   <form:hidden id="pics" path="pics" htmlEscape="false" maxlength="1024" class="form-control required"/>
				   <sys:ckfinder input="pics" type="images" uploadPath="/store/pics" selectMultiple="true"/>
			   </td>
		   </tr>


		   </tbody>
		</table>
	</form:form>


<script type="text/javascript">
	var pointx='${store.pointx}';
    var pointy='${store.pointy}';
	var point;
	if(pointx&&pointy){
		point = new BMap.Point(pointx,pointy);
	}else{
		point = new BMap.Point(113.438197,23.17543509);//默认为广州的坐标
		$('#pointx').val(113.438197);//经度
		$('#pointy').val(23.17543509);//纬度
	}
	//百度地图API功能
	var map = new BMap.Map("allmap");
	var stCtrl = new BMap.PanoramaControl();
	stCtrl.setOffset(new BMap.Size(20, 20));
	map.addControl(stCtrl);
	//右上角，仅包含平移和缩放按钮
	var top_right_navigation = new BMap.NavigationControl({anchor: BMAP_ANCHOR_TOP_RIGHT, type: BMAP_NAVIGATION_CONTROL_SMALL});
	map.addControl(top_right_navigation);
	//不可滚动缩放地图
	//map.disableScrollWheelZoom();
	var new_marker = new BMap.Marker(point);// 创建标注
	map.addOverlay(new_marker);         // 将标注添加到地图中
	map.centerAndZoom(point,15);      // 默认用广州作为起始点




	/*根据名字查找地方*/
	function searchPlaceByName(placeName){
		map.centerAndZoom(placeName,15);
		var url="${ctx}/portal/store/findLngAndLatByAddress";
		var	param={address:placeName};
		$.post(url,param,function(data){
			var lng=data.lng;
			var lat=data.lat;
			point=new BMap.Point(lng,lat);
			$('#pointx').val(lng);//经度
			$('#pointy').val(lat);//纬度
			new_marker.setPosition(point)

		},'json');

	};


	$('#address').on('blur',function(){
		var placeName=$('#province').find("option:selected").text()+$('#city').find("option:selected").text()+$('#address').val();
		searchPlaceByName(placeName);
	});


	$('#city').on('change',function(){
		var placeName=$('#province').find("option:selected").text()+$('#city').find("option:selected").text();
		searchPlaceByName(placeName);
	});
	$('#province').on('change',function(){
		var placeName=$('#province').find("option:selected").text();
		searchPlaceByName(placeName);
	});

</script>
</body>
</html>