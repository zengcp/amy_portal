package cn.gzjp.modules.portal.service;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.portal.dao.StoreDao;
import cn.gzjp.modules.portal.entity.Store;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 集团概况Service
 * @author zengcp
 * @version 2017-08-14
 */
@Service
@Transactional(readOnly = true)
public class StoreService extends CrudService<StoreDao, Store> {

	public Store get(String id) {
		return super.get(id);
	}
	
	public List<Store> findList(Store profile) {
		return super.findList(profile);
	}
	
	public Page<Store> findPage(Page<Store> page, Store profile) {
		return super.findPage(page, profile);
	}
	

	@Transactional(readOnly = false)
	public void save(Store profile) {
		super.save(profile);
	}
	
	@Transactional(readOnly = false)
	public void delete(Store profile) {
		super.deleteByLogic(profile);
	}


	public List<String> selectDistinctProvince(){
		return dao.selectDistinctProvince();
	}

	public  List<String>  selectDistinctCity(String province){
		return dao.selectDistinctCity(province);
	}
	
}