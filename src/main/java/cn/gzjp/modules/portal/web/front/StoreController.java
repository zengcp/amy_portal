package cn.gzjp.modules.portal.web.front;

import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.portal.entity.Store;
import cn.gzjp.modules.portal.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 实体店Controller
 * @author zengcp
 * @version 2017-08-17
 */
@Controller("frontStore")
@RequestMapping(value = "${frontPath}/store")
public class StoreController extends BaseController {

	@Autowired
	private StoreService storeService;


	@RequestMapping(value = "/getCitys",method = RequestMethod.POST)
	@ResponseBody
	public List<String> getCitys(String province) {
		 List<String> citys = storeService.selectDistinctCity(province);
		return citys;
	}

	@RequestMapping(value = "/getStoreInfo",method = RequestMethod.POST)
	@ResponseBody
	public Store getStoreInfo(String id) {
		Store store = storeService.get(id);
		return store;
	}

	@RequestMapping(value = "/getStoreList",method = RequestMethod.POST)
	@ResponseBody
	public List<Store> getStoreList(String province,String city,String name) {
		Store search = new Store();
		search.setProvince(province);
		search.setCity(city);
		search.setName(name);
		List<Store> storeList = storeService.findList(search);
		return  storeList;
	}

}