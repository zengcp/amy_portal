<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>下载中心管理</title>
	<meta name="decorator" content="default"/>
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/webuploader-0.1.5/webuploader.css">
	<link rel="stylesheet" type="text/css" href="${ctxStatic}/webuploader-0.1.5/demo.css">
	  <script type="text/javascript">
    // 添加全局站点信息
    var BASE_URL = '/webuploader';
    </script>
	<script type="text/javascript" src="${ctxStatic}/webuploader-0.1.5/webuploader.js"></script>
	<script type="text/javascript" src="${ctxStatic}/jquery-jcrop/jquery.Jcrop.min.js"></script>
    <link rel="stylesheet" href="${ctxStatic}/jquery-jcrop/jquery.Jcrop.min.css" type="text/css" />
	<script type="text/javascript">
		var validateForm;
		function doSubmit(){//回调函数，在编辑和保存动作时，供openDialog调用提交表单。
		  if(validateForm.form()){
			  $("#inputForm").submit();
			  return true;
		  }
	
		  return false;
		}

		$(document).ready(function() {

			<c:if test="${empty download.pic}">
				$("#prePic").hide();
			</c:if>
			validateForm = $("#inputForm").validate({
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
			
		});
	</script>


</head>
<body class="hideScroll">
		<form:form id="inputForm" modelAttribute="download" action="${ctx}/portal/download/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<form:hidden path="pic"/>
			<form:hidden path="filePath"/>

		<sys:message content="${message}"/>	
		<table class="table table-bordered  table-condensed dataTables-example dataTable no-footer">
		   <tbody>
				<tr>
					<td class="width-15 active"><label class="pull-right">封面图：</label></td>
					<td class="width-35" >
						<img id="prePic" src="${download.pic}" width="220px"/>
						<div id="uploader-demo">
						    <div class="progress-small" id="downloadPicDiv" style="display: none;">
								<div id="downloadPic" class="progress-bar" role="progressbar" style="<th>描述内容</th>width: 0%;">
								</div>
							</div>
						    <br/>
						    <div id="picPicker">选择图片</div>
						</div>
					</td>
				</tr>


				<tr>
					<td class="width-15 active"><label class="pull-right">文件：</label></td>
					<td class="width-35" >
						<div id="uploader-demo2">
							<div class="progress-small" id="filePicDiv" style="display: none;">
								<div id="filePic" class="progress-bar" role="progressbar" style="width: 0%;">
								</div>
							</div>
							<br/>
							<div id="filePicker">选择文件</div>
						</div>
					</td>
				</tr>
				

				<tr>
					<td class="width-15 active"><label class="pull-right">类型：</label></td>
					<%--<td class="width-35">--%>
						<%--<form:select path="type" class="form-control  required" >--%>
							<%--<form:option value="" label=""/>--%>
							<%--<form:options items="${fns:getDictList('down_type')}" itemLabel="label" itemValue="value" htmlEscape="false"/>--%>
						<%--</form:select>--%>
					<%--</td>--%>
					<td class="width-35">
						<sys:treeselect id="type" name="type.id" value="${download.type.id}" labelName="type.name" labelValue="${download.type.name}"
										title="分类" url="/portal/mdict/treeData?type=download_type" extId="${type.id}" cssClass="form-control required" allowClear="true" notAllowSelectParent="true"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">名称：</label></td>
					<td class="width-35">
						<form:input path="name" htmlEscape="false"  maxlength="32"   class="form-control required"/>
					</td>
				</tr>
				<tr>
					<td class="width-15 active"><label class="pull-right">描述：</label></td>
					<td class="width-35">
						<form:textarea path="content" htmlEscape="false" rows="3"    class="form-control "/>
					</td>
				</tr>

				<tr>
					<td class="width-15 active"><label class="pull-right">顺序：</label></td>
					<td class="width-35">
						<form:input path="sort" htmlEscape="false"   maxlength="11"  class="form-control digits required"/>
					</td>
				</tr>
				
		 	</tbody>
		</table>
	</form:form>

<script>
var uploader = WebUploader.create({

    // 选完文件后，是否自动上传。
    auto: true,

    // swf文件路径
    swf: BASE_URL + '/js/Uploader.swf',

    // 文件接收服务端。1940*540
    server: '${ctx}/portal/download/imageUpload',

    // 选择文件的按钮。可选。
    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
    pick: '#picPicker',
    compress: false,//不启用压缩
    resize: false,//尺寸不改变
    fileSizeLimit: 5 * 1024 * 1024,    // 10 M
    fileSingleSizeLimit: 5 * 1024 * 1024 ,   // 10 M
    // 只允许选择图片文件。
    accept: {
        title: 'Images',
        extensions: 'gif,jpg,jpeg,png',
        mimeTypes: 'image/jpg,image/jpeg,image/png'
    }
});


//文件上传过程中创建进度条实时显示。
uploader.on( 'uploadStart', function( file) {
	$('#downloadPicDiv').show();
    $('#downloadPic').css( 'width', '0%' );
});

//文件上传过程中创建进度条实时显示。
uploader.on( 'uploadProgress', function( file, percentage ) {
	$('#downloadPicDiv').show();
    $('#downloadPic').css( 'width', percentage * 100 + '%' );
});

// 文件上传成功，给item添加成功class, 用样式标记上传成功。
uploader.on( 'uploadSuccess', function( file,response ) {
    if(response.success){
    	$("#pic").val(response.msg);
    	$("#prePic").show();
    	$("#prePic").attr('src',response.msg);

    	layer.msg("上传完成");
    }
});

//文件上传失败，显示上传出错。
uploader.on( 'uploadError', function( file ) {
    top.layer.msg("图片上传失败,请重新上传", {icon: 2});
});

uploader.on("error", function (type) {
    if (type == "Q_TYPE_DENIED") {
        layer.msg("请上传JPG、PNG、GIF、jpeg格式文件");
    } else if (type == "Q_EXCEED_SIZE_LIMIT") {
        layer.msg("文件大小不能超过5M");
    }else {
        layer.msg("上传出错！请检查后重新上传！错误代码"+type);
    }
});


var fileUploader = WebUploader.create({
    // 选完文件后，是否自动上传。
    auto: true,
    // swf文件路径
    swf: BASE_URL + '/js/Uploader.swf',
    // 文件接收服务端。
    server: '${ctx}/portal/download/fileUpload',

    // 选择文件的按钮。可选。
    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
    pick: '#filePicker',
    fileSizeLimit: 5 * 1024 * 1024,    // 5 M
    fileSingleSizeLimit: 5 * 1024 * 1024    // 1 M


});



//文件上传过程中创建进度条实时显示。
fileUploader.on( 'uploadProgress', function( file, percentage ) {
    $('#filePicDiv').show();
    $('#filePic').css( 'width', percentage * 100 + '%' );
});

// 文件上传成功，给item添加成功class, 用样式标记上传成功。
fileUploader.on( 'uploadSuccess', function( file,response ) {
    if(response.success){
        $("#filePath").val(response.msg);
        layer.msg("上传成功");
    }
});

//文件上传失败，显示上传出错。
fileUploader.on( 'uploadError', function( file ) {
    top.layer.msg("上传失败,请重新上传", {icon: 2});
});

fileUploader.on("error", function (type) {
    if (type == "Q_EXCEED_SIZE_LIMIT") {
        layer.msg("文件大小不能超过5M");
    }else {
        layer.msg("上传出错！请检查后重新上传！错误代码"+type);
    }
});
</script>
</body>
</html>