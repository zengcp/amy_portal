package cn.gzjp.modules.portal.web;

import java.util.List;
import java.util.Map;

import cn.gzjp.common.utils.CacheUtils;
import com.xiaoleilu.hutool.util.RandomUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import cn.gzjp.common.utils.MyBeanUtils;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.common.utils.StringUtils;
import cn.gzjp.modules.portal.entity.Mdict;
import cn.gzjp.modules.portal.service.MdictService;

/**
 * 数据类型Controller
 * @author zengcp
 * @version 2017-08-17
 */
@Controller
@RequestMapping(value = "${adminPath}/portal/mdict")
public class MdictController extends BaseController {

	@Autowired
	private MdictService mdictService;
	
	
	/**
	 * 数据类型列表页面
	 */
	@RequestMapping(value = {"list", ""})
	public String list(Mdict mdict, String type, Model model) {
		mdict.setType(type);
		List<Mdict> list = mdictService.findList(mdict);
		model.addAttribute("type", type);
		model.addAttribute("list", list);
		return "modules/portal/mdictList";
	}

	/**
	 * 查看，增加，编辑数据类型表单页面
	 */
	@RequestMapping(value = "form")
	public String form(Mdict mdict,String type, Model model) {
		if (mdict.getParent()!=null && StringUtils.isNotBlank(mdict.getParent().getId())){
			mdict.setParent(mdictService.get(mdict.getParent().getId()));
			// 获取排序号，最末节点排序号+30
			if (StringUtils.isBlank(mdict.getId())){
				Mdict search = new Mdict();
				search.setParent(new Mdict(mdict.getParent().getId()));
				List<Mdict> list = mdictService.findList(search);
				if (list.size() > 0){
					mdict.setSort(list.get(list.size()-1).getSort());
					if (mdict.getSort() != null){
						mdict.setSort(mdict.getSort() + 30);
					}
				}
			}
		}
		if (mdict.getSort() == null){
			mdict.setSort(30);
		}
		model.addAttribute("mdict", mdict);
		model.addAttribute("type", type);
		return "modules/portal/mdictForm";
	}

	/**
	 * 保存数据类型
	 */
	@RequestMapping(value = "save")
	public String save(Mdict mdict, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if(!mdict.getIsNewRecord()){
			Mdict t = mdictService.get(mdict.getId());
			MyBeanUtils.copyBeanNotNull2Bean(mdict, t);
			mdictService.save(t);
		}else{//新增表单保存
			if(mdict.getParent()==null|| StrUtil.isBlank(mdict.getParent().getId())){
				mdict.setParent(new Mdict("0"));
			}
			//String id = RandomUtil.randomNumbers(4);
			mdictService.save(mdict);
		}
		addMessage(redirectAttributes, "保存数据类型成功");
		CacheUtils.remove("productTypeList");
		return redirect("/portal/mdict?type="+mdict.getType());
	}
	
	/**
	 * 删除数据类型
	 */
	@RequestMapping(value = "delete")
	public String delete(Mdict mdict, RedirectAttributes redirectAttributes) {
		mdictService.delete(mdict);
		addMessage(redirectAttributes, "删除数据类型成功");
		CacheUtils.remove("productTypeList");
		return redirect("/portal/mdict?type="+mdict.getType());
	}
	

	
	@ResponseBody
	@RequestMapping(value = "treeData")
	public List<Map<String, Object>> treeData(String type) {
		List<Map<String, Object>> mapList = Lists.newArrayList();
		Mdict search = new Mdict();
		search.setType(type);
		List<Mdict> list = mdictService.findList(search);
		for (int i=0; i<list.size(); i++){
			Mdict e = list.get(i);
				Map<String, Object> map = Maps.newHashMap();
				map.put("id", e.getId());
				map.put("pId", e.getParentId());
				map.put("name", e.getName());
				mapList.add(map);
		}
		return mapList;
	}

	@ModelAttribute
	public Mdict get(@RequestParam(required=false) String id) {
		Mdict entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = mdictService.get(id);
		}
		if (entity == null){
			entity = new Mdict();
		}
		return entity;
	}
	

}