package cn.gzjp.modules.portal.entity;

import cn.gzjp.common.persistence.DataEntity;

/**
 * @Author zengcp
 * @Date 2018/10/13
 */
public class ProductStyle extends DataEntity<ProductStyle> {

    private long productId;

    private String name;

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
