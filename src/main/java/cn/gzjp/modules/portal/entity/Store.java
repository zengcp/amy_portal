package cn.gzjp.modules.portal.entity;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.persistence.DataEntity;
import com.google.common.collect.Lists;
import com.xiaoleilu.hutool.util.StrUtil;

import java.util.Arrays;
import java.util.List;

/**
 * 实体店
 * @author zengcp
 * @version 2017-08-14
 */
public class Store extends DataEntity<Store> {

	private static final long serialVersionUID = 1L;
	private String name;
	private String address;

	private String  tel;

	private String pics;

	private String province;

	private String city;
	private String pointx;		// x坐标
	private String pointy;		// y坐标


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getPics() {
		return pics;
	}

	public void setPics(String pics) {
		this.pics = pics;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public List<String> getPicsList() {
		if(StrUtil.isNotBlank(this.getPics())){
			String pics[] = this.getPics().split("\\|");
			List<String> picsList = Arrays.asList(pics);
			List<String> result = Lists.newArrayList();
			for(String pic:picsList){
				if(StrUtil.isNotBlank(pic)){
					result.add(Global.getConfig("frontUrl")+pic);
				}
			}
			return result;
		}
		return null;
	}

	public String getPointx() {
		return pointx;
	}

	public void setPointx(String pointx) {
		this.pointx = pointx;
	}

	public String getPointy() {
		return pointy;
	}

	public void setPointy(String pointy) {
		this.pointy = pointy;
	}
}