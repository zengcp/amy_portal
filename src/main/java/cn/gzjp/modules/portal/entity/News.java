package cn.gzjp.modules.portal.entity;

import java.util.Date;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.constant.Constants;
import cn.gzjp.modules.sys.utils.DictUtils;
import com.xiaoleilu.hutool.date.DateUtil;
import com.xiaoleilu.hutool.util.ObjectUtil;
import com.xiaoleilu.hutool.util.StrUtil;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.validator.constraints.Length;

import com.xiaoleilu.hutool.http.HtmlUtil;

import cn.gzjp.common.persistence.DataEntity;
import cn.gzjp.common.utils.excel.annotation.ExcelField;

/**
 * 新闻资讯Entity
 * @author zengcp
 * @version 2017-08-17
 */
public class News extends DataEntity<News> {
	
	private static final long serialVersionUID = 1L;
	
	private long newsId;
	private String type;		// 业务类型 1媒体中心 2最新活动 3教程 4品牌相关
	private String title;		// 中文标题
	private String content;		// 内容
	private int sort;		// 顺序
	private String pic;		// 封面图
	
	private String smallType;// 1官方新闻，2媒体报道 3行业资讯

	
	private String item;//类型，包含父类型；
	
	private String position;//位置 0正常位置  1首页轮播 2首页单图

	
	private Date date;//发布时间
	
	private String status;//状态 0启用  1不启用

	private String author;//作者


	/**栏目id**/
	private Integer itemId;

	private String itemName;
	
	
	
	public News() {
		super();
	}

	public News(String id){
		super(id);
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@Length(min=0, max=256, message="中文标题长度必须介于 0 和 256 之间")
	@ExcelField(title="中文标题", align=2, sort=2)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@ExcelField(title="内容", align=2, sort=4)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}


	public int getSort() {
		return sort;
	}

	public void setSort(int sort) {
		this.sort = sort;
	}

	public String getPic() {
		return pic;
	}

	public void setPic(String pic) {
		this.pic = pic;
	}

	public String getSmallType() {
		return smallType;
	}

	public void setSmallType(String smallType) {
		this.smallType = smallType;
	}



	public long getNewsId() {
		return newsId;
	}

	public void setNewsId(long newsId) {
		this.newsId = newsId;
	}

	public String getItem() {
		return item;
	}

	public void setItem(String item) {
		this.item = item;
	}


	public String getZhDesc(){
		if(StringUtils.isNotBlank(this.getContent())){
			String content = HtmlUtil.cleanHtmlTag(this.getContent());
			content= content.replaceAll("&nbsp;", " ");
			return content.substring(0,content.length()>58?58:content.length())+"...";
		}
		return "";

	}



	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}


	public String getPublishDate(){
		if(ObjectUtil.isNotNull(this.getDate())){
			if(this.getType().equals(Constants.NewsType.ACTIVITY)){
				return DateUtil.format(this.getDate(),"yyyy/MM/dd");
			}
			return DateUtil.format(this.getDate(),"yyyy-MM-dd");
		}

		return "";
	}

	public String getNewsTypeLabel(){
		if(this.getType().equals(Constants.NewsType.NEWS)){
			return DictUtils.getDictLabel(this.getSmallType(),"news_type","");
		}
		if(this.getType().equals(Constants.NewsType.ACTIVITY)){
			return DictUtils.getDictLabel(this.getSmallType(),"activity_type","");
		}
		if(this.getType().equals(Constants.NewsType.COURSE)){
			return DictUtils.getDictLabel(this.getSmallType(),"course_type","");
		}
		if(this.getType().equals(Constants.NewsType.BRAND)){
			return DictUtils.getDictLabel(this.getSmallType(),"brand_type","");
		}
		if(this.getType().equals(Constants.NewsType.SERVICE)){
			return DictUtils.getDictLabel(this.getSmallType(),"service_type","");
		}
		return "";

	}

	public String getPicUrl(){
		if(StrUtil.isNotBlank(this.getPic())){
			return Global.getConfig("frontUrl")+this.getPic();
		}
		return "";
	}

	public Integer getItemId() {
		return itemId;
	}

	public void setItemId(Integer itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
}