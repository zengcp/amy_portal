package cn.gzjp.modules.portal.web;

import cn.gzjp.common.config.Global;
import cn.gzjp.common.constant.Constants;
import cn.gzjp.common.mapper.JsonMapper;
import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.utils.*;
import cn.gzjp.common.web.AjaxResult;
import cn.gzjp.common.web.BaseController;
import cn.gzjp.modules.portal.entity.*;
import cn.gzjp.modules.portal.entity.vo.DownLoadVo;
import cn.gzjp.modules.portal.service.DownloadService;
import cn.gzjp.modules.portal.service.MdictService;
import com.google.common.collect.Lists;
import com.xiaoleilu.hutool.util.StrUtil;
import org.apache.commons.io.FilenameUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * 下载中心
 * @author zengcp
 * @version 2017-08-22
 */
@Controller
@RequestMapping(value = "${adminPath}/portal/download")
public class DownloadController extends BaseController {

	@Autowired
	private DownloadService downloadService;
	@Autowired
	private MdictService mdictService;
	
	
	/**
	 * 列表页面
	 */
	@RequestMapping(value = {"list", ""})
	public String list(Download download, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Download> page = downloadService.findPage(new Page<Download>(request, response), download);
		model.addAttribute("page", page);
		return "modules/portal/downloadList";
	}

	/**
	 * 查看，增加，编辑表单页面
	 */
	@RequestMapping(value = "form")
	public String form(Download download, Model model) {
		if(StrUtil.isNotBlank(download.getId())){
			download = downloadService.get(download.getId());
		}
		model.addAttribute("download", download);
		String forward = "modules/portal/downloadForm";

		return forward;
	}

	/**
	 * 保存
	 */
	@RequestMapping(value = "save")
	public String save(Download download, Model model, RedirectAttributes redirectAttributes) throws Exception{
		if (!beanValidator(model, download)){
			return form(download,model);
		}
		if(!download.getIsNewRecord()){//编辑表单保存
			Download t = downloadService.get(download.getId());//从数据库取出记录的值
			MyBeanUtils.copyBeanNotNull2Bean(download, t);//将编辑表单中的非NULL值覆盖数据库记录中的值
			downloadService.save(t);//保存
		}else{//新增表单保存
			downloadService.save(download);//保存
		}
		addMessage(redirectAttributes, "保存成功");
		return redirect("/portal/download/");
	}
	
	/**
	 * 删除
	 */
	@RequestMapping(value = "delete")
	public String delete(Download download, RedirectAttributes redirectAttributes) {
		downloadService.delete(download);
		addMessage(redirectAttributes, "删除成功");
		CacheUtils.remove("downloadList");
		return redirect("/portal/download/?type="+download.getType());
	}
	
	
	@ModelAttribute
	public Download get(@RequestParam(required=false) String id) {
		Download entity = null;
		if (StringUtils.isNotBlank(id)){
			entity = downloadService.get(id);
		}
		if (entity == null){
			entity = new Download();
		}
		return entity;
	}
	

	/**
	 * 图片上传
	 * @param request
	 * @param file
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	@RequestMapping(value = "imageUpload")
	@ResponseBody
	public String imageUpload( HttpServletRequest request,MultipartFile file,Integer width,Integer height) throws IllegalStateException, IOException {
		AjaxResult result = new AjaxResult(false,"");
		// 判断文件是否为空  
        if (!file.isEmpty()) {  
                // 文件保存路径  
            	String realPath = Global.USERFILES_BASE_URL+"/upload_images/" ;
                // 转存文件  
            	FileUtils.createDirectory(Global.getUserfilesBaseDir()+realPath);
            	String extension = FilenameUtils.getExtension(file
    					.getOriginalFilename());
            	String mmddss = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date());
        		String fileName = mmddss+"."+extension;
        		
            	file.transferTo(new File( Global.getUserfilesBaseDir() +realPath +  fileName));  
            	String img = request.getContextPath()+realPath + fileName;

            	result.setSuccess(true);
    			result.setMsg(img);
        }  
    	return JsonMapper.getInstance().toJson(result);
	}
	/**
	 * 文件上传
	 * @param request
	 * @param file
	 * @return
	 * @throws IllegalStateException
	 * @throws IOException
	 */
	@RequestMapping(value = "fileUpload")
	@ResponseBody
	public String fileUpload(HttpServletRequest request,MultipartFile file) throws IllegalStateException, IOException {
		AjaxResult result = new AjaxResult(false,"");
		// 判断文件是否为空  
        if (!file.isEmpty()) {  
                // 文件保存路径  
            	String realPath = Global.USERFILES_BASE_URL+"/file/" ;
                // 转存文件  
            	FileUtils.createDirectory(Global.getUserfilesBaseDir()+realPath);
            	String extension = FilenameUtils.getExtension(file
    					.getOriginalFilename());
        		String newFileName = new SimpleDateFormat("yyyyMMddHHmmssSSS").format(new Date())+"."+extension;
            	file.transferTo(new File( Global.getUserfilesBaseDir() +realPath +  newFileName));  
            	String img = request.getContextPath()+realPath + newFileName;
            	result.setSuccess(true);
    			result.setMsg(img);
        }  
    	return JsonMapper.getInstance().toJson(result);
	}




	@RequestMapping(value = "createIndexHtml")
	@ResponseBody
	public String createIndexHtml(){
		String template=Constants.TEMPLATES+"download.html";
		VelocityContext context = this.getContext(4);

		List<Mdict> types = mdictService.findTreeList("download_type");
		//左侧类别树
		context.put("types", types);
		for(Mdict mdict:types){
			List<Mdict> childs =  mdict.getChildren();
			List<DownLoadVo> downLoadVoList = Lists.newArrayList();
			for(Mdict child:childs){
				Download search = new Download();
				search.setType(child);
				Page<Download> apage1 = downloadService.findPage(new Page<Download>(1,6),search);
				List<Download> downloadList = apage1.getList();
				DownLoadVo vo = new DownLoadVo();
				vo.setNextPage("no");
				int totalPage = apage1.getTotalPage()>0?apage1.getTotalPage():1;
				if(totalPage>1){
					vo.setNextPage("download"+child.getId()+"-2.html");
				}
				vo.setTypeId(child.getId());
				vo.setDownloadList(downloadList);
				downLoadVoList.add(vo);
			}
			mdict.setDownLoadVoList(downLoadVoList);
		}
		//pc首页
		context.put("downloadTypes", types);
		String fileName="download.html";
		StringWriter sw = new StringWriter();
		Template tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		String htmlFile = Global.getConfig("htmlPath")+fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)){
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}

		//手机首页
		//createMobileIndexHtml();
		context.put("types", types);
		template=Constants.TEMPLATES+"mobile/download.html";
		fileName="mobile/download.html";
		sw = new StringWriter();
		tpl = Velocity.getTemplate(template, Constants.UTF8);
		tpl.merge(context, sw);
		htmlFile = Global.getConfig("htmlPath")+fileName;
		FileUtils.deleteFile(htmlFile);
		if (FileUtils.createFile(htmlFile)){
			FileUtils.writeToFile(htmlFile, sw.toString(), true);
		}




		for(Mdict type:types){
			//一级不处理
			//二级类型
			for(Mdict child:type.getChildren()){
				type = child;
				context.put("type", type);
				context.put("typeName", type.getName());
				context.put("typeUrl", fileName);
				Download search= new Download();
				search.setItem(type.getId());
				Page<Download> apage1= downloadService.findPage(new Page<Download>(1,6),search);
				int totalPage = apage1.getTotalPage()>0?apage1.getTotalPage():1;
				for(int pageNo=1;pageNo<=totalPage;pageNo++) {
                    //pc
					template=Constants.TEMPLATES+"download_list.html";
					fileName="download"+type.getId()+"-"+pageNo+".html";

					List<Download> downloadList = downloadService.findPage(new Page<Download>(pageNo, 6),search).getList();
					context.put("downloadList", downloadList);
					context.put("nextPage","no");
					if(pageNo<totalPage){
						context.put("nextPage","download"+type.getId()+"-"+(pageNo+1)+".html");
					}
					sw = new StringWriter();
					tpl = Velocity.getTemplate(template, Constants.UTF8);
					tpl.merge(context, sw);


					htmlFile = Global.getConfig("htmlPath")+fileName;
					FileUtils.deleteFile(htmlFile);
					if (FileUtils.createFile(htmlFile)){
						FileUtils.writeToFile(htmlFile, sw.toString(), true);
					}

					//mobile
					template=Constants.TEMPLATES+"mobile/download_list.html";
					fileName="mobile/download"+type.getId()+"-"+pageNo+".html";

					sw = new StringWriter();
					tpl = Velocity.getTemplate(template, Constants.UTF8);
					tpl.merge(context, sw);
					htmlFile = Global.getConfig("htmlPath")+fileName;
					FileUtils.deleteFile(htmlFile);
					if (FileUtils.createFile(htmlFile)){
						FileUtils.writeToFile(htmlFile, sw.toString(), true);
					}

				}

			}
		}

		return JsonMapper.getInstance().toJson("生成成功");
	}



}