package cn.gzjp.modules.portal.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.gzjp.common.persistence.Page;
import cn.gzjp.common.service.CrudService;
import cn.gzjp.modules.portal.entity.Profile;
import cn.gzjp.modules.portal.dao.ProfileDao;

/**
 * 集团概况Service
 * @author zengcp
 * @version 2017-08-14
 */
@Service
@Transactional(readOnly = true)
public class ProfileService extends CrudService<ProfileDao, Profile> {

	public Profile get(String id) {
		return super.get(id);
	}
	
	public List<Profile> findList(Profile profile) {
		return super.findList(profile);
	}
	
	public Page<Profile> findPage(Page<Profile> page, Profile profile) {
		return super.findPage(page, profile);
	}
	

	@Transactional(readOnly = false)
	public void save(Profile profile) {
		super.save(profile);
	}
	
	@Transactional(readOnly = false)
	public void delete(Profile profile) {
		super.deleteByLogic(profile);
	}
	
}