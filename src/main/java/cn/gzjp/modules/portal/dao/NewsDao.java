package cn.gzjp.modules.portal.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.portal.entity.News;

/**
 * 新闻资讯DAO接口
 * @author zengcp
 * @version 2017-08-17
 */
@MyBatisDao
public interface NewsDao extends CrudDao<News> {

	News get(long id);
	
}