package cn.gzjp.modules.portal.entity.vo;

import java.util.List;

/**
 * @Author zengcp
 * @Date 2018/10/13
 */
public class ProvinceVo {

    private String province;

    private List<String> citys;

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public List<String> getCitys() {
        return citys;
    }

    public void setCitys(List<String> citys) {
        this.citys = citys;
    }
}
