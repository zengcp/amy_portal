<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>新闻资讯管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<div class="ibox-title">
		<h5>${item.name}列表 </h5>
	</div>
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="news" action="${ctx}/portal/news/" method="post" class="form-inline">
		<input id="itemId" name="itemId" type="hidden" value="${itemId}"/>
		<input id="type" name="type" type="hidden" value="${type}"/>
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		<div class="form-group">
			<span>标题：</span>
				<form:input path="title" htmlEscape="false" maxlength="256"  class=" form-control input-sm"/>

			<div class="pull-right">
				<button  class="btn btn-primary btn-rounded btn-outline btn-sm " onclick="search()" ><i class="fa fa-search"></i> 查询</button>

			</div>
		 </div>	
	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
			<shiro:hasPermission name="portal:news:add">
				<table:addRow url="${ctx}/portal/news/form?type=${type}&itemId=${itemId}" title="${item.name}" width="100%" height="100%" label="添加${item.name}"></table:addRow><!-- 增加按钮 -->
			</shiro:hasPermission>
			<button type="button" onclick="createIndexHtml(${type},${itemId})"  class="btn btn-primary btn-xs">生成html</button>
		</div>
		
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead>
			<tr>
				<th>分类</th>
				<th>标题</th>
				<th>发布日期</th>
				<th>创建时间</th>
				<th width="20%">操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="news">
			<tr>
				<td>
					${news.itemName}
				</td>
				<td>
					${news.title}
				</td>
				<td>
					<fmt:formatDate value="${news.date}" pattern="yyyy-MM-dd"/>
				</td>
				<td>
					<fmt:formatDate value="${news.createDate}" pattern="yyyy-MM-dd HH:mm:ss"/>
				</td>

				<td>
					<shiro:hasPermission name="portal:news:edit">
    					<a href="#" onclick="openDialog('修改', '${ctx}/portal/news/form?newsId=${news.newsId}&type=${news.type}&itemId=${news.itemId}','100%', '100%')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 修改</a>
    				</shiro:hasPermission>
    				<shiro:hasPermission name="portal:news:del">
						<a href="${ctx}/portal/news/delete?newsId=${news.newsId}" onclick="return confirmx('确认要删除该数据吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
					</shiro:hasPermission>

				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
<script type="text/javascript">


function createIndexHtml(type,itemId){
    layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
        if(type=='3'){
            $.get('${ctx}/html/create/brand',function(result){layer.msg('操作成功', {time: 2000}); search(); });
		}
        if(type=='6'){
            $.get('${ctx}/html/create/activity',function(result){layer.msg('操作成功', {time: 2000}); search(); });
        }
        if(type=='4'||type=='5'){
            $.get('${ctx}/portal/news/createHtml?itemId='+itemId+"&type="+type,function(result){layer.msg('操作成功', {time: 2000}); search(); });
		}

    });
}

</script>


</body>
</html>