<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>轮播图管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
		});
	</script>
</head>
<body class="gray-bg">
	<div class="wrapper wrapper-content">
	<div class="ibox">
	<!--
	<div class="ibox-title">
		<h5>轮播图列表 </h5>
	</div>
    -->
    <div class="ibox-content">
	<sys:message content="${message}"/>
	
	<!--查询条件-->
	<div class="row">
	<div class="col-sm-12">
	<form:form id="searchForm" modelAttribute="banner" action="${ctx}/portal/banner/" method="post" class="form-inline">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<table:sortColumn id="orderBy" name="orderBy" value="${page.orderBy}" callback="sortOrRefresh();"/><!-- 支持排序 -->
		
	</form:form>
	<br/>
	</div>
	</div>
	
	<!-- 工具栏 -->
	<div class="row">
	<div class="col-sm-12">
		<div class="pull-left">
			<shiro:hasPermission name="portal:banner:add">
				<table:addRow url="${ctx}/portal/banner/form" title="" width="800px" height="580px"></table:addRow><!-- 增加按钮 -->
			</shiro:hasPermission>

			</div>
		
	</div>
	</div>
	
	<!-- 表格 -->
	<table id="contentTable" class="table table-striped table-bordered table-hover table-condensed dataTables-example dataTable">
		<thead style="background:#fff">
			<tr>
				<th>图片</th>
				<%--<th>链接地址</th>--%>
				<th>顺序</th>
				<th width="30%">操作</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${page.list}" var="banner">
			<tr>
				<td>
					<img alt="" src="${banner.pic}" width="320px" height="90px" >	
				</td>
				<%--<td>--%>
					<%--${banner.url}--%>
				<%--</td>--%>
				<td>
					${banner.sort}
				</td>
				<td>
					
    					<a href="#" onclick="openDialog('编辑', '${ctx}/portal/banner/form?id=${banner.id}&lang=zh&type=${banner.type}','800px', '580px')" class="btn btn-success btn-xs" ><i class="fa fa-edit"></i> 编辑</a>
						<a href="${ctx}/portal/banner/delete?id=${banner.id}&type=${banner.type}" onclick="return confirmx('确认要删除吗？', this.href)"   class="btn btn-danger btn-xs"><i class="fa fa-trash"></i> 删除</a>
				</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	
		<!-- 分页代码 -->
	<table:page page="${page}"></table:page>
	<br/>
	<br/>
	</div>
	</div>
</div>
<script>
    function createHomeHtml(){
        layer.confirm('确认生成首页html?',{icon: 3, title:'系统提示'},   function(){
            $.get('${ctx}/html/create/home',function(result){layer.msg('操作成功', {time: 2000}); search(); });
        });
    }

    function createProfileHtml(){
        layer.confirm('确认生成简介页html?',{icon: 3, title:'系统提示'},   function(){
            $.get('${ctx}/html/create/profile',function(result){layer.msg('操作成功', {time: 2000}); search(); });
        });
    }

    function createProductHtml(){
        layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
            $.get('${ctx}/html/create/product',function(result){layer.msg('操作成功', {time: 2000}); search(); });
        });
    }

    function createBrandHtml(){
        layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
            $.get('${ctx}/html/create/brand',function(result){layer.msg('操作成功', {time: 2000}); search(); });
        });
    }
    function createActivityHtml(){
        layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
            $.get('${ctx}/html/create/activity',function(result){layer.msg('操作成功', {time: 2000}); search(); });
        });
    }

    function createNewsHtml(){
        layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
            $.get('${ctx}/html/create/news',function(result){layer.msg('操作成功', {time: 2000}); search(); });
        });
    }

    function createServiceHtml(){
        layer.confirm('确认生成html?',{icon: 3, title:'系统提示'},   function(){
            $.get('${ctx}/html/create/service',function(result){layer.msg('操作成功', {time: 2000}); search(); });
        });
    }

</script>
</body>
</html>