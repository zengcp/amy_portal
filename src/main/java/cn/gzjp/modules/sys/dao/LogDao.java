/**
 * Copyright &copy; 2015-2020 <a href="http://www..org/"></a> All rights reserved.
 */
package cn.gzjp.modules.sys.dao;

import cn.gzjp.common.persistence.CrudDao;
import cn.gzjp.common.persistence.annotation.MyBatisDao;
import cn.gzjp.modules.sys.entity.Log;

/**
 * 日志DAO接口
 * @author 
 * @version 2014-05-16
 */
@MyBatisDao
public interface LogDao extends CrudDao<Log> {

	public void empty();
}
